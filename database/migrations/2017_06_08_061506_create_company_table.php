<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('company_name');
            $table->string('address',100);
            $table->string('city',20);
            $table->string('zip_code',20)->nullable();
            $table->smallInteger('country_id');
            $table->string('phone',20);
            $table->string('email', 100)->nullable();
            $table->string('website', 50)->nullable();
            $table->string('fb_link', 50)->nullable();
            $table->string('logo', 50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
