<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('invoice_code');
            $table->date('invoice_date');
            $table->date('due_date');
            $table->smallInteger('company_id');
            $table->integer('customer_id');
            $table->text('notes')->nullable();
            $table->integer('total_price');
            $table->integer('advance_payment')->nullable();
            $table->integer('delivery_charge')->nullable();
            $table->enum('shipping_method',['Home Delivery', 'Condition Delivery']);
            $table->enum('status',['paid','partial paid','unpaid','cancelled']);
            $table->integer('created_by');
            $table->integer('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
