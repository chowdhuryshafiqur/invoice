
## Q-Invoice

Q-Invoice is a Invoicing system for multiple 
company or store can create invoice using this application easily.

#Features
1. Create invoice ( 2 design template).
2. List Invoice ( All, paid, unpaid, canceled, Partially Paid )
3. Dashboard
4. Daily Invoice Report
5. Today's Amount Income
6. Income Report (Daily, monthly, yearly, Lifetime)


# Language technologies
    1. Laravel Framework
    2. Php > 5.6
    3. MySQL (Relational Database)
    4. HTML, CSS, Bootstrap
    5. Javascript, JQuery
    
## Contributor
   Mobarok Hossen 
   
   Skype: mobarok9 
   
   Email: mobarok_hossen@outlook.com 
   
   website: http://mobarokhossen.me
   
   
   
   Shafiq Chowdhury

## Security Vulnerabilities


## License

