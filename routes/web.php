<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//ADMIN CONTROLLER
Route::get('/','HomeController@index');


// Start Admin Customer Controller
Route::get('/add-customer','AdminCustomerController@add_customer');
Route::get('/list-customer','AdminCustomerController@list_customer');
Route::post('/save-customer','AdminCustomerController@save_customer');
Route::post('/save-customer-ajax','AdminCustomerController@save_customer_ajax');
Route::get('/delete-customer/{id}','AdminCustomerController@delete_customer');
Route::get('/edit-customer/{id}','AdminCustomerController@edit_customer');
Route::post('/update-customer','AdminCustomerController@update_customer');
Route::get('/select-customer-info/{id}','AdminCustomerController@select_customer_info');
// End Admin Customer Controller



//Start Company Controller
Route::get('/add-company','AdminCompanyController@add_company');
Route::post('/save-company','AdminCompanyController@save_company');
Route::get('/list-company','AdminCompanyController@list_company');
Route::get('/delete-company/{id}','AdminCompanyController@delete_company');
Route::get('/edit-company/{id}','AdminCompanyController@edit_company');
Route::post('/update-company','AdminCompanyController@update_company');




//Start Invoice Controller
Route::get('/create-invoice','AdminInvoiceController@create_invoice');
Route::get('/create-invoice-2','AdminInvoiceController@add_invoice');
Route::post('/save-invoice','AdminInvoiceController@save_invoice');
Route::get('/edit-invoice/{id}','AdminInvoiceController@edit_invoice');
Route::post('/update-invoice','AdminInvoiceController@update_invoice');
Route::get('/list-invoice','AdminInvoiceController@list_invoice');
Route::get('/unpaid-list-invoice','AdminInvoiceController@unpaid_invoices');
Route::get('/paid-list-invoice','AdminInvoiceController@paid_invoices');
Route::get('/cancel-list-invoice','AdminInvoiceController@cancel_invoices');
Route::get('/partially-paid-list-invoice','AdminInvoiceController@partial_paid_invoices');
Route::get('/unpaid-invoice/{id}','AdminInvoiceController@unpaid_invoice');
Route::get('/paid-invoice/{id}','AdminInvoiceController@paid_invoice');
Route::get('/cancel-invoice/{id}','AdminInvoiceController@cancel_invoice');
Route::get('/delete-invoice/{id}','AdminInvoiceController@delete_invoice');
Route::get('/download-invoice/{id}','AdminInvoiceController@download_invoice');
Route::get('/view-invoice-pdf/{id}','AdminInvoiceController@view_invoice_pdf');
Route::get('/view-invoice/{id}','AdminInvoiceController@view_invoice');
Route::get('/view-update/{id}','AdminInvoiceController@view_update');
Route::get('/preview-invoice/{id}','AdminInvoiceController@preview_invoice');
Route::get('/view-print/{id}','AdminInvoiceController@print_view_invoice');

// End Admin Invoice Controller

//Route::get('/list-shipping-method','AdminShippingController@list_shipping_method');
Route::get('/add-shipping-method','AdminShippingController@add_shipping_method');
Route::post('/save-shipping-method','AdminShippingController@save_shipping_method');
Route::post('/update-shipping-method','AdminShippingController@update_shipping_method');
Route::get('/delete-shipping/{id}','AdminShippingController@delete_shipping');
Route::get('/edit-shipping/{id}','AdminShippingController@edit_shipping_method');

//Setting
Route::get('/system-setting','AdminSettingController@index');
Route::post('/save-setting','AdminSettingController@save_setting');
Route::post('/update-setting','AdminSettingController@update_setting');


Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');


Route::get('/home','AdminController@index')->name('home');
