// Wait for the DOM to be ready
$(function() {
  // Initialize form validation on the registration form.
  // It has the name attribute "registration"
  $("form[name='customer_info']").validate({
    // Specify validation rules
    rules: {
      // The key name on the left side is the name attribute
      // of an input field. Validation rules are defined
      // on the right side
      company_name: "required",
      full_name: "required",
      email: {
        required: true,
        // Specify that email should be validated
        // by the built-in "email" rule
        email: true
      },
     phone:"required",
     address:"required",
     city:"required",
     zip:"required",
     country:"required"
  
    },
    // Specify validation error messages
    messages: {
      company_name: "Please enter your company name",
      full_name: "Please enter your full Name",
     
      email: "Please enter a valid email address",
      phone: "Please enter a valid email address",
      address: "Please enter a valid email address",
      city: "Please enter a valid email address",
      country: "Please enter a valid email address"
      
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    submitHandler: function(form) {
      form.submit();
    }
  });
});