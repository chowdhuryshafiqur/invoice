/**
 * Created by Mobarok Hossen on 7/29/2017.
 */

$(document).ready(function () {


    var $invoice_items = $('#invoice_items');

    $('.item_name').redactor({paragraphize: false,
        replaceDivs: false,
        linebreaks: true});



    $invoice_items.on('change', 'select', function(){
        //   $('#taxtotal').html('dd');
        var taxrate = $('#stax').val().replace(',', '.');
        // $(this).val(taxrate);

    });

    var item_remove = $('#item-remove');
    item_remove.hide();



    item_remove.on('click', function(){
        $("#invoice_items tr.info").fadeOut(300, function(){
            $(this).remove();
            total_price();
        });

    });


    var rowNum = 0;

    $('#blank-add').on('click', function(){
        rowNum++;
        $invoice_items.find('tbody')
            .append(
                '<tr> ' +
                '<td><input type="text" class="form-control"  name="item_code[]"></td> ' +
                '<td>' +
                '<textarea class="form-control item_name" name="product[]" rows="3" id="i_' + rowNum + '">' +
                '</textarea></td> ' +
                '<td><input type="number" class="form-control per_product_price" onkeyup="total_products_price('+rowNum+')" name="qty[]"  id="total_qty_' + rowNum + '"></td> ' +
                '<td><input type="number" class="form-control per_product_price" onkeyup="total_products_price('+rowNum+')" name="price[] "  id="per_price_' + rowNum + '" ></td> ' +
                '<td class="ltotal"><input type="text" class="form-control  total_per_product" id="total_product_price_' + rowNum + '" readonly ></td>' +
                '</tr>'
            );


        $('#i_' + rowNum).redactor({paragraphize: false,
            replaceDivs: false,
            linebreaks: true});

        item_remove.show();
    });

    $invoice_items.on('click', '.redactor-editor', function(){
        $("tr").removeClass("info");
        $(this).closest('tr').addClass("info");

        item_remove.show();
    });

    //function doStuff() {
    //    $('.amount').autoNumeric('init');
    //   // alert('dd');
    //}
    //setInterval(doStuff, 5000);

/*

 $("input.per_product_price").change(function(){

 var total_product_price = $(this).attr("data-target");
 var product_qty = $(this).attr("data-qty");
 var per_price = $(this).attr("data-price");

 var qty = $(product_qty).val();
 var price = $(per_price).val();

 qty==null?qty=0:qty;
 price==null?price=0:price;

 var total_per_product = qty*price;

 $(total_product_price).val(total_per_product);


 console.log("total_per_product: "+total_per_product+" Quantity: "+qty+" Per price: "+price);

 });

 */

    $("#delivery_payment, #advance_payment").keyup(function () {
        total_price();
    });


    $("#status").change(function() {
        if ($("#status").val() == "partial paid") {
            $("#adv_payment").css("display", "block");
            $("#advance_payment").css("display", "block");
        }else {
            $("#adv_payment").css("display", "none");
            $("#advance_payment").css("display", "none");
            $("#advance_payment").val('')
            total_price();
        }
    }).trigger("change");



    $('#discount_setting').click(function(){
        var dis_type = $('input[name=set_discount_type]:checked', '#discount_info').val();
        if(dis_type!=null){

            var get_discount = $('.discount_amount').val();

            $('.tr-discount').css("display","table-row");
            if(dis_type=='percent'){

                $('.discount_text').text("Discount ( "+get_discount+"% )");
                $("#type_of_discount").val('Percent');
                $("#discount_percent").val(get_discount);

            }else if(dis_type=='fixed'){

                $('.discount_text').text("Fixed Discount");
                $('.discount_amount_total').text("- "+get_discount);
                $("#type_of_discount").val('Fixed');
                $("#discount_percent").val(get_discount);
            }
        }

        total_price();
    });

});

var maxRow, total, subtotal;

function total_products_price(row_num) {

    var qty = $("#total_qty_"+row_num).val();
    var price = $("#per_price_"+row_num).val();

    var total_per_product = qty*price;

    $("#total_product_price_"+row_num).val(total_per_product);
   // console.log("total_per_product: "+total_per_product+" Quantity: "+qty+" Per price: "+price);
    maxRow = $("#last_row").val();

    if(maxRow < row_num){
        $("#last_row").val(row_num);
    }
    total_price();
}

function total_price() {
    total = 0;
    subtotal = 0;
    var per_product =0;
    maxRow = parseInt($("#last_row").val());

    var delivery_charge = $('#delivery_payment').val();



    delivery_charge==''?delivery_charge=0:delivery_charge=parseInt(delivery_charge);


   // console.log("delivery_charge: "+delivery_charge);
    for(var i=0; i<=maxRow;i++)
    {
        per_product = parseInt($("#total_product_price_"+i).val());

        subtotal+=per_product;
        total+=per_product;

        //$("#total_amount").val(total);
      //  console.log("Total: "+total);
    }


    if(delivery_charge!='') {
        $('.tr-delivery-charge').css("display", "table-row");
        $('.delivery_charge').text(delivery_charge);
    }else{
        $('.tr-delivery-charge').css("display", "none");
        delivery_charge=0;
    }


    var advance_payment = 0;

    if($('#status').val()=="partial paid")
    {
        $("#advance_payment").val()==""?advance_payment:advance_payment = parseInt($("#advance_payment").val());


        $("#adv_invoice_amount").val(advance_payment);
        $('.tr-advence-payment').css("display", "table-row");
        $('.advence_payment').text("- "+parseInt(advance_payment));
    }else{
        $('.tr-advence-payment').css("display", "none");
    }

    var discount = 0;

    if($("#type_of_discount").val()=="Fixed"){
        var discount = $("#discount_percent").val();

    }else if($("#type_of_discount").val()=="Percent"){
        var get_discount = $("#discount_percent").val();
        var amount_discount  = get_discount*0.01;
        var discount = amount_discount*subtotal;
        $('.discount_amount_total').text("- "+parseInt(discount));

    }else{
        $("#type_of_discount").val("");
        $("#discount_percent").val("");
        $('.tr-discount').css("display","none");
    }


    total=total+delivery_charge-advance_payment-discount;
    $('.sub_total').text(parseInt(subtotal));
    $('.total').text(parseInt(total));
}
