/**
 * Created by MOBAROK on 7/6/2017.
 */

    $(document).ready(function(){
        $('.status-change').on('click', function(event){
            event.preventDefault();
            var ref_link= $(this).attr("href");
            bootbox.confirm(
                {
                    title: "Alert",
                    message: "Are you sure? ",
                    buttons: {
                        cancel: {
                            label: '<i class="fa fa-times"></i> Cancel',
                            className: 'btn-danger'
                        },
                        confirm: {
                            label: '<i class="fa fa-check"></i> Confirm',
                            className: 'btn-success'
                        }
                    }, callback: function(result) {
                    if (result) {
                        //include the href duplication link here?;
                        window.location=ref_link;


                    }
                }
                });
        });
    });

