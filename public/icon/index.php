
<!DOCTYPE html>


<!-- The Dhaka Digital, 2017 -->

<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Contacts - Demo Company</title>
    <link rel="shortcut icon" href="icon/favicon.ico" type="image/x-icon" />

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/lib/_all.css" rel="stylesheet">
    <link href="css/animate.css" rel="stylesheet">
    <link href="css/bootstrap-toggle.min.css" rel="stylesheet">
    <link href="css/open-sans.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/component.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">

   <link href="css/ibilling_icons.css" rel="stylesheet">
   <link href="css/material.css" rel="stylesheet">

      <link href="css/dark.css" rel="stylesheet">



    
       <link rel="stylesheet" type="text/css" href="css/select2.min.css" />
        
    
    

</head>

<body class="fixed-nav ">
<section>
    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">

                <ul class="nav" id="side-menu">

    <li class="nav-header">
        <div class="dropdown profile-element"> <span>

                                                    <img src="icon/default-user-avatar.png"  class="img-circle" style="max-width: 64px;" alt="">
                                                             </span>
            <a data-toggle="dropdown" class="dropdown-toggle" href="#" aria-expanded="false">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">Administrator</strong>
                             </span> <span class="text-muted text-xs block">My Account <b class="caret"></b></span> </span> </a>
            <ul class="dropdown-menu animated fadeIn m-t-xs">
                <li><a href="">Edit Profile</a></li>
                <li><a href="">Change Password</a></li>

                <li class="divider"></li>
                <li><a href="">Logout</a></li>
            </ul>
        </div>
    </li>

    

            <li ><a href=""><i class="fa fa-tachometer"></i> <span class="nav-label">Dashboard</span></a></li>
    



    

        <li class="active">
        <a href="#"><i class="icon-users"></i> <span class="nav-label">Customers</span><span class="fa arrow"></span></a>
        <ul class="nav nav-second-level">
            <li><a href="add_customer.php">Add Customer</a></li>

            <li><a href="">List Customers</a></li>
            <li><a href="group.php">Groups</a></li>
            
        </ul>
    </li>
        

        <li ><a href=""><i class="fa fa-building-o"></i> <span class="nav-label">Companies</span></a></li>
    

    
        
    


    
                        <li class="">
                <a href="#"><i class="fa fa-database"></i> <span class="nav-label">Transactions</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li><a href="">New Deposit</a></li>
                    <li><a href="">New Expense</a></li>
                    <li><a href="">Transfer</a></li>
                    <li><a href="">View Transactions</a></li>
                    <li><a href="">Balance Sheet</a></li>
                </ul>
            </li>
            


            <li class="">
                <a href="#"><i class="icon-credit-card-1"></i> <span class="nav-label">Sales</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">

                                            <li><a href="">Invoices</a></li>
                        <li><a href="">New Invoice</a></li>
                        <li><a href="">Recurring Invoices</a></li>
                        <li><a href="">New Recurring Invoice</a></li>
                    
                                            <li><a href="">Quotes</a></li>
                        <li><a href="">Create New Quote</a></li>
                                        <li><a href="">Payments</a></li>
                </ul>
            </li>

        
    


    
        
    


            <li ><a href=""><i class="fa fa-file-o"></i> <span class="nav-label">Documents</span></a></li>
    
            <li ><a href=""><i class="fa fa-calendar"></i> <span class="nav-label">Calendar</span></a></li>
    


    

                        <li class="">
                <a href="#"><i class="fa fa-university"></i> <span class="nav-label">Bank & Cash</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li><a href="account.php">New Account</a></li>

                    <li><a href="view_account.php">List Accounts</a></li>
                    <li><a href="balance.php">Account Balances</a></li>

                </ul>
            </li>
        
    

    

    
            <li class="">
            <a href="#"><i class="fa fa-cube"></i> <span class="nav-label">Products & Services</span><span class="fa arrow"></span></a>
            <ul class="nav nav-second-level">
                <li><a href="">Products</a></li>
                <li><a href="">New Product</a></li>
                <li><a href="">Services</a></li>
                <li><a href="">New Service</a></li>


            </ul>
        </li>
    
    

    

    
            
            <li class="">
            <a href="#"><i class="fa fa-bar-chart-o"></i> <span class="nav-label">Reports </span><span class="fa arrow"></span></a>
            <ul class="nav nav-second-level">


                <li><a href="account_statement.php">Account Statement</a></li>
                <li><a href="">Income Reports</a></li>
                <li><a href="">Expense Reports</a></li>
                <li><a href="">Income Vs Expense</a></li>

                <li><a href="">Reports by Date</a></li>
                
                <li><a href="">All Income</a></li>
                <li><a href="">All Expense</a></li>
                <li><a href="">All Transactions</a></li>


                


            </ul>
            </li>

        
    
    
        <li class="">
            <a href="#"><i class="icon-article"></i> <span class="nav-label">Utilities </span><span class="fa arrow"></span></a>
            <ul class="nav nav-second-level">
                <li><a href="">Activity Log</a></li>
                <li><a href="">Email Message Log</a></li>
                <li><a href="">Database Status</a></li>
                <li><a href="">CRON Log</a></li>
                <li><a href="">Integration Code</a></li>
                <li><a href="">System Status</a></li>
                <li><a href="">Terminal</a></li>
            </ul>
        </li>

    

    
        
        

            
            
            



        
    

    
        <li class="" id="li_appearance">
            <a href="#"><i class="icon-params"></i> <span class="nav-label">Appearance </span><span class="fa arrow"></span></a>
            <ul class="nav nav-second-level">

                <li><a href="">User Interface</a></li>
                <li><a href="">Customize</a></li>

                

                <li><a href="">Editor</a></li>

                <li><a href="">Themes</a></li>

            </ul>
        </li>


           
    



</ul>

            </div>
        </nav>
        <div id="page-wrapper" class="gray-bg">
            <div class="row border-bottom">
                <nav class="navbar navbar-fixed-top white-bg" role="navigation" style="margin-bottom: 0">

                    <img class="logo" style="max-height: 40px; width: auto;" src="http://demo.tryib.com/application/storage/system/logo.png" alt="Logo">

                    <div class="navbar-header">
                        <a class="navbar-minimalize minimalize-styl-2 btn btn-primary btn-flat" href="#"><i class="fa fa-dedent"></i> </a>

                    </div>
                    <ul class="nav navbar-top-links navbar-right pull-right">



                        <li class="hidden-xs">
                            <form class="navbar-form full-width" method="post" action="http://demo.tryib.com/contacts/list/">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="name" placeholder="Search Customers...">
                                    <button type="submit" class="btn btn-search"><i class="fa fa-search"></i></button>
                                </div>
                            </form>
                        </li>

                        
                        
                        

                        

                        <li class="dropdown">
                            <a class="dropdown-toggle count-info" data-toggle="dropdown" id="get_activity" href="#" aria-expanded="true">
                                <i class="fa fa-bell"></i>
                            </a><div class="dropdown-backdrop"></div>
                            <ul class="dropdown-menu dropdown-alerts" id="activity_loaded">



                                <li id="activity_wait">
                                    <div class="text-center link-block">
                                        <a href="javascript:void(0)">
                                            <strong>Please Wait...</strong> <br> <br>
                                            <img class="text-center" src="http://demo.tryib.com/application/storage/system/download.gif" alt="Loading....">

                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </li>

                        <li class="dropdown navbar-user">

                            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">

                                                                    <img src="http://demo.tryib.com/ui/lib/imgs/default-user-avatar.png" alt="">
                                
                                <span class="hidden-xs">Welcome Administrator</span> <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu animated fadeIn">
                                <li class="arrow"></li>
                                <li><a href="">Edit Profile</a></li>
                                <li><a href="">Change Password</a></li>
                                <li class="divider"></li>
                                <li><a href="">Logout</a></li>

                            </ul>
                        </li>

                        <li>
                            <a class="right-sidebar-toggle">
                                <i class="fa fa-tasks"></i>
                            </a>
                        </li>




                    </ul>

                </nav>
            </div>

            <div class="row wrapper white-bg page-heading">
                <div class="col-lg-12">
                    <h2 style="color: #2F4050; font-size: 16px; font-weight: 400; margin-top: 18px"> Contacts </h2>

                </div>

            </div>

            <div class="wrapper wrapper-content animated fadeIn">
                

<div class="wrapper wrapper-content">
<div class="row">

    <div class="col-md-12">



        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Add Contact</h5>

                <a href="http://demo.tryib.com/contacts/import_csv/" class="btn btn-xs btn-primary btn-rounded pull-right"><i class="fa fa-bars"></i> Import Contacts</a>

            </div>
            <div class="ibox-content" id="ibox_form">
                <div class="alert alert-danger" id="emsg">
                    <span id="emsgbody"></span>
                </div>

                <form class="form-horizontal" id="rform">

                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group"><label class="col-md-4 control-label" for="account">Full Name<small class="red">*</small> </label>

                                <div class="col-lg-8"><input type="text" id="account" name="account" class="form-control" autofocus>

                                </div>
                            </div>

                            <div class="form-group"><label class="col-md-4 control-label" for="company">Company Name</label>

                                <div class="col-lg-8"><input type="text" id="company" name="company" class="form-control">

                                </div>
                            </div>

                            <div class="form-group"><label class="col-md-4 control-label" for="email">Email</label>

                                <div class="col-lg-8"><input type="text" id="email" name="email" class="form-control">

                                </div>
                            </div>
                            <div class="form-group"><label class="col-md-4 control-label" for="phone">Phone</label>

                                <div class="col-lg-8"><input type="text" id="phone" name="phone" class="form-control">

                                </div>
                            </div>
                            <div class="form-group"><label class="col-md-4 control-label" for="address">Address</label>

                                <div class="col-lg-8"><input type="text" id="address" name="address" class="form-control">

                                </div>
                            </div>


                            <div class="form-group"><label class="col-md-4 control-label" for="city">City</label>

                                <div class="col-lg-8"><input type="text" id="city" name="city" class="form-control">

                                </div>
                            </div>
                            <div class="form-group"><label class="col-md-4 control-label" for="state">State/Region</label>

                                <div class="col-lg-8"><input type="text" id="state" name="state" class="form-control">

                                </div>
                            </div>
                            <div class="form-group"><label class="col-md-4 control-label" for="zip">ZIP/Postal Code </label>

                                <div class="col-lg-8"><input type="text" id="zip" name="zip" class="form-control">

                                </div>
                            </div>
                            <div class="form-group"><label class="col-md-4 control-label" for="country">Country</label>

                                <div class="col-lg-8">

                                    <select name="country" id="country" class="form-control">
                                        <option value="">Select Country</option>
                                        
	<option value="Afghanistan">Afghanistan</option>
	<option value="Aland Islands">Aland Islands</option>
	<option value="Albania">Albania</option>
	<option value="Algeria">Algeria</option>
	<option value="American Samoa">American Samoa</option>
	<option value="Andorra">Andorra</option>
	<option value="Angola">Angola</option>
	<option value="Anguilla">Anguilla</option>
	<option value="Antarctica">Antarctica</option>
	<option value="Antigua And Barbuda">Antigua And Barbuda</option>
	<option value="Argentina">Argentina</option>
	<option value="Armenia">Armenia</option>
	<option value="Aruba">Aruba</option>
	<option value="Australia">Australia</option>
	<option value="Austria">Austria</option>
	<option value="Azerbaijan">Azerbaijan</option>
	<option value="Bahamas">Bahamas</option>
	<option value="Bahrain">Bahrain</option>
	<option value="Bangladesh">Bangladesh</option>
	<option value="Barbados">Barbados</option>
	<option value="Belarus">Belarus</option>
	<option value="Belgium">Belgium</option>
	<option value="Belize">Belize</option>
	<option value="Benin">Benin</option>
	<option value="Bermuda">Bermuda</option>
	<option value="Bhutan">Bhutan</option>
	<option value="Bolivia">Bolivia</option>
	<option value="Bosnia And Herzegovina">Bosnia And Herzegovina</option>
	<option value="Botswana">Botswana</option>
	<option value="Bouvet Island">Bouvet Island</option>
	<option value="Brazil">Brazil</option>
	<option value="British Indian Ocean Territory">British Indian Ocean Territory</option>
	<option value="Brunei Darussalam">Brunei Darussalam</option>
	<option value="Bulgaria">Bulgaria</option>
	<option value="Burkina Faso">Burkina Faso</option>
	<option value="Burundi">Burundi</option>
	<option value="Cambodia">Cambodia</option>
	<option value="Cameroon">Cameroon</option>
	<option value="Canada">Canada</option>
	<option value="Cape Verde">Cape Verde</option>
	<option value="Cayman Islands">Cayman Islands</option>
	<option value="Central African Republic">Central African Republic</option>
	<option value="Chad">Chad</option>
	<option value="Chile">Chile</option>
	<option value="China">China</option>
	<option value="Christmas Island">Christmas Island</option>
	<option value="Cocos (Keeling) Islands">Cocos (Keeling) Islands</option>
	<option value="Colombia">Colombia</option>
	<option value="Comoros">Comoros</option>
	<option value="Congo">Congo</option>
	<option value="Congo, Democratic Republic">Congo, Democratic Republic</option>
	<option value="Cook Islands">Cook Islands</option>
	<option value="Costa Rica">Costa Rica</option>
	<option value="Cote D'Ivoire">Cote D'Ivoire</option>
	<option value="Croatia">Croatia</option>
	<option value="Cuba">Cuba</option>
	<option value="Cyprus">Cyprus</option>
	<option value="Czech Republic">Czech Republic</option>
	<option value="Denmark">Denmark</option>
	<option value="Djibouti">Djibouti</option>
	<option value="Dominica">Dominica</option>
	<option value="Dominican Republic">Dominican Republic</option>
	<option value="Ecuador">Ecuador</option>
	<option value="Egypt">Egypt</option>
	<option value="El Salvador">El Salvador</option>
	<option value="Equatorial Guinea">Equatorial Guinea</option>
	<option value="Eritrea">Eritrea</option>
	<option value="Estonia">Estonia</option>
	<option value="Ethiopia">Ethiopia</option>
	<option value="Falkland Islands (Malvinas)">Falkland Islands (Malvinas)</option>
	<option value="Faroe Islands">Faroe Islands</option>
	<option value="Fiji">Fiji</option>
	<option value="Finland">Finland</option>
	<option value="France">France</option>
	<option value="French Guiana">French Guiana</option>
	<option value="French Polynesia">French Polynesia</option>
	<option value="French Southern Territories">French Southern Territories</option>
	<option value="Gabon">Gabon</option>
	<option value="Gambia">Gambia</option>
	<option value="Georgia">Georgia</option>
	<option value="Germany">Germany</option>
	<option value="Ghana">Ghana</option>
	<option value="Gibraltar">Gibraltar</option>
	<option value="Greece">Greece</option>
	<option value="Greenland">Greenland</option>
	<option value="Grenada">Grenada</option>
	<option value="Guadeloupe">Guadeloupe</option>
	<option value="Guam">Guam</option>
	<option value="Guatemala">Guatemala</option>
	<option value="Guernsey">Guernsey</option>
	<option value="Guinea">Guinea</option>
	<option value="Guinea-Bissau">Guinea-Bissau</option>
	<option value="Guyana">Guyana</option>
	<option value="Haiti">Haiti</option>
	<option value="Heard Island & Mcdonald Islands">Heard Island & Mcdonald Islands</option>
	<option value="Holy See (Vatican City State)">Holy See (Vatican City State)</option>
	<option value="Honduras">Honduras</option>
	<option value="Hong Kong">Hong Kong</option>
	<option value="Hungary">Hungary</option>
	<option value="Iceland">Iceland</option>
	<option value="India">India</option>
	<option value="Indonesia">Indonesia</option>
	<option value="Iran, Islamic Republic Of">Iran, Islamic Republic Of</option>
	<option value="Iraq">Iraq</option>
	<option value="Ireland">Ireland</option>
	<option value="Isle Of Man">Isle Of Man</option>
	<option value="Israel">Israel</option>
	<option value="Italy">Italy</option>
	<option value="Jamaica">Jamaica</option>
	<option value="Japan">Japan</option>
	<option value="Jersey">Jersey</option>
	<option value="Jordan">Jordan</option>
	<option value="Kazakhstan">Kazakhstan</option>
	<option value="Kenya">Kenya</option>
	<option value="Kiribati">Kiribati</option>
	<option value="Korea">Korea</option>
	<option value="Kuwait">Kuwait</option>
	<option value="Kyrgyzstan">Kyrgyzstan</option>
	<option value="Lao People's Democratic Republic">Lao People's Democratic Republic</option>
	<option value="Latvia">Latvia</option>
	<option value="Lebanon">Lebanon</option>
	<option value="Lesotho">Lesotho</option>
	<option value="Liberia">Liberia</option>
	<option value="Libyan Arab Jamahiriya">Libyan Arab Jamahiriya</option>
	<option value="Liechtenstein">Liechtenstein</option>
	<option value="Lithuania">Lithuania</option>
	<option value="Luxembourg">Luxembourg</option>
	<option value="Macao">Macao</option>
	<option value="Macedonia">Macedonia</option>
	<option value="Madagascar">Madagascar</option>
	<option value="Malawi">Malawi</option>
	<option value="Malaysia">Malaysia</option>
	<option value="Maldives">Maldives</option>
	<option value="Mali">Mali</option>
	<option value="Malta">Malta</option>
	<option value="Marshall Islands">Marshall Islands</option>
	<option value="Martinique">Martinique</option>
	<option value="Mauritania">Mauritania</option>
	<option value="Mauritius">Mauritius</option>
	<option value="Mayotte">Mayotte</option>
	<option value="Mexico">Mexico</option>
	<option value="Micronesia, Federated States Of">Micronesia, Federated States Of</option>
	<option value="Moldova">Moldova</option>
	<option value="Monaco">Monaco</option>
	<option value="Mongolia">Mongolia</option>
	<option value="Montenegro">Montenegro</option>
	<option value="Montserrat">Montserrat</option>
	<option value="Morocco">Morocco</option>
	<option value="Mozambique">Mozambique</option>
	<option value="Myanmar">Myanmar</option>
	<option value="Namibia">Namibia</option>
	<option value="Nauru">Nauru</option>
	<option value="Nepal">Nepal</option>
	<option value="Netherlands">Netherlands</option>
	<option value="Netherlands Antilles">Netherlands Antilles</option>
	<option value="New Caledonia">New Caledonia</option>
	<option value="New Zealand">New Zealand</option>
	<option value="Nicaragua">Nicaragua</option>
	<option value="Niger">Niger</option>
	<option value="Nigeria">Nigeria</option>
	<option value="Niue">Niue</option>
	<option value="Norfolk Island">Norfolk Island</option>
	<option value="Northern Mariana Islands">Northern Mariana Islands</option>
	<option value="Norway">Norway</option>
	<option value="Oman">Oman</option>
	<option value="Pakistan">Pakistan</option>
	<option value="Palau">Palau</option>
	<option value="Palestinian Territory, Occupied">Palestinian Territory, Occupied</option>
	<option value="Panama">Panama</option>
	<option value="Papua New Guinea">Papua New Guinea</option>
	<option value="Paraguay">Paraguay</option>
	<option value="Peru">Peru</option>
	<option value="Philippines">Philippines</option>
	<option value="Pitcairn">Pitcairn</option>
	<option value="Poland">Poland</option>
	<option value="Portugal">Portugal</option>
	<option value="Puerto Rico">Puerto Rico</option>
	<option value="Qatar">Qatar</option>
	<option value="Reunion">Reunion</option>
	<option value="Romania">Romania</option>
	<option value="Russian Federation">Russian Federation</option>
	<option value="Rwanda">Rwanda</option>
	<option value="Saint Barthelemy">Saint Barthelemy</option>
	<option value="Saint Helena">Saint Helena</option>
	<option value="Saint Kitts And Nevis">Saint Kitts And Nevis</option>
	<option value="Saint Lucia">Saint Lucia</option>
	<option value="Saint Martin">Saint Martin</option>
	<option value="Saint Pierre And Miquelon">Saint Pierre And Miquelon</option>
	<option value="Saint Vincent And Grenadines">Saint Vincent And Grenadines</option>
	<option value="Samoa">Samoa</option>
	<option value="San Marino">San Marino</option>
	<option value="Sao Tome And Principe">Sao Tome And Principe</option>
	<option value="Saudi Arabia">Saudi Arabia</option>
	<option value="Senegal">Senegal</option>
	<option value="Serbia">Serbia</option>
	<option value="Seychelles">Seychelles</option>
	<option value="Sierra Leone">Sierra Leone</option>
	<option value="Singapore">Singapore</option>
	<option value="Slovakia">Slovakia</option>
	<option value="Slovenia">Slovenia</option>
	<option value="Solomon Islands">Solomon Islands</option>
	<option value="Somalia">Somalia</option>
	<option value="South Africa">South Africa</option>
	<option value="South Georgia And Sandwich Isl.">South Georgia And Sandwich Isl.</option>
	<option value="Spain">Spain</option>
	<option value="Sri Lanka">Sri Lanka</option>
	<option value="Sudan">Sudan</option>
	<option value="Suriname">Suriname</option>
	<option value="Svalbard And Jan Mayen">Svalbard And Jan Mayen</option>
	<option value="Swaziland">Swaziland</option>
	<option value="Sweden">Sweden</option>
	<option value="Switzerland">Switzerland</option>
	<option value="Syrian Arab Republic">Syrian Arab Republic</option>
	<option value="Taiwan">Taiwan</option>
	<option value="Tajikistan">Tajikistan</option>
	<option value="Tanzania">Tanzania</option>
	<option value="Thailand">Thailand</option>
	<option value="Timor-Leste">Timor-Leste</option>
	<option value="Togo">Togo</option>
	<option value="Tokelau">Tokelau</option>
	<option value="Tonga">Tonga</option>
	<option value="Trinidad And Tobago">Trinidad And Tobago</option>
	<option value="Tunisia">Tunisia</option>
	<option value="Turkey">Turkey</option>
	<option value="Turkmenistan">Turkmenistan</option>
	<option value="Turks And Caicos Islands">Turks And Caicos Islands</option>
	<option value="Tuvalu">Tuvalu</option>
	<option value="Uganda">Uganda</option>
	<option value="Ukraine">Ukraine</option>
	<option value="United Arab Emirates">United Arab Emirates</option>
	<option value="United Kingdom">United Kingdom</option>
	<option value="United States" selected="selected">United States</option>
	<option value="United States Outlying Islands">United States Outlying Islands</option>
	<option value="Uruguay">Uruguay</option>
	<option value="Uzbekistan">Uzbekistan</option>
	<option value="Vanuatu">Vanuatu</option>
	<option value="Venezuela">Venezuela</option>
	<option value="Viet Nam">Viet Nam</option>
	<option value="Virgin Islands, British">Virgin Islands, British</option>
	<option value="Virgin Islands, U.S.">Virgin Islands, U.S.</option>
	<option value="Wallis And Futuna">Wallis And Futuna</option>
	<option value="Western Sahara">Western Sahara</option>
	<option value="Yemen">Yemen</option>
	<option value="Zambia">Zambia</option>
	<option value="Zimbabwe">Zimbabwe</option>
                                    </select>

                                </div>
                            </div>

                            

                            

                            <div class="form-group"><label class="col-md-4 control-label" for="tags">Tags</label>

                                <div class="col-lg-8">
                                    
                                    <select name="tags[]" id="tags" class="form-control" multiple="multiple">
                                        

                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-12">


                            <div class="form-group"><label class="col-md-4 control-label" for="currency">Currency</label>

                                <div class="col-lg-8">
                                    <select id="currency" name="currency" class="form-control">

                                                                                    <option value="1"
                                                    selected="selected" >USD</option>
                                            

                                    </select>
                                </div>
                            </div>

                            <div class="form-group"><label class="col-md-4 control-label" for="group">Group</label>

                                <div class="col-lg-8">
                                    <select class="form-control" name="group" id="group">
                                        <option value="0">None</option>
                                        
                                    </select>
                                    <span class="help-block"><a href="#" id="add_new_group">Add New Group</a> </span>
                                </div>
                            </div>


                            <div class="form-group"><label class="col-md-4 control-label" for="password">Password</label>

                                <div class="col-lg-8"><input type="password" id="password" name="password" class="form-control">

                                </div>
                            </div>

                            <div class="form-group"><label class="col-md-4 control-label" for="cpassword">Confirm Password</label>

                                <div class="col-lg-8"><input type="password" id="cpassword" name="cpassword" class="form-control">

                                </div>
                            </div>


                            <div class="form-group"><label class="col-md-4 control-label" for="send_client_signup_email">Welcome Email</label>

                                <div class="col-lg-8">


                                    <input type="checkbox" checked data-toggle="toggle" data-size="small" data-on="Yes" data-off="No" id="send_client_signup_email" name="send_client_signup_email" value="Yes">


                                    <span class="help-block">Set Yes to send Client Signup Email.</span>

                                </div>
                            </div>



                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <div class="col-md-offset-2 col-lg-10">

                                    <button class="md-btn md-btn-primary waves-effect waves-light" type="submit" id="submit"><i class="fa fa-check"></i> Save</button> | <a href="">Or Cancel</a>


                                </div>
                            </div>
                        </div>
                    </div>


                </form>
            </div>
        </div>
    </div>
</div>


</div>

<input type="hidden" name="_msg_add_new_group" id="_msg_add_new_group" value="Add New Group">
<input type="hidden" name="_msg_group_name" id="_msg_group_name" value="Group Name">

<div id="ajax-modal" class="modal container fade-scale" tabindex="-1" style="display: none;"></div>
</div>

    
        
</div>

<div id="right-sidebar">
    <div class="sidebar-container">

        <ul class="nav nav-tabs navs-3">

            <li class="active"><a data-toggle="tab" href="#tab-1">
                    Notes
                </a></li>
            
                    
                
            <li class=""><a data-toggle="tab" href="#tab-3">
                    <i class="fa fa-gear"></i>
                </a></li>
        </ul>

        <div class="tab-content">


            <div id="tab-1" class="tab-pane active">

                <div class="sidebar-title">
                    <h3> <i class="fa fa-file-text-o"></i> Quick Notes</h3>

                </div>

                <div style="padding: 10px">

                    <form class="form-horizontal push-10-t push-10" method="post" onsubmit="return false;">

                        <div class="form-group">
                            <div class="col-xs-12">
                                <div class="form-material floating">
                                    <textarea class="form-control" id="ib_admin_notes" name="ib_admin_notes" rows="10"></textarea>
                                    <label for="ib_admin_notes">What's on your mind?</label>
                                </div>

                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <button class="btn btn-sm btn-success" type="submit" id="ib_admin_notes_save"><i class="fa fa-check"></i> Save</button>
                            </div>
                        </div>
                    </form>
                    </div>




            </div>

            

                
                    
                    
                

                
                    
                    
                        
                            
                            
                            

                            
                            
                                
                            
                            
                        
                    

                

            

            <div id="tab-3" class="tab-pane">

                <div class="sidebar-title">
                    <h3><i class="fa fa-gears"></i> Settings</h3>

                </div>

                <div class="setings-item">
                    <h4>Theme Color</h4>

                    <ul id="ib_theme_color" class="ib_theme_color">

                        <li><a href=""><span class="light"></span></a></li>
                        <li><a href=""><span class="blue"></span></a></li>
                        <li><a href=""><span class="dark"></span></a></li>
                    </ul>


                </div>
                <div class="setings-item">
                    <span>
                        Fold Sidebar by Default ?
                    </span>
                    <div class="switch">
                        <div class="onoffswitch">
                            <input type="checkbox" name="r_fold_sidebar"  class="onoffswitch-checkbox" id="r_fold_sidebar">
                            <label class="onoffswitch-label" for="r_fold_sidebar">
                                <span class="onoffswitch-inner"></span>
                                <span class="onoffswitch-switch"></span>
                            </label>
                        </div>
                    </div>
                </div>


            </div>
        </div>

    </div>



</div>

</div>
</section>
<!-- BEGIN PRELOADER -->
<input type="hidden" id="_url" name="_url" value="">
<input type="hidden" id="_df" name="_df" value="Y-m-d">
<input type="hidden" id="_lan" name="_lan" value="en">
<!-- END PRELOADER -->
<!-- Mainly scripts -->
<script src="js/jquery-1.10.2.js"></script>
<script src="js/.4.min.js"></script>
<script>
    var _L = [];


    var base_url = 'http://demo.tryib.com/';

        var config_animate = 'No';
        
_L['Working'] = 'Working';
 
</script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.metisMenu.js"></script>
<script src="js/jquery.slimscroll.min.js"></script>


    <script src="js/lib/moment-with-locales.min.js"></script>

    <script>
        moment.locale('en');
    </script>

    

<script src="js/lib/blockui.js"></script>
<script src="js/lib/app.js"></script>
<script src="js/lib/bootstrap-toggle.min.js"></script>

<script src="js/lib/progress.js"></script>
<script src="js/lib/bootbox.min.js"></script>
<!-- iCheck -->
<script src="js/lib/icheck.min.js"></script>
<script src="js/lib/theme.js"></script>
    <script type="js/lib/select2.min.js"></script>
        <script type="text/javascript" src="js/lib/en.js"></script>
        <script type="text/javascript" src="js/lib/add-contact.js"></script>
        
<script>
    jQuery(document).ready(function() {
        // initiate layout and plugins

        matForms();



                
 $("#country").select2({
 theme: "bootstrap"
 });
 
        
    });

</script>
</body>

</html>
