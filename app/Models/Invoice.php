<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class Invoice extends Model
{
    //

    protected $table = "invoices";

    use SoftDeletes;

 //   protected $dates = ['deleted_at'];




    public static function getDailyReport(){

        $daily_report = new Invoice();

        $total_invoice=DB::table('invoices')
            ->select(DB::raw('SUM(total_price) as total_amount'),DB::raw('COUNT(id) as total_invoice'))
            ->whereDate('updated_at', date("Y-m-d"))
            ->first();

        $daily_report->total_invoice = $total_invoice->total_invoice;
        $daily_report->total_amount = $total_invoice->total_amount;


        $total_paid=DB::table('invoices')
            ->select(DB::raw('SUM(total_price) as paid_amount'),DB::raw('COUNT(id) as total_paid'))
            ->where('status','paid')
            ->whereDate('updated_at', date("Y-m-d"))
            ->first();

        $daily_report->total_paid = $total_paid->total_paid;
        $daily_report->paid_amount = $total_paid->paid_amount;

        $total_unpaid=DB::table('invoices')
            ->select(DB::raw('SUM(total_price) as unpaid_amount'),DB::raw('COUNT(id) as total_unpaid'))
            ->where('status','unpaid')
            ->whereDate('updated_at', date("Y-m-d"))
            ->first();

        $daily_report->total_unpaid = $total_unpaid->total_unpaid;
        $daily_report->unpaid_amount = $total_unpaid->unpaid_amount;


        $total_partial_paid=DB::table('invoices')
            ->select(DB::raw('SUM(total_price) as partial_paid_amount'),DB::raw('COUNT(id) as total_partial_paid'))
            ->where('status','partial Paid')
            ->whereDate('updated_at', date("Y-m-d"))
            ->first();

        $daily_report->total_partial_paid = $total_partial_paid->total_partial_paid;
        $daily_report->partial_paid_amount = $total_partial_paid->partial_paid_amount;


        $total_cancel =DB::table('invoices')
            ->select(DB::raw('SUM(total_price) as cancel_amount'),DB::raw('COUNT(id) as total_cancel'))
            ->where('status','cancelled')
            ->whereDate('updated_at', date("Y-m-d"))
            ->first();



        $daily_report->cancel_amount = $total_cancel->cancel_amount;
        $daily_report->total_cancel = $total_cancel->total_cancel;

        return $daily_report;
    }

    public static function getMonthlyReport(){


        //one day (today)
        // $date = Carbon::now()->startOfDay;

        //one month / 30 days
        //  $date = Carbon::now()->subDays(30)->startOfDay;

        //   $invoices =  Invoice::where('updated_at', '>=', $date)->sum('total_price');

        $monthly_report = new Invoice();

        $last_month = date("m")-1;

        $total_invoice=DB::table('invoices')
            ->select(DB::raw('SUM(total_price) as total_amount'),DB::raw('COUNT(id) as total_invoice'))
            ->whereMonth('updated_at', $last_month)
            ->first();

        $monthly_report->total_invoice = $total_invoice->total_invoice;
        $monthly_report->total_amount = $total_invoice->total_amount;


        $total_paid=DB::table('invoices')
            ->select(DB::raw('SUM(total_price) as paid_amount'),DB::raw('COUNT(id) as total_paid'))
            ->where('status','paid')
            ->whereMonth('updated_at',$last_month)
            ->first();

        $monthly_report->total_paid = $total_paid->total_paid;
        $monthly_report->paid_amount = $total_paid->paid_amount;

        $total_unpaid=DB::table('invoices')
            ->select(DB::raw('SUM(total_price) as unpaid_amount'),DB::raw('COUNT(id) as total_unpaid'))
            ->where('status','unpaid')
            ->whereMonth('updated_at',$last_month)
            ->first();

        $monthly_report->total_unpaid = $total_unpaid->total_unpaid;
        $monthly_report->unpaid_amount = $total_unpaid->unpaid_amount;


        $total_partial_paid=DB::table('invoices')
            ->select(DB::raw('SUM(total_price) as partial_paid_amount'),DB::raw('COUNT(id) as total_partial_paid'))
            ->where('status','partial Paid')
            ->whereMonth('updated_at',$last_month)
            ->first();

        $monthly_report->total_partial_paid = $total_partial_paid->total_partial_paid;
        $monthly_report->partial_paid_amount = $total_partial_paid->partial_paid_amount;


        $total_cancel =DB::table('invoices')
            ->select(DB::raw('SUM(total_price) as cancel_amount'),DB::raw('COUNT(id) as total_cancel'))
            ->where('status','cancelled')
            ->whereMonth('updated_at',$last_month)
            ->first();



        $monthly_report->cancel_amount = $total_cancel->cancel_amount;
        $monthly_report->total_cancel = $total_cancel->total_cancel;

        return $monthly_report;


    }



    public static function getYearlyReport(){

        $yearly_report = new Invoice();
        $date = Carbon::now()->subDays(365);

        $total_invoice=DB::table('invoices')
            ->select(DB::raw('SUM(total_price) as total_amount'),DB::raw('COUNT(id) as total_invoice'))
            ->whereDate('updated_at','>=', $date)
            ->first();

        $yearly_report->total_invoice = $total_invoice->total_invoice;
        $yearly_report->total_amount = $total_invoice->total_amount;


        $total_paid=DB::table('invoices')
            ->select(DB::raw('SUM(total_price) as paid_amount'),DB::raw('COUNT(id) as total_paid'))
            ->where('status','paid')
            ->whereDate('updated_at','>=', $date)
            ->first();

        $yearly_report->total_paid = $total_paid->total_paid;
        $yearly_report->paid_amount = $total_paid->paid_amount;

        $total_unpaid=DB::table('invoices')
            ->select(DB::raw('SUM(total_price) as unpaid_amount'),DB::raw('COUNT(id) as total_unpaid'))
            ->where('status','unpaid')
            ->whereDate('updated_at','>=', $date)
            ->first();

        $yearly_report->total_unpaid = $total_unpaid->total_unpaid;
        $yearly_report->unpaid_amount = $total_unpaid->unpaid_amount;


        $total_partial_paid=DB::table('invoices')
            ->select(DB::raw('SUM(total_price) as partial_paid_amount'),DB::raw('COUNT(id) as total_partial_paid'))
            ->where('status','partial Paid')
            ->whereDate('updated_at','>=', $date)
            ->first();

        $yearly_report->total_partial_paid = $total_partial_paid->total_partial_paid;
        $yearly_report->partial_paid_amount = $total_partial_paid->partial_paid_amount;


        $total_cancel =DB::table('invoices')
            ->select(DB::raw('SUM(total_price) as cancel_amount'),DB::raw('COUNT(id) as total_cancel'))
            ->where('status','cancelled')
            ->whereDate('updated_at','>=', $date)
            ->first();



        $yearly_report->cancel_amount = $total_cancel->cancel_amount;
        $yearly_report->total_cancel = $total_cancel->total_cancel;

        return $yearly_report;
    }




    public static function getLifeTimeReport(){

        $invoices = new Invoice();

        $total_invoice=DB::table('invoices')
            ->select(DB::raw('SUM(total_price) as total_amount'),DB::raw('COUNT(id) as total_invoice'))

            ->first();

        $invoices->total_invoice = $total_invoice->total_invoice;
        $invoices->total_amount = $total_invoice->total_amount;


        $total_paid=DB::table('invoices')
            ->select(DB::raw('SUM(total_price) as paid_amount'),DB::raw('COUNT(id) as total_paid'))
            ->where('status','paid')
            ->first();

        $invoices->total_paid = $total_paid->total_paid;
        $invoices->paid_amount = $total_paid->paid_amount;

        $total_unpaid=DB::table('invoices')
            ->select(DB::raw('SUM(total_price) as unpaid_amount'),DB::raw('COUNT(id) as total_unpaid'))
            ->where('status','unpaid')
            ->first();

        $invoices->total_unpaid = $total_unpaid->total_unpaid;
        $invoices->unpaid_amount = $total_unpaid->unpaid_amount;


        $total_partial_paid=DB::table('invoices')
            ->select(DB::raw('SUM(total_price) as partial_paid_amount'),DB::raw('COUNT(id) as total_partial_paid'))
            ->where('status','partial Paid')
            ->first();

        $invoices->total_partial_paid = $total_partial_paid->total_partial_paid;
        $invoices->partial_paid_amount = $total_partial_paid->partial_paid_amount;


        $total_cancel =DB::table('invoices')
            ->select(DB::raw('SUM(total_price) as cancel_amount'),DB::raw('COUNT(id) as total_cancel'))
            ->where('status','cancelled')
            ->first();



        $invoices->cancel_amount = $total_cancel->cancel_amount;
        $invoices->total_cancel = $total_cancel->total_cancel;

        return $invoices;


    }
}
