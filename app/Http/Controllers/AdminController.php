<?php

namespace App\Http\Controllers;
use DB;
use Session;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Illuminate\Support\Facades;
use App\Models\Invoice;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        $data = array();

        $lifetime_report = Invoice::getLifeTimeReport();
        $daily_report = Invoice::getDailyReport();
        $month_report = Invoice::getMonthlyReport();
        $year_report = Invoice::getYearlyReport();


/*
        $last_month_amount = DB::table("invoices")
            ->select(DB::raw('SUM(total_price) as last_month_amount'))
            ->where('due_date','<=', Carbon::now()->subMonths(1))
            ->first();


        $today_amount = DB::table("invoices")
            ->select('total_price')
            ->where('due_date', date("Y-m-d"))
            ->sum('total_price');


        $current_month_amount = DB::table("invoices")
            ->select(DB::raw('SUM(total_price) as current_month_amount'))
            ->where('due_date','>=', Carbon::now()->subMonths(1))
            ->first();


        ->with('last_month_amount', $last_month_amount)
        ->with('today_amount', $today_amount)
        ->with('current_month_amount', $current_month_amount)  */

        return view('admin.pages.dashboard')
                    ->with('lifetime_report', $lifetime_report)
                    ->with('daily_report', $daily_report)
                    ->with('month_report', $month_report)
                    ->with('year_report', $year_report);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
