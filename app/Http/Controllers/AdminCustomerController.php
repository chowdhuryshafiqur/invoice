<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Support\Facades\Session;
use Validator;
use Illuminate\Support\Facades\Redirect;


use Illuminate\Http\Request;

class AdminCustomerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }



    public function add_customer(){

        $countries = DB::table('countries')
                        ->select('id','countryName')
                        ->get();

        return view('admin.pages.add_customer')
            ->with('countries', $countries);
    }


    public function save_customer(Request $request){
        $validator = Validator::make($request->all(), [

            'customer_name' => 'required',
            'phone' => 'required',
            'address' => 'required',
            'country_id' => 'required',
        ]);

        if ($validator->fails()) {

            $request->session()->flash('error', "Must Fill up required Field");

            return redirect()->back()->withErrors($validator)->withInput();
        } else {

            $data = array();
            $data['customer_name'] = $request->customer_name;
            $data['email'] = $request->email;
            $data['phone'] = $request->phone;
            $data['address'] = $request->address;
            $data['city'] = $request->city;
            $data['zip_code'] = $request->zip_code;
            $data['country_id'] = $request->country_id;
            $data['created_at'] = date("Y-m-d H:i:s");
            $data['updated_at'] = date("Y-m-d H:i:s");


            $insert_table = DB::table('customers')->insert($data);
            if ($insert_table) {
                $request->session()->flash('message', 'Save Customer Information successfully');
            } else {
                $request->session()->flash('error', 'Unable to Add Customer Information!');
            }
            return Redirect::to('/add-customer');
        }

    }



    public function save_customer_ajax(Request $request){
        $validator = Validator::make($request->all(), [

            'customer_name' => 'required',
            'phone' => 'required',
            'address' => 'required',
            'country_id' => 'required',
        ]);

        if ($validator->fails()) {

            $request->session()->flash('error', "Must Fill up required Field");

            return redirect()->back()->withErrors($validator)->withInput();
        } else {

            $data = array();
            $data['customer_name'] = $request->customer_name;
            $data['email'] = $request->email;
            $data['phone'] = $request->phone;
            $data['address'] = $request->address;
            $data['city'] = $request->city;
            $data['zip_code'] = $request->zip_code;
            $data['country_id'] = $request->country_id;
            $data['updated_at'] = date("Y-m-d H:i:s");


            $insert_table = DB::table('customers')->insert($data);
            if ($insert_table) {
                $request->session()->flash('message', 'Save Customer Information successfully');
            } else {
                $request->session()->flash('error', 'Unable to Add Customer Information!');
            }

            $customers = DB::table('customers')
                ->select('*')
                ->orderBy('customer_id','desc')
                ->first();

            $country = DB::table('countries')
                ->select('id','countryName')
                ->where('id',$customers->country_id)
                ->first();

            Session::put('customer_id',$customers->customer_id);
            Session::put('invoice_address',$customers->address.', '.$customers->city.', '.$country->countryName);


            return redirect()->back();
        }

    }

    public function list_customer(){
        $all_customer_info=DB::table('customers')
            ->orderBy('customer_id',"desc")->paginate(20);

        return view('admin.pages.list_customer')
            ->with('all_customer_info',$all_customer_info);
    }

    public function delete_customer($customer_id){
        //print_r($customer_id);
        //exit;
        $deleted =  DB::table('customers')
            ->where('customer_id',$customer_id)
            ->delete();

        if($deleted)
            session()->flash('message','Delete Data successfully');
        else
            session()->flash('error','Unable to delete data');

        return Redirect::to('/list-customer');
    }


    public function edit_customer($id)
    {
        $edit_customer = DB::table('customers')
                        ->where('customer_id',$id)
                        ->first();

        $countries = DB::table('countries')->select('id','countryName')->get();
        return view('admin.pages.edit_customer')
                        ->with('edit_customer', $edit_customer)
                        ->with('countries', $countries);
    }

    public function update_customer(Request $request){

        $validator = Validator::make($request->all(), [

            'customer_name' => 'required',
            'phone' => 'required',
            'address' => 'required',
            'country_id' => 'required',
        ]);

        if ($validator->fails()) {

            $request->session()->flash('error', "Must Fill up required Field");

            return redirect()->back()->withErrors($validator)->withInput();
        } else {

            $data = array();
            $customer_id = $request->customer_id;
            $data['customer_name'] = $request->customer_name;
            $data['email'] = $request->email;
            $data['phone'] = $request->phone;
            $data['address'] = $request->address;
            $data['city'] = $request->city;
            $data['zip_code'] = $request->zip_code;
            $data['country_id'] = $request->country_id;
            $data['created_at'] = date("Y-m-d H:i:s");
            $data['updated_at'] = date("Y-m-d H:i:s");


            $update = DB::table('customers')
                ->where('customer_id', $customer_id)
                ->update($data);

            if ($update) {
                $request->session()->flash('message', 'Customer Information update successfully');
            } else {
                $request->session()->flash('error', 'Unable to update Customer Information!');
            }

            return Redirect::to('/list-customer');

        }

    }

    public function select_customer_info($id){
        $data = DB::table('customers')
                    ->where('customer_id', $id)
                    ->first();

        $country = DB::table('countries')
            ->select('id','countryName')
            ->where('id',$data->country_id)
            ->first();

        echo $data->address.", ". $data->city.", ".$country->countryName;

    }

}
