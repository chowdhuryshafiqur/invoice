<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Session;
use phpDocumentor\Reflection\Types\Null_;
use Validator;
use Illuminate\Support\Facades\Redirect;
use PDF;



class AdminInvoiceController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }



    public function create_invoice(){

        $countries = DB::table('countries')
            ->select('id','countryName')
            ->get();

        $companies = DB::table('companies')
            ->select('id','company_name')
            ->get();

        $customers = DB::table('customers')
            ->select('*')
            ->orderBy('customer_id','desc')
            ->get();



        return view('admin.pages.create_invoice1')
            ->with('countries', $countries)
            ->with('customers', $customers)
            ->with('companies', $companies);
    }


    public function add_invoice(){

        $countries = DB::table('countries')
            ->select('id','countryName')
            ->get();

        $companies = DB::table('companies')
            ->select('id','company_name')
            ->get();

        $customers = DB::table('customers')
            ->select('*')
            ->orderBy('customer_id','desc')
            ->get();

        return view('admin.pages.add_invoice')
            ->with('countries', $countries)
            ->with('customers', $customers)
            ->with('companies', $companies);
    }

    public function save_invoice(Request $request){
        $validator = Validator::make($request->all(), [
            'product' => 'required',
            'customer_id' => 'required',
            'company_id' => 'required',
            'qty'=>'required',
            'price'=>'required',
            'invoice_date'=>'required',
            'shipping_method'=>'required'
        ]);

        if ($validator->fails()) {

            $request->session()->flash('error', "Must Fill up required Field");

            return redirect()->back()->withErrors($validator)->withInput();
        } else {

            $data = array();
            $data['invoice_code'] = rand();
            $total_product = count($request['product']);
            $data['invoice_date'] = $request->invoice_date;
            $data['due_date'] = date("Y-m-d");

            //$data['company_id'] = $request->company_id;
            $data['company_id'] = 1;
            $data['created_by'] = $request->user_id;
            $data['customer_id'] = $request->customer_id;
            $data['notes'] = $request->notes;
            $data['shipping_method'] = $request->shipping_method;
            $data['delivery_charge'] = $request->delivery_charge;

            $data['status'] = $request->status;
            if ($request->status == "partial paid") {
                $data['advance_payment'] = $request->advance_payment;
             }else if ($request->status == "paid"){
                $data['advance_payment'] = 0;
             }else{
                  $data['advance_payment']=NULL;
             }

            $data['total_price'] = 0;
             $data['created_at'] = date("Y-m-d H:i:s");
            $data['updated_at'] = date("Y-m-d H:i:s");


            $insert_invoice = DB::table('invoices')->insert($data);

            if($insert_invoice){
                $invoice = DB::table('invoices')
                            ->select('id')
                            ->orderBy('id','desc')
                            ->first();
            }

            $discount_data = array();

            if(!empty($request->discount) && !empty($request->discount_type)){
                $discount_data['discount'] = $request->discount;
                $discount_data['discount_type'] = $request->discount_type;
                $discount_data['invoice_id'] = $invoice->id;
                $discount_data['created_at'] = date("Y-m-d H:i:s");
                $discount_data['updated_at'] = date("Y-m-d H:i:s");

                DB::table('discounts')->insert($discount_data);
            }

            $product_data = array();

            $total_price = 0;

            for($i=0; $i<=$total_product-1; $i++) {
                $product_data['product_code'] = $request->item_code[$i];
                $product_data['products'] = $request->product[$i];
                $product_data['price'] = $request->price[$i];
                $product_data['quantity'] = $request->qty[$i];
                $product_data['invoice_id'] = $invoice->id;
                $product_data['created_at'] = date("Y-m-d H:i:s");
                $product_data['updated_at'] = date("Y-m-d H:i:s");
                $total_price += $request->price[$i]*$request->qty[$i];
                $insert_products = DB::table('products')->insert($product_data);
            }


            if($insert_products){

                if ($request->status == "paid"){
                    DB::table('invoices')->where('id',$invoice->id)->update(['advance_payment' => $total_price]);
                }
                $total = DB::table('invoices')->where('id',$invoice->id)->update(['total_price' => $total_price]);


            }


            if ($insert_invoice && $insert_products && $total) {
                $request->session()->flash('message', 'Save Invoice Information successfully');
            } else {
                $request->session()->flash('error', 'Unable to Add Invoice Information!');
            }
            return Redirect::to('/view-invoice/'.$invoice->id);
        }

    }

    public function edit_invoice($id){

        $countries = DB::table('countries')
            ->select('id','countryName')
            ->get();

        $companies = DB::table('companies')
            ->select('id','company_name')
            ->get();

        $products = DB::table('products')
            ->select('*')
            ->where('invoice_id',$id)
            ->get();

        $customers = DB::table('customers')
            ->select('*')
            ->orderBy('customer_id','desc')
            ->get();



        $edit_invoice = DB::table('invoices')
            ->leftjoin('customers', 'invoices.customer_id', '=', 'customers.customer_id')
            ->leftjoin('discounts','invoices.id','=','discounts.invoice_id')
            ->select('invoices.*','invoices.id AS invoices_id', 'discounts.*','customers.*')
            ->where('invoices.id',$id)
            ->first();

        return view('admin.pages.edit_invoice')
            ->with('countries', $countries)
            ->with('customers', $customers)
            ->with('companies', $companies)
            ->with('products',$products)
            ->with('edit_invoice', $edit_invoice);
    }


    public function update_invoice(Request $request){
        $validator = Validator::make($request->all(), [
            'product' => 'required',
            'customer_id' => 'required',
            'company_id' => 'required',
            'qty'=>'required',
            'price'=>'required',
            'invoice_date'=>'required',
            'shipping_method'=>'required'
        ]);

        if ($validator->fails()) {

            $request->session()->flash('error', "Must Fill up required Field");

            return redirect()->back()->withErrors($validator)->withInput();
        } else {

            $data = array();
            // $data['invoice_code'] = rand();
            $total_product = count($request->product);
            $data['invoice_date'] = $request->invoice_date;

            $data['updated_by']=$request->updated_by;
            $data['due_date'] = date("Y-m-d");

            $data['company_id'] = $request->company_id;
            $data['customer_id'] = $request->customer_id;
            $data['notes'] = $request->notes;
            $data['shipping_method'] = $request->shipping_method;
            $data['delivery_charge'] = $request->delivery_charge;

            $data['status']= $request->status;

            $data['advance_payment']=$request->advance_payment;

            $data['total_price'] = $request->total_amount;
            $data['updated_at'] = date("Y-m-d H:i:s");


            $update_invoice = DB::table('invoices')->where('id',$request->invoice_id)->update($data);


            $discount_data = array();

            if(!empty($request->discount) && !empty($request->discount_type)){
                $discount_data['discount'] = $request->discount;
                $discount_data['discount_type'] = $request->discount_type;
                $discount_data['updated_at'] = date("Y-m-d H:i:s");

                DB::table('discounts')->where('invoice_id',$request->invoice_id)->update($discount_data);
            }

            $product_data = array();
            $total_price=0;
            for($i=0; $i<=$total_product-1; $i++) {
                $product_data['product_code'] = $request->item_code[$i];
                $product_data['products'] = $request->product[$i];
                $product_data['price'] = $request->price[$i];
                $product_data['quantity'] = $request->qty[$i];
                $product_data['updated_at'] = date("Y-m-d H:i:s");

                $total_price += $request->price[$i]*$request->qty[$i];
                $update_products = DB::table('products')->where('id',$request->product_id[$i])->update($product_data);
            }

            if($update_products){

                if ($request->status == "paid"){
                    $total = DB::table('invoices')->where('id',$request->invoice_id)->update(['advance_payment' => $total_price]);
                }
                DB::table('invoices')->where('id',$request->invoice_id)->update(['total_price' => $total_price]);
            }




            if ($update_invoice || $update_products) {
                $request->session()->flash('message', 'Update Invoice Information successfully');
            } else {
                $request->session()->flash('error', 'Unable to update Invoice Information!');
            }
            return Redirect::to('/view-update/'.$request->invoice_id);
        }
    }

       public function list_invoice(){


        $invoice_number = DB::table('system_setting')
            ->select('invoice_number')
            ->orderBy('id','desc')->first();

          // $invoice_number->invoice_number = $invoice_number->invoice_number-1;

        $all_invoice_list=DB::table('invoices')
           ->join('customers', 'invoices.customer_id', '=', 'customers.customer_id')
           ->join('companies', 'invoices.company_id', '=', 'companies.id')
            ->leftjoin('discounts','invoices.id','=','discounts.invoice_id')
            ->select('invoices.id','invoices.*','invoices.id AS invoice_id', 'customers.*' ,'companies.company_name')

            ->orderBy('invoice_id',"desc")
            ->paginate(20);

/*        if(!$invoice_number)
            $invoice_number->invoice_number = 0;
*/
           return view('admin.pages.list_invoice')->with('all_invoice_list',$all_invoice_list)
                                                    ->with('invoice_number',$invoice_number);
       }

    public function unpaid_invoices(){

        $invoice_number = DB::table('system_setting')
            ->select('invoice_number')
            ->orderBy('id','desc')->first();

        $all_invoice_list=DB::table('invoices')
            ->join('customers', 'invoices.customer_id', '=', 'customers.customer_id')
            ->join('companies', 'invoices.company_id', '=', 'companies.id')
            ->leftjoin('discounts','invoices.id','=','discounts.invoice_id')
            ->select('invoices.*','invoices.id AS invoice_id', 'customers.*' ,'companies.company_name')
            ->where('status','unpaid')
            ->orderBy('id',"desc")
            ->paginate(20);

        return view('admin.pages.unpaid_invoice')->with('all_invoice_list',$all_invoice_list)->with('invoice_number',$invoice_number);
    }


    public function paid_invoices(){
        $invoice_number = DB::table('system_setting')
            ->select('invoice_number')
            ->orderBy('id','desc')->first();

        $all_invoice_list=DB::table('invoices')
            ->join('customers', 'invoices.customer_id', '=', 'customers.customer_id')
            ->join('companies', 'invoices.company_id', '=', 'companies.id')
            ->leftjoin('discounts','invoices.id','=','discounts.invoice_id')
            ->select('invoices.*','invoices.id AS invoice_id', 'customers.*' ,'companies.company_name')
            ->where('status','paid')
            ->orderBy('id',"desc")
            ->paginate(20);

        return view('admin.pages.paid_invoice')->with('all_invoice_list',$all_invoice_list)
                                                ->with('invoice_number',$invoice_number);
    }


    public function cancel_invoices(){
        $invoice_number = DB::table('system_setting')
            ->select('invoice_number')
            ->orderBy('id','desc')->first();

        $all_invoice_list=DB::table('invoices')
            ->join('customers', 'invoices.customer_id', '=', 'customers.customer_id')
            ->join('companies', 'invoices.company_id', '=', 'companies.id')
            ->leftjoin('discounts','invoices.id','=','discounts.invoice_id')
            ->select('invoices.*','invoices.id AS invoice_id', 'customers.*' ,'companies.company_name')
            ->where('status','cancelled')
            ->orderBy('id',"desc")
            ->paginate(20);

        return view('admin.pages.cancel_invoice')
            ->with('all_invoice_list',$all_invoice_list)
            ->with('invoice_number',$invoice_number);
    }

    public function partial_paid_invoices(){
        $invoice_number = DB::table('system_setting')
            ->select('invoice_number')
            ->orderBy('id','desc')->first();

        $all_invoice_list=DB::table('invoices')
            ->join('customers', 'invoices.customer_id', '=', 'customers.customer_id')
            ->join('companies', 'invoices.company_id', '=', 'companies.id')
            ->leftjoin('discounts','invoices.id','=','discounts.invoice_id')
            ->select('invoices.*','invoices.id AS invoice_id', 'customers.*' ,'companies.company_name')
            ->where('status','partial paid')
            ->orderBy('id',"desc")
            ->paginate(20);

        return view('admin.pages.partial_paid_invoice')
            ->with('all_invoice_list',$all_invoice_list)
            ->with('invoice_number',$invoice_number);
    }



    public function delete_invoice($id){

           $deleted=DB::table('invoices')
               ->where('id',$id)
               ->delete();

         if($deleted)
             session()->flash('message','Delete Data successfully');
         else
             session()->flash('error','Unable to delete data');

         return Redirect::to('/list-invoice');
     }




    public function view_invoice($id){
        $invoice_setting = DB::table('system_setting')
            ->select('*')
            ->orderBy('id','desc')->first();



        $view_invoice = DB::table('invoices')
                        ->leftjoin('customers', 'invoices.customer_id', '=', 'customers.customer_id')
                        ->leftjoin('companies', 'invoices.company_id', '=', 'companies.id')
                        ->leftjoin('discounts','invoices.id','=','discounts.invoice_id')
                        ->select('invoices.*','invoices.id AS invoices_id', 'customers.*' ,'companies.*','discounts.*','companies.address AS company_address','companies.city AS company_city','companies.country_id as company_country','customers.address AS customer_address','customers.city AS customer_city','customers.country_id as customer_country','customers.email as customer_email','customers.phone as customer_phone','customers.zip_code as customer_zip_code')
                        ->where('invoices.id',$id)
                        ->first();



 //       ->join('customers','customers.country_id','=','countries.id')
 //           ->join('companies','companies.country_id','=','countries.id')


        return view('admin.pages.invoice')
                        ->with('view_invoice',$view_invoice)
                        ->with('invoice_number',$invoice_setting);
    }

    public function view_update($id){

        $invoice_number = DB::table('system_setting')
            ->select('*')
            ->orderBy('id','desc')->first();

        $view_invoice = DB::table('invoices')
            ->leftjoin('customers', 'invoices.customer_id', '=', 'customers.customer_id')
            ->leftjoin('companies', 'invoices.company_id', '=', 'companies.id')
            ->leftjoin('discounts','invoices.id','=','discounts.invoice_id')
            ->select('invoices.*','invoices.id AS invoices_id', 'customers.*' ,'companies.*','discounts.*','companies.address AS company_address','companies.city AS company_city','companies.country_id as company_country','customers.address AS customer_address','customers.city AS customer_city','customers.country_id as customer_country','customers.email as customer_email','customers.phone as customer_phone','customers.zip_code as customer_zip_code')
            ->where('invoices.id',$id)
            ->first();



        //       ->join('customers','customers.country_id','=','countries.id')
        //           ->join('companies','companies.country_id','=','countries.id')


        return view('admin.pages.view_update')
            ->with('view_invoice',$view_invoice)->with('invoice_number',$invoice_number);
    }


    public function preview_invoice($id){

        $invoice_number = DB::table('system_setting')
            ->select('*')
            ->orderBy('id','desc')->first();

        $view_invoice = DB::table('invoices')
            ->leftjoin('customers', 'invoices.customer_id', '=', 'customers.customer_id')
            ->leftjoin('companies', 'invoices.company_id', '=', 'companies.id')
            ->leftjoin('discounts','invoices.id','=','discounts.invoice_id')
            ->select('invoices.*','invoices.id AS invoices_id', 'customers.*' ,'companies.*','discounts.*','companies.address AS company_address','companies.city AS company_city','companies.country_id as company_country','customers.address AS customer_address','customers.city AS customer_city','customers.country_id as customer_country','customers.email as customer_email','customers.phone as customer_phone','customers.zip_code as customer_zip_code')
            ->where('invoices.id',$id)
            ->first();



        return view('admin.pages.preview_invoice')
                    ->with('view_invoice',$view_invoice)->with('invoice_number',$invoice_number);
    }

    public function download_invoice($id){
        $invoice_number = DB::table('system_setting')
            ->select('*')
            ->orderBy('id','desc')->first();

        $number = $invoice_number==""?0:$invoice_number->invoice_number;


        $view_invoice = DB::table('invoices')
            ->leftjoin('customers', 'invoices.customer_id', '=', 'customers.customer_id')
            ->leftjoin('companies', 'invoices.company_id', '=', 'companies.id')
            ->leftjoin('discounts','invoices.id','=','discounts.invoice_id')
            ->select('invoices.*','invoices.id AS invoices_id', 'customers.*' ,'companies.*','discounts.*','companies.address AS company_address','companies.city AS company_city','companies.country_id as company_country','customers.address AS customer_address','customers.city AS customer_city','customers.country_id as customer_country','customers.email as customer_email','customers.phone as customer_phone','customers.zip_code as customer_zip_code')
            ->where('invoices.id',$id)
            ->first();


        $company_country=DB::table('countries')
            ->select('id','countryName','countries.id AS country_id')
            ->where('id',$view_invoice->company_country)
            ->first();

        $customer_country=DB::table('countries')
            ->select('id','countryName')
            ->where('id',$view_invoice->customer_country)
            ->first();


        $data=array();
        $data['invoices_id']=$view_invoice->invoices_id;
        $data['invoice_number']=$view_invoice->invoices_id+$number;
        $data['invoice_prefix']=$invoice_number==""?"":$invoice_number->invoice_prefix;
        $data['invoice_code']=$view_invoice->invoice_code;
        $data['invoice_date']=$view_invoice->invoice_date;
        $data['advance_payment']=$view_invoice->advance_payment;
        $data['due_date']=$view_invoice->due_date;
        $data['total_price']=$view_invoice->total_price;
        $data['delivery_charge']=$view_invoice->delivery_charge;
        $data['notes']=$view_invoice->notes;

        if($view_invoice->status=="paid")
            $data['status']="Paid";
        else
            $data['status']="Unpaid";


        $data['customer_name']=$view_invoice->customer_name;
        $data['customer_phone']=$view_invoice->customer_phone;
        $data['customer_address']=$view_invoice->customer_address;
        $data['customer_city']=$view_invoice->customer_city;
        $data['customer_country']=$customer_country->countryName;
        $data['customer_zip_code']=$view_invoice->zip_code;
        $data['customer_email']=$view_invoice->customer_email;

        $data['company_name']=$view_invoice->company_name;
        $data['company_address']=$view_invoice->company_address;
        $data['company_city']=$view_invoice->company_city;
        $data['company_country']=$company_country->countryName;
        $data['logo']=$view_invoice->logo;
        $data['discount']=$view_invoice->discount;
        $data['discount_type']=$view_invoice->discount_type;
        $data['shipping_method']=$view_invoice->shipping_method;


        $pdf = PDF::loadView('admin.pages.view_invoice_pdf', array('data' => $data));
        $invoice_name = 'INV-'.$view_invoice->invoice_code.'-'.$view_invoice->invoice_date.'.pdf';
        return $pdf->download($invoice_name);
    }

    public function view_invoice_pdf($id){
        $invoice_number = DB::table('system_setting')
            ->select('*')
            ->orderBy('id','desc')->first();


        $number = $invoice_number==""?0:$invoice_number->invoice_number;


        $view_invoice = DB::table('invoices')
            ->leftjoin('customers', 'invoices.customer_id', '=', 'customers.customer_id')
            ->leftjoin('companies', 'invoices.company_id', '=', 'companies.id')
            ->leftjoin('discounts','invoices.id','=','discounts.invoice_id')
            ->select('invoices.*','invoices.id AS invoices_id', 'customers.*' ,'companies.*','discounts.*','companies.address AS company_address','companies.city AS company_city','companies.country_id as company_country','customers.address AS customer_address','customers.city AS customer_city','customers.country_id as customer_country','customers.email as customer_email','customers.phone as customer_phone','customers.zip_code as customer_zip_code')
            ->where('invoices.id',$id)
            ->first();


        $company_country=DB::table('countries')
            ->select('id','countryName','countries.id AS country_id')
            ->where('id',$view_invoice->company_country)
            ->first();

        $customer_country=DB::table('countries')
            ->select('id','countryName')
            ->where('id',$view_invoice->customer_country)
            ->first();


        $data=array();
        $data['invoices_id']=$view_invoice->invoices_id;
        $data['invoice_number']=$view_invoice->invoices_id+$number;
        $data['invoice_prefix']=$invoice_number==""?"":$invoice_number->invoice_prefix;
        $data['invoice_code']=$view_invoice->invoice_code;
        $data['invoice_date']=$view_invoice->invoice_date;
        $data['due_date']=$view_invoice->due_date;
        $data['total_price']=$view_invoice->total_price;
        $data['advance_payment']=$view_invoice->advance_payment;
        $data['delivery_charge']=$view_invoice->delivery_charge;
        $data['notes']=$view_invoice->notes;
        $data['discount']=$view_invoice->discount;
        $data['discount_type']=$view_invoice->discount_type;

        if($view_invoice->status=="paid")
            $data['status']="Paid";
        else
            $data['status']="Unpaid";


        $data['customer_name']=$view_invoice->customer_name;
        $data['customer_phone']=$view_invoice->customer_phone;
        $data['customer_address']=$view_invoice->customer_address;
        $data['customer_city']=$view_invoice->customer_city;
        $data['customer_country']=$customer_country->countryName;
        $data['customer_zip_code']=$view_invoice->zip_code;
        $data['customer_email']=$view_invoice->customer_email;

        $data['company_name']=$view_invoice->company_name;
        $data['company_address']=$view_invoice->company_address;
        $data['company_city']=$view_invoice->company_city;
        $data['company_country']=$company_country->countryName;
        $data['logo']=$view_invoice->logo;
        $data['shipping_method']=$view_invoice->shipping_method;

       $pdf = PDF::loadView('admin.pages.view_invoice_pdf', array('data' => $data));
       $invoice_name = 'INV-'.$view_invoice->invoice_code.'-'.$view_invoice->invoice_date.'.pdf';
       return $pdf->stream($invoice_name);

      //  return view('admin.pages.view_invoice_pdf')
        //    ->with('view_invoice',$view_invoice);

    }

    public function print_view_invoice($id){
        $invoice_number = DB::table('system_setting')
            ->select('*')
            ->orderBy('id','desc')->first();


        $number = $invoice_number==""?0:$invoice_number->invoice_number;



        $view_invoice = DB::table('invoices')
            ->leftjoin('customers', 'invoices.customer_id', '=', 'customers.customer_id')
            ->leftjoin('companies', 'invoices.company_id', '=', 'companies.id')
            ->leftjoin('discounts','invoices.id','=','discounts.invoice_id')
            ->select('invoices.*','invoices.id AS invoices_id', 'customers.*' ,'companies.*','discounts.*','companies.address AS company_address','companies.city AS company_city','companies.country_id as company_country','customers.address AS customer_address','customers.city AS customer_city','customers.country_id as customer_country','customers.email as customer_email','customers.phone as customer_phone','customers.zip_code as customer_zip_code')
            ->where('invoices.id',$id)
            ->first();


        $company_country=DB::table('countries')
            ->select('id','countryName','countries.id AS country_id')
            ->where('id',$view_invoice->company_country)
            ->first();

        $customer_country=DB::table('countries')
            ->select('id','countryName')
            ->where('id',$view_invoice->customer_country)
            ->first();


        $data=array();
        $data['invoices_id']=$view_invoice->invoices_id;
        $data['invoice_number']=$view_invoice->invoices_id+$number;
        $data['invoice_prefix']=$invoice_number==""?"":$invoice_number->invoice_prefix;
        $data['invoice_code']=$view_invoice->invoice_code;
        $data['invoice_date']=$view_invoice->invoice_date;
        $data['due_date']=$view_invoice->due_date;
        $data['total_price']=$view_invoice->total_price;
        $data['advance_payment']=$view_invoice->advance_payment;
        $data['delivery_charge']=$view_invoice->delivery_charge;
        $data['notes']=$view_invoice->notes;
        $data['discount']=$view_invoice->discount;
        $data['discount_type']=$view_invoice->discount_type;
        $data['shipping_method']=$view_invoice->shipping_method;

        if($view_invoice->status=="paid")
            $data['status']="Paid";
        else
            $data['status']="Unpaid";


        $data['customer_name']=$view_invoice->customer_name;
        $data['customer_phone']=$view_invoice->customer_phone;
        $data['customer_address']=$view_invoice->customer_address;
        $data['customer_city']=$view_invoice->customer_city;
        $data['customer_country']=$customer_country->countryName;
        $data['customer_zip_code']=$view_invoice->zip_code;
        $data['customer_email']=$view_invoice->customer_email;

        $data['company_name']=$view_invoice->company_name;
        $data['company_address']=$view_invoice->company_address;
        $data['company_city']=$view_invoice->company_city;
        $data['company_country']=$company_country->countryName;
        $data['logo']=$view_invoice->logo;


        return view('admin.pages.view_invoice_pdf')
            ->with('data',$data);
    }


    public function paid_invoice($id){

        $advance_payment = DB::table('invoices')
            ->select('total_price')
            ->where('id',$id)
            ->first();

            $update = DB::table('invoices')
                            ->where('id',$id)
                            ->update(['status' => 'paid','advance_payment'=> $advance_payment->total_price]);
        if($update)
            session()->flash('message','Update Invoice Status successfully');
        else
            session()->flash('error','Unable to update  Invoice Status data');

        return redirect()->back();

    }

    public function unpaid_invoice($id){

        $update = DB::table('invoices')
            ->where('id',$id)
            ->update(['status' => 'unpaid','advance_payment'=>0]);
        if($update)
            session()->flash('message','Update Invoice Status successfully');
        else
            session()->flash('error','Unable to update  Invoice Status data');

        return redirect()->back();
    }

    public function cancel_invoice($id){

        $update = DB::table('invoices')
            ->where('id',$id)
            ->update(['status' => 'Cancelled']);
        if($update)
            session()->flash('message','Update Invoice Status successfully');
        else
            session()->flash('error','Unable to update  Invoice Status data');

        return redirect()->back();
    }

}
