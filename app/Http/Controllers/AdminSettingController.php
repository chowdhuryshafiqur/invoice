<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Session;

use Illuminate\Support\Facades\Redirect;

class AdminSettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }




    public function index()
    {
        $settings=DB::table('system_setting')->orderby('id','desc')->first();


        $edit=null;

        if($settings)
            $edit="edit";


        return view('admin.pages.system_satting')->with('edit',$edit)->with('settings', $settings);
    }

    public function save_setting(Request $request)
    {


            $data = array();
            $data['invoice_number'] = $request->invoice_number;
            $data['invoice_prefix'] = $request->invoice_prefix;
            $data['create_view'] = $request->create_view;
            $data['created_at'] = date("Y-m-d H:i:s");
            $data['updated_at'] = date("Y-m-d H:i:s");

            $insert_table=DB::table('system_setting')->insert($data);

            if ($insert_table) {
                $request->session()->flash('message', 'Save Setting successfully');
            } else {
                $request->session()->flash('error', 'Unable to Setting Information!');
            }
        return redirect::to('/system-setting');


        }


    public function update_setting(Request $request){
        $data=array();
        $data['invoice_number']=$request->invoice_number;
        $data['invoice_prefix']=$request->invoice_prefix;
        $data['create_view'] = $request->create_view;
        $data['updated_at'] = date("Y-m-d H:i:s");

        $update=DB::table('system_setting')->where('id', $request->id)->update($data);


        if ($update) {
            $request->session()->flash('message', 'Update Setting successfully');
        } else {
            $request->session()->flash('error', 'Unable to Update Setting Information!');
        }

        return redirect::to('/system-setting');
    }

}
