<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Validator;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;


class AdminShippingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }




    public function add_shipping_method(){

        $list_shipping_method=DB::table('shipping_method')->where('deleted_at',0)->paginate(10);
        $edit_shipping = null;

        return view('admin.pages.add_shipping_method')
            ->with('list_shipping_method',$list_shipping_method)

            ->with('edit_shipping', $edit_shipping);

    }

    public function edit_shipping_method($id){

        $list_shipping_method=DB::table('shipping_method')->where('deleted_at',0)->paginate(10);
        $edit =DB::table('shipping_method')->where('id',$id)->first();
        $edit_shipping = "edit";
        return view('admin.pages.add_shipping_method')
            ->with('list_shipping_method',$list_shipping_method)
            ->with('edit_shipping', $edit_shipping)
            ->with('update_shipping', $edit);
    }

    public function save_shipping_method(Request $request)
    {
        $validator = Validator::make($request->all(), [

            'name' => 'required',

        ]);

        if ($validator->fails()) {

            $request->session()->flash('error', "Must Fill up required Field");

            return redirect()->back()->withErrors($validator)->withInput();
        } else {


            $data['name'] = $request->name;
            $data['created_at'] = date("Y-m-d H:i:s");
            $data['updated_at'] = date("Y-m-d H:i:s");
            $insert_table = DB::table('shipping_method')->insert($data);
            if ($insert_table) {
                $request->session()->flash('message', 'Save Shipping Information successfully');
            } else {
                $request->session()->flash('error', 'Unable to Add Shipping Information!');
            }
            return Redirect::to('/add-shipping-method');
        }
    }

    public function delete_shipping($id){
        DB::table('shipping_method')
            ->where('id',$id)
            ->update(['deleted_at'=>1]);
        return Redirect::to('/add-shipping-method');


    }

    public function update_shipping_method(Request $request){
       $edit_shipping= DB::table('shipping_method')
            ->where('id',$request->id)
           ->update(['name'=>$request->name]);
        return Redirect::to('/add-shipping-method')->with('edit_shipping',$edit_shipping);

    }

}
