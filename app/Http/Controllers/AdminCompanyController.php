<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Session;
use Validator;
use Illuminate\Support\Facades\Redirect;


class AdminCompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }



    public function add_company()
    {

        $countries = DB::table('countries')
            ->select('id', 'countryName')
            ->get();

        return view('admin.pages.add_company')
            ->with('countries', $countries);

    }

    public function save_company(Request $request)
    {
        $validator = Validator::make($request->all(), [

            'company_name' => 'required',
            'address' => 'required',
            'city' => 'required',
            'phone' => 'required',
            'country_id' => 'required'

        ]);

        if ($validator->fails()) {

            $request->session()->flash('error', "Must Fill up required Field");

            return redirect()->back()->withErrors($validator)->withInput();
        } else {

            $data = array();
            $data['company_name'] = $request->company_name;
            $data['address'] = $request->address;
            $data['city'] = $request->city;
            $data['zip_code'] = $request->zip_code;
            $data['email'] = $request->email;
            $data['phone'] = $request->phone;
            $data['country_id'] = $request->country_id;
            $data['website'] = $request->website;
            $data['fb_link'] = $request->fb_link;
            $data['created_at'] = date("Y-m-d H:i:s");
            $data['updated_at'] = date("Y-m-d H:i:s");
            $image = $request->file('logo');
            // print_r($image);
            //   exit;

            if ($image) {

                $image_name = str_random(20);
                $ext = strtolower($image->getClientOriginalExtension());
                if($ext=="jpg" || $ext=="png" || $ext=="jpeg"||$ext=='gif') {
                    $image_full_name = $image_name . '.' . $ext;
                    $upload_path = 'company_image/';
                    $image_url = $upload_path . $image_full_name;
                    $success = $image->move($upload_path, $image_full_name);


                    if ($success) {
                        $data['logo'] = $image_url;
                    } else {
                        $data['logo'] = '';
                    }
                }else{
                    $request->session()->flash('error', "Image Must be JPG, JPEG, PNG, GIF");

                    return redirect()->back()->withErrors($validator)->withInput();
                }
            }


            $insert = DB::table('companies')->insert($data);
            if($insert)
                $request->session()->flash('message', 'Save Company Information successfully');
            else
                $request->session()->flash('error', 'Unable to Add Company Information!');


            return Redirect::to('/add-company');





        }
    }


    public function list_company()
    {
        $all_company_info = DB::table('companies')->orderBy('id',"desc")->paginate(20);
        return view('admin.pages.list_company')->with('all_company_info', $all_company_info);

    }
    public function system_satting(){
        return view('admin.pages.system_satting');
    }
    public function delete_company($id)
    {
        $deleted = DB::table('companies')
            ->where('id', $id)
            ->delete();
        if ($deleted)
            session()->flash('message', 'Delete Data successfully');
        else
            session()->flash('error', 'Unable to delete data');

        return Redirect::to('/list-company');

    }

    public function edit_company($id)
    {

        $edit_company = DB::table('companies')
            ->where('id', $id)
            ->first();
        $countries = DB::table('countries')->select('id', 'countryName')->get();
        return view('admin.pages.edit_company')
            ->with('edit_company', $edit_company)
            ->with('countries', $countries);

    }

    public function update_company(Request $request)
    {
        $validator = Validator::make($request->all(), [

            'company_name' => 'required',
            'address' => 'required',
            'phone' => 'required',
            'city' => 'required',
            'country_id' => 'required'

        ]);

        if ($validator->fails()) {

            $request->session()->flash('error', "Must Fill up required Field");

            return redirect()->back()->withErrors($validator)->withInput();
        } else {

            $data = array();
            $id = $request->id;
            $data['company_name'] = $request->company_name;
            $data['address'] = $request->address;
            $data['city'] = $request->city;
            $data['zip_code'] = $request->zip_code;
            $data['email'] = $request->email;
            $data['phone'] = $request->phone;
            $data['country_id'] = $request->country_id;
            $data['website'] = $request->website;
            $data['fb_link'] = $request->fb_link;
            $data['updated_at'] = date("Y-m-d H:i:s");
            $image = $request->file('logo');




            if ($image) {

                $image_name = str_random(20);
                $ext = strtolower($image->getClientOriginalExtension());
                if($ext=="jpg" || $ext=="png" || $ext=="jpeg"||$ext=='gif') {
                    $image_full_name = $image_name . '.' . $ext;
                    $upload_path = 'company_image/';
                    $image_url = $upload_path . $image_full_name;
                    $success = $image->move($upload_path, $image_full_name);



                    if($success){
                        $data['logo']=$image_url;


                        $old_image_path = $request->last_image_path;
                        //echo $old_image_path;

                        //    unlink($old_image_path);
                        //exit();
                    }
                    else{
                        $data['logo']='';
                    }
                }else{
                    $request->session()->flash('error', "Image Must be JPG, JPEG, PNG, GIF");

                    return redirect()->back()->withErrors($validator)->withInput();
                }
            }

            $update =  DB::table('companies')
                ->where('id', $id)
                ->update($data);


            if($update)
                $request->session()->flash('message', 'Update Company Information successfully');
            else
                $request->session()->flash('error', 'Unable to Update Company Information!');

            return Redirect::to('/list-company');


        }


    }
}
