@extends('admin.master')
@section('content')

    <link rel="stylesheet" type="text/css" href="{{URL::asset('css/dashboard_style.css')}}" />


    <div class="row wrapper white-bg page-heading">
        <div class="col-lg-12">
            <h2 style="color: #2F4050; font-size: 16px; font-weight: 400; margin-top: 18px">Dashboard </h2>

        </div>

    </div>




   <br>
    <div class="wrapper wrapper-content animated fadeIn">
        <div class="col-md-12">
                <ul  class="nav nav-pills dash-tab">
                    <li class="active">
                        <a  href="#daily" data-toggle="tab">Daily</a>
                    </li>
                    <li>
                        <a href="#monthly" data-toggle="tab">Monthly</a>
                    </li>
                    <li>
                        <a href="#yearly" data-toggle="tab">Yearly </a>
                    </li>
                    <li>
                        <a href="#lifetime" data-toggle="tab">Life Time </a>
                    </li>

                </ul>
        </div>

        <div class="tab-content">
             @include('admin.pages.dashboard.daily')
             @include('admin.pages.dashboard.monthly')
             @include('admin.pages.dashboard.yearly')
             @include('admin.pages.dashboard.lifetime')
        </div>
    </div>

    </div>
    </div>
@endsection