<?php
/**
 * Created by PhpStorm.
 * User: MObarok Hossen
 * Date: 7/22/2017
 * Time: 4:03 PM
 */

?>


<div class="col-md-12 tab-pane fade" id="yearly">
    <div class="ibox float-e-margins">


        <div class="ibox-title">

            <h5 style="font-size: 20px;">Invoice Yearly Report</h5>
        </div>


        <div class="ibox-content">
            <div id="inc_exp_pie" style="height:270px;">
                <div class="col-lg-2 col-sm-6">
                    <div class="circle-tile">
                        <a href="#">
                            <div class="circle-tile-heading dark-blue">
                                <i class="fa fa-tags fa-fw fa-3x"></i>
                            </div>
                        </a>
                        <div class="circle-tile-content dark-blue">
                            <div class="circle-tile-description text-faded">
                                Total Invoice
                            </div>
                            <div class="circle-tile-number text-faded">
                                {{$year_report->total_invoice}}

                                <span id="sparklineA"></span>
                            </div>
                            <a href="{{URL::to('/list-invoice')}}" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-sm-6">
                    <div class="circle-tile">
                        <a href="#">
                            <div class="circle-tile-heading blue">
                                <i class="fa fa-money fa-fw fa-3x"></i>
                            </div>
                        </a>
                        <div class="circle-tile-content blue">
                            <div class="circle-tile-description text-faded">
                                Paid Invocie
                            </div>
                            <div class="circle-tile-number text-faded">
                                {{$year_report->total_paid}}

                                <span id="sparklineB"></span>
                            </div>
                            <a href="{{URL::to('/paid-list-invoice')}}" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-sm-6">
                    <div class="circle-tile">
                        <a href="#">
                            <div class="circle-tile-heading green">
                                <i class="fa fa-circle-o-notch fa-fw fa-3x"></i>
                            </div>
                        </a>
                        <div class="circle-tile-content green">
                            <div class="circle-tile-description text-faded">
                                Partial Invoice
                            </div>
                            <div class="circle-tile-number text-faded">
                                {{$year_report->total_partial_paid}}
                            </div>
                            <a href="partially-paid-list-invoice" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-sm-6">
                    <div class="circle-tile">
                        <a href="#">
                            <div class="circle-tile-heading orange">
                                <i class="fa fa-thumbs-o-down fa-fw fa-3x"></i>
                            </div>
                        </a>
                        <div class="circle-tile-content orange">
                            <div class="circle-tile-description text-faded">
                                Unpaid Invoice
                            </div>
                            <div class="circle-tile-number text-faded">
                                {{$year_report->total_unpaid}}
                            </div>
                            <a href="{{URL::to('/unpaid-list-invoice')}}" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-sm-6">
                    <div class="circle-tile">
                        <a href="#">
                            <div class="circle-tile-heading red">
                                <i class="fa fa-ban fa-fw fa-3x"></i>
                            </div>
                        </a>
                        <div class="circle-tile-content red">
                            <div class="circle-tile-description text-faded">
                                Cancel Invoice
                            </div>
                            <div class="circle-tile-number text-faded">
                                {{ $year_report->total_cancel>0?$year_report->total_cancel:"0"}}
                                <span id="sparklineC"></span>
                            </div>
                            <a href="{{URL::to('/cancel-list-invoice')}}" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>


    <div class="ibox float-e-margins">
        <div class="ibox-title">

            <h5 style="font-size:20px;">Last Year Amount</h5>
        </div>
        <div class="ibox-content">
            <div id="inc_exp_pie" style="height:400px;">

                <div class="col-lg-3 col-sm-6">
                    <div class="circle-tile">
                        <a href="#">
                            <div class="circle-tile-heading dark-blue">
                                <i class="fa fa-comments fa-fw fa-3x"></i>
                            </div>
                        </a>
                        <div class="circle-tile-content dark-blue">
                            <div class="circle-tile-description text-faded">
                                Total Amount
                            </div>
                            <div class="circle-tile-number text-faded">

                                {{$year_report->total_amount>0?$year_report->total_amount:'0'}}
                                <span style="font-size: 13px;">TK</span>
                                <span id="sparklineD"></span>
                            </div>
                            <a href="#" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="circle-tile">
                        <a href="#">
                            <div class="circle-tile-heading blue">
                                <i class="fa fa-money fa-fw fa-3x"></i>
                            </div>
                        </a>
                        <div class="circle-tile-content blue">
                            <div class="circle-tile-description text-faded">
                                Paid Amount
                            </div>
                            <div class="circle-tile-number text-faded">
                                {{$year_report->paid_amount>0?$year_report->paid_amount:"0"}}
                                <span style="font-size: 13px;">TK</span>
                                <span id="sparklineD"></span>
                            </div>
                            <a href="#" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="circle-tile">
                        <a href="#">
                            <div class="circle-tile-heading green">
                                <i class="fa fa-circle-o-notch fa-fw fa-3x"></i>
                            </div>
                        </a>
                        <div class="circle-tile-content green">
                            <div class="circle-tile-description text-faded">
                                Partial Amount
                            </div>
                            <div class="circle-tile-number text-faded">
                                {{$year_report->partial_paid_amount>0?$year_report->partial_paid_amount:"0"}}

                                <span style="font-size: 13px;">TK</span>
                                <span id="sparklineD"></span>
                            </div>
                            <a href="#" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-sm-6">
                    <div class="circle-tile">
                        <a href="#">
                            <div class="circle-tile-heading orange">
                                <i class="fa fa-thumbs-o-down fa-fw fa-3x"></i>
                            </div>
                        </a>
                        <div class="circle-tile-content orange">
                            <div class="circle-tile-description text-faded">
                                Unpaid Amount
                            </div>
                            <div class="circle-tile-number text-faded">
                                {{$year_report->unpaid_amount>0?$year_report->unpaid_amount:"0"}}
                                <span style="font-size: 13px;">TK</span>
                                <span id="sparklineD"></span>
                            </div>
                            <a href="#" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a>
                        </div>
                    </div>
                </div>




            </div>

        </div>
    </div>

</div>
