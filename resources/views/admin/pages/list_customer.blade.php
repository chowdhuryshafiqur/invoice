@extends('admin.master')
@section('content')


    <div class="row wrapper white-bg page-heading">
        <div class="col-lg-12">
            <h2 style="color: #2F4050; font-size: 16px; font-weight: 400; margin-top: 18px"> Customers </h2>

        </div>

    </div>

    <div class="wrapper wrapper-content animated fadeIn">




        <div class="row">



            <div class="col-md-12">

                @include('admin.partials.message')

                <div class="panel panel-default">
                    <div class="panel-body">

                        <a href="{{URL::to('/add-customer')}}" class="btn btn-success"><i class="fa fa-plus"></i> Add Customer</a>


                    </div>
                    <div class="panel-body">
                        <form class="form-horizontal" method="post" action="">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <span class="fa fa-search"></span>
                                        </div>
                                        <input type="text" name="name" id="foo_filter" class="form-control" placeholder="Search..."/>

                                    </div>
                                </div>

                            </div>
                        </form>



                        <table class="table table-bordered table-hover sys_table footable"  data-filter="#foo_filter" data-page-size="50">
                            <thead>
                            <tr>
                                <th>#</th>



                                <th>Customer Name</th>
                                <th>Email</th>
                                <th>Address</th>
                                <th>Phone</th>
                                <th class="text-right" data-sort-ignore="true">Manage</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($all_customer_info as $v_info)
                                <tr>


                                    <td>{{$v_info->customer_id}} </td>
                                    <td>{{$v_info->customer_name}}</td>



                                    <td>{{$v_info->email}}</td>
                                    <td>{{$v_info->address}}</td>


                                    <td>{{$v_info->phone}}</td>
                                    <td class="text-right">

                                        <a href="{{URL::to('/edit-customer/'.$v_info->customer_id)}}" class="btn btn-primary btn-xs"><i class="fa fa-pencil"> Edit</i> </a>


                                        <a href="{{URL::to('/delete-customer/'.$v_info->customer_id)}}" class="btn btn-danger btn-xs status-change"><i class="fa fa-trash"></i> </a>
                                    </td>
                                </tr>
                            @endforeach




                        </table>

                    </div>
                </div>
            </div>

        </div>

    </div>



    </div>

    <script type="text/javascript">

        $(document).ready(function(){
            $('.status-change').on('click', function(event){
                event.preventDefault();
                var ref_link= $(this).attr("href");
                bootbox.confirm(
                    {
                        title: "Alert",
                        message: "Are you sure? ",
                        buttons: {
                            cancel: {
                                label: '<i class="fa fa-times"></i> Cancel',
                                className: 'btn-danger'
                            },
                            confirm: {
                                label: '<i class="fa fa-check"></i> Confirm',
                                className: 'btn-success'
                            }
                        }, callback: function(result) {
                        if (result) {
                            //include the href duplication link here?;
                            window.location=ref_link;


                        }
                    }
                    });
            });
        });

    </script>


@endsection
@section('javascript')
    <script src="{{URL::asset('js/jquery.metisMenu.js')}}"></script>
    <script src="{{URL::asset('js/jquery.slimscroll.min.js')}}"></script>



@endsection