@extends('admin.master')
@section('content')




    <div class="row wrapper white-bg page-heading">
        <div class="col-lg-12">
            <h2 style="color: #2F4050; font-size: 16px; font-weight: 400; margin-top: 18px"> Invoices </h2>

        </div>

    </div>

    <div class="wrapper wrapper-content animated fadeIn">


        <div class="row">
            <!-----<div class="col-lg-12">
                <div class="form-group">
                    <label for="exampleInputEmail1">Unique Invoice URL:</label>
                    <input type="text" class="form-control" id="invoice_public_url" onClick="this.setSelectionRange(0, this.value.length)" value="http://demo.tryib.com/client/iview/1595/token_1482382968">
                </div>-->
        </div>
        <div class="col-lg-12"  id="application_ajaxrender">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>{{$invoice_number==""?"Invoice ":$invoice_number->invoice_prefix==""?"Invoice ":$invoice_number->invoice_prefix}} No- {{$invoice_number==""?$view_invoice->invoices_id:$invoice_number->invoice_number+$view_invoice->invoices_id}} </h5>
                    <input type="hidden" name="iid" value="1595" id="iid">


                    <div class="btn-group  pull-right" role="group" aria-label="...">






                        <!---- <button type="button" class="btn  btn-danger btn-sm" id="add_payment"><i class="fa fa-plus"></i> Add Payment</button>--->

                        <!-----  <a href="http://demo.tryib.com/client/iview/1595/token_1482382968" target="_blank" class="btn btn-primary  btn-sm"><i class="fa fa-paper-plane-o"></i> Preview</a>----------->
                        <a href="{{URL::to('/edit-invoice/'.$view_invoice->invoices_id)}}" class="btn btn-warning  btn-sm"><i class="fa fa-pencil"></i> Edit</a>
                        <!--
                        <div class="btn-group" role="group">

                            <button type="button" class="btn  btn-success btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <i class="fa fa-envelope-o"></i>  Send Email
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#" id="mail_invoice_created">Invoice Created</a></li>
                                <li><a href="#" id="mail_invoice_reminder">Invoice Payment Reminder</a></li>
                                <li><a href="#" id="mail_invoice_overdue">Invoice Overdue Notice</a></li>
                                <li><a href="#" id="mail_invoice_confirm">Invoice Payment Confirmation</a></li>
                                <li><a href="#" id="mail_invoice_refund">Invoice Refund Confirmation</a></li>
                            </ul>
                        </div> -->

                        <div class="btn-group" role="group">
                            <button type="button" class="btn  btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <i class="fa fa-ellipsis-v"></i>  Mark As
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                                @if($view_invoice->status !="paid")
                                    <li><a href="{{URL::to('/paid-invoice/'.$view_invoice->invoices_id)}}" class="status-change" id="mark_paid">Paid</a></li>
                                @endif
                                @if($view_invoice->status !="unpaid")
                                    <li><a href="{{URL::to('/unpaid-invoice/'.$view_invoice->invoices_id)}}" class="status-change"   id="mark_partially_paid">Unpaid</a></li>
                                @endif
                                @if($view_invoice->status !="cancelled")
                                    <li><a href="{{URL::to('/cancel-invoice/'.$view_invoice->invoices_id)}}" class="status-change"  id="mark_cancelled">Cancelled</a></li>
                                @endif
                            </ul>
                        </div>

                        <!--<button type="button" class="btn  btn-danger btn-sm" id="add_payment"><i class="fa fa-plus"></i> Add Payment</button>-->

                        <a href="{{URL::to('/preview-invoice/'.$view_invoice->invoices_id)}}" target="_blank" class="btn btn-primary  btn-sm"><i class="fa fa-paper-plane-o"></i> Preview</a>
                        <button type="button" class="btn  btn-success btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-file-pdf-o"></i>
                            PDF
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" role="menu">

                            <li><a href="{{URL::to('/view-invoice-pdf/'.$view_invoice->invoices_id)}}" target="_blank">View PDF</a></li>
                            <li><a href="{{URL::to('/download-invoice/'.$view_invoice->invoices_id)}}">Download PDF</a></li>
                        </ul>

                        <a href="{{URL::to('/view-print/'.$view_invoice->invoices_id)}}" target="_blank" class="btn btn-primary  btn-sm"><i class="fa fa-print"></i>  Print</a>
                    </div>


                </div>

            </div>
            <div class="ibox-content">

                <div class="invoice">
                    <header class="clearfix">
                        <div class="row">
                            <div class="col-sm-6 mt-md">
                                <h2 class="h2 mt-none mb-sm text-dark text-bold text-uppercase">{{$invoice_number==""?"Invoice ":$invoice_number->invoice_prefix==""?"Invoice ":$invoice_number->invoice_prefix}}</h2>
                                <h4 class="h4 m-none text-dark text-bold">No# {{$invoice_number==""?$view_invoice->invoices_id:$invoice_number->invoice_number+$view_invoice->invoices_id}} </h4>

                                @if($view_invoice->status == "cancelled")
                                    <h3 class="alert alert-danger">
                                        Cancelled
                                    </h3>
                                @elseif($view_invoice->status == "unpaid")
                                    <h3 class="alert alert-warning">
                                        Unpaid
                                    </h3>
                                @elseif($view_invoice->status == "paid") <h3 class="alert alert-success">
                                    Paid
                                </h3>
                                @endif
                            </div>

                            <?php

                            $company_country=DB::table('countries')
                                ->select('id','countryName','countries.id AS country_id')
                                ->where('id',$view_invoice->company_country)
                                ->first();

                            ?>

                            <div class="col-sm-6 text-right mt-md mb-md">
                                <address class="ib mr-xlg">
                                    {{$view_invoice->company_name}}
                                    <br> {{$view_invoice->company_address}}
                                    <br>{{$view_invoice->company_city}}
                                    <br> {{$company_country->countryName}}
                                </address>
                                <div class="ib">
                                    @if(!empty($view_invoice->logo))
                                        <img height="70px" src="{{URL::asset('/').$view_invoice->logo}}" alt="Logo"/>
                                    @else
                                        <img width="200px" src="{{URL::asset('/company_image/default.png')}}" style="visibility: hidden;" alt="Logo"/>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </header>
                    <div class="bill-info">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="bill-to">
                                    <p class="h5 mb-xs text-dark text-semibold"><strong>Invoiced To:</strong></p>
                                    <address>
                                        {{$view_invoice->customer_name}}
                                        <br>

                                        {{$view_invoice->customer_address}}  <br>
                                        {{$view_invoice->customer_city}}  <br>
                                        Postal Code: {{$view_invoice->customer_zip_code}} <br>
                                        <?php
                                        $_SESSION['total_price']='';
                                        $customer_country=DB::table('countries')
                                            ->select('id','countryName')
                                            ->where('id',$view_invoice->customer_country)
                                            ->first();

                                        ?>

                                        {{$customer_country->countryName}}
                                        <br>
                                        <strong>Phone:</strong> {{$view_invoice->customer_phone}}
                                        <br>
                                        @if($view_invoice->customer_email)  <strong>Email:</strong>{{$view_invoice->customer_email}}
                                        @endif


                                    </address>





                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="bill-data text-right">
                                    <p class="mb-none">
                                        <span class="text-dark">Invoice Date: </span>
                                        <span class="value">{{$view_invoice->invoice_date}}</span>
                                    </p>
                                    <p class="mb-none">

                                    </p><span class="text-dark">Shipping Method: </span>
                                    <span class="value">{{$view_invoice->shipping_method}}</span>
                                    <h2> Invoice Total: <span class="amount" data-a-sign=" $  ">{{"Tk.".number_format((float)$view_invoice->total_price, 2, '.', '')}}</span> </h2>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="table-responsive">
                        <table class="table invoice-items">
                            <thead>
                            <tr class="h4 text-dark">
                                <th id="cell-id" class="text-semibold">Item Code  </th>
                                <th id="cell-item" class="text-semibold">Products</th>

                                <th id="cell-price" class="text-right text-semibold">Price</th>
                                <th id="cell-qty" class="text-center text-semibold">Quantity</th>
                                <th id="cell-total" class="text-right text-semibold">Total</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $products=DB::table('products')
                                ->where('invoice_id',$view_invoice->invoices_id)
                                ->get();
                            $total_price = 0;
                            foreach($products as $value){

                            ?>

                            <tr>
                                <td>{{$value->product_code}}</td>
                                <td class="text-semibold text-dark">{{$value->products}}</td>

                                <td class="text-right amount" data-a-sign=" $  ">{{"Tk. ".$value->price}}</td>
                                <td class="text-center">{{$value->quantity}}</td>
                                <td class="text-right amount" data-a-sign=" $  ">{{ "Tk. ".number_format((float)$value->price*$value->quantity, 2, '.', '')}}</td>
                            </tr>
                            <?php
                            $total_price += $value->price*$value->quantity;
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>

                    <div class="invoice-summary">
                        <div class="row">
                            <div class="col-sm-4 col-sm-offset-8">
                                <table class="table h5 text-dark">
                                    <tbody>
                                    <tr class="b-top-none h4">
                                        <td colspan="2">Subtotal</td>
                                        <td class="text-right amount" data-a-sign=" $  ">{{"Tk. ".number_format((float)$total_price, 2, '.', '')}}</td>
                                    </tr>
                                    @if($view_invoice->delivery_charge>0)
                                        <tr class="b-top-none">
                                            <td colspan="2">Delivery Fee</td>
                                            <td class="text-right amount" data-a-sign=" $  ">{{"Tk. ".number_format((float)$view_invoice->delivery_charge, 2, '.', '')}}</td>
                                        </tr>
                                    @endif
                                    @if($view_invoice->advance_payment>0)
                                        <tr class="b-top-none">
                                            <td colspan="2">Advance Payment</td>
                                            <td class="text-right amount" data-a-sign=" $  ">{{"Tk.  - ".number_format((float)$view_invoice->advance_payment, 2, '.', '')}}</td>
                                        </tr>
                                    @endif

                                    <tr class="b-top-none">
                                        <?php $discount=0; ?>
                                        @if($view_invoice->discount_type=='Percent')
                                            <td colspan="2">Discount({{$view_invoice->discount}}%)</td>
                                            <td class="text-right amount" data-a-sign=" $  ">
                                                {{"Tk. - ".number_format((float)$total_price*$view_invoice->discount*0.01, 2, '.', '')}}
                                                <?php $discount=$total_price*$view_invoice->discount*0.01; ?>
                                            </td>
                                        @elseif($view_invoice->discount_type=='Fixed')
                                            <td colspan="2">Fixed Discount</td>
                                            <td class="text-right amount" data-a-sign=" $  ">
                                            {{"Tk. - ".number_format((float)$view_invoice->discount,2, '.', '')}}
                                            <?php $discount=$view_invoice->discount;?>
                                        @endif

                                    </tr>
                                    <tr class="h4">
                                        <td colspan="2">Grand Total</td>
                                        <td class="text-right amount" data-a-sign=" $  ">
                                            {{"Tk. ".number_format((float)$total_price-$discount-$view_invoice->advance_payment+$view_invoice->delivery_charge, 2, '.', '')}}</td>
                                    </tr>
                                    <tr class="b-top-none" >
                                        <td colspan="2"></td>
                                        <td style=" text-align: right;"> <a href="{{URL::to('/list-invoice')}}" class="md-btn md-btn-success waves-effect waves-light" ><i class="fa fa-check"></i> Update Invoice</a></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>

    </div>

    <script type="text/javascript">

        $(document).ready(function(){
            $('.status-change').on('click', function(event){
                event.preventDefault();
                var ref_link= $(this).attr("href");
                bootbox.confirm(
                    {
                        title: "Alert",
                        message: "Are you sure? ",
                        buttons: {
                            cancel: {
                                label: '<i class="fa fa-times"></i> Cancel',
                                className: 'btn-danger'
                            },
                            confirm: {
                                label: '<i class="fa fa-check"></i> Confirm',
                                className: 'btn-success'
                            }
                        }, callback: function(result) {
                        if (result) {
                            //include the href duplication link here?;
                            window.location=ref_link;


                        }
                    }
                    });
            });
        });

    </script>

@endsection