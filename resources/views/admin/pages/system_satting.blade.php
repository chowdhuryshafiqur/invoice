@extends('admin.master')
@section('content')

    <div class="row wrapper white-bg page-heading">
        <div class="col-lg-12">
            <h2 style="color: #2F4050; font-size: 16px; font-weight: 400; margin-top: 18px">Setting System</h2>

        </div>

    </div>

    <div class="wrapper wrapper-content animated fadeIn">


        <div class="wrapper wrapper-content">
            <div class="row">

                <div class="col-md-12">



                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5> Setting System</h5>

                            <!--   <a href="" class="btn btn-xs btn-primary btn-rounded pull-right"><i class="fa fa-bars"></i> Import Contacts</a>-->

                        </div>

                        <div class="ibox-content" id="ibox_form">
                            @include('admin.partials.message')



                            @if($edit==null)
                            {!! Form::open(['url' => '/save-setting','class'=>'form-horizontal','id'=>'rform','name'=>'settings']) !!}
                            @else
                            {!! Form::open(['url' => '/update-setting','class'=>'form-horizontal','name'=>'settings']) !!}
                            @endif

                            <div class="row">
                                <div class="col-md-6 col-sm-12">


                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="account">Start Invoice Number<small class="red"></small> </label>
                                        <div class="col-lg-8">
                                            <input type="number" id="invoice_number" name="invoice_number" class="form-control number" autofocus value="{{$edit==null?'':$settings->invoice_number}}">
                                            <input type="hidden"  name="id" value="{{$edit==null?'':$settings->id}}" class="form-control" autofocus>

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="account">Invoice Prefix<small class="red"></small> </label>
                                        <div class="col-lg-8">
                                            <input type="text" id="invoice_prefix" name="invoice_prefix" class="form-control" autofocus  value="{{$edit==null?'':$settings->invoice_prefix}}">

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="account">Create Invoice<small class="red"></small> </label>
                                        <div class="col-lg-8" style="margin-top: 8px;">
                                            <input type="radio" id="default" name="create_view"  value="0"> Layout 1 (Default)
                                            <input type="radio" id="layout_1" name="create_view" style="margin-left: 30px;" value="1"> Layout 2

                                        </div>
                                    </div>





                                    </div>




                            </div>


                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="col-md-offset-2 col-lg-10">

                                            <button class="md-btn md-btn-primary waves-effect waves-light" type="submit" name="submit" id="btn"><i class="fa fa-check"></i> Save</button>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>



    </div>


    </div>


    </div>
<script>

        @if( $edit=="edit" )
            @if($settings->create_view==0)
                  document.getElementById("default").checked = true;
                  document.getElementById("layout_1").checked = false;
            @else
                document.getElementById("default").checked = false;
                document.getElementById("layout_1").checked = true;
            @endif
        @elseif($edit==null)
            document.getElementById("default").checked = true;
        @endif
</script>
@endsection