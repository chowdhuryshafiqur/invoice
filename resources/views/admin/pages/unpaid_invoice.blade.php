<?php
/**
 * Created by PhpStorm.
 * User: MOBAROK
 * Date: 6/21/2017
 * Time: 12:42 PM
 */
?>

@extends('admin.master')
@section('content')
    <div class="row wrapper white-bg page-heading">
        <div class="col-lg-12">
            <h2 style="color: #2F4050; font-size: 16px; font-weight: 400; margin-top: 18px">Unpaid Invoices
                <div class="btn-group pull-right" style="padding-right: 10px;">

                </div>
            </h2>

        </div>

    </div>

    <div class="wrapper wrapper-content animated fadeIn">







        <div class="row">
            <div class="col-md-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">

                        <div class="ibox-tools">

                            <a href="{{URL::to('/create-invoice')}}" class="btn btn-primary btn-xs"><i class="fa fa-plus"></i> Add Invoice</a>

                        </div>
                    </div>
                    <div class="ibox-content">

                        <table class="table table-bordered table-hover sys_table footable" >
                            <thead>
                            <tr>
                                <th width="8%">Inv No#</th>
                                <th width="18%">Customer Name</th>
                                <th width="9%">Mobile</th>
                                <th width="34%">Address</th>
                                <th width="5%">Amount</th>
                                <th width="10%">Invoice Date</th>
                                <th width="5%">
                                    Status
                                </th>

                                <th width="11%" class="text-right">Manage</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($all_invoice_list as $value)

                                <tr>
                                    <td  data-value="{{$value->invoice_code}}">

                                        {{$invoice_number==""?$value->invoice_id:$invoice_number->invoice_number+$value->invoice_id}}
                                        </a> </td>
                                    <td data-value="{{$value->customer_name}}">{{$value->customer_name}}</td>
                                    <td data-value="{{$value->customer_name}}">{{$value->phone}}</td>
                                    <td >{{$value->address}}</td>
                                    <td class="amount" data-a-sign=" $  ">{{$value->total_price}}</td>
                                    <td data-value="{{$value->invoice_date}}">{{$value->invoice_date}}</td>
                                    <td>
                                        @if($value->status == "unpaid")
                                            <span class=" text-warning">Unpaid</span>
                                        @elseif($value->status == "paid")
                                            <span class=" text-success">Paid</span>
                                        @elseif($value->status == "cancelled")
                                            <span class=" text-danger">Cancel</span>
                                        @elseif($value->status == "partial paid")
                                            <span class=" text-info">Partially Paid</span>
                                        @endif


                                    </td>

                                    <td class="text-right">
                                        <a href="{{URL::to('/view-invoice/'.$value->invoice_id)}}" class=" text-primary manage" ><i class="fa fa-eye"></i></a>
                                        @if($value->status == "unpaid" || $value->status == "cancelled" || $value->status == "partial paid")
                                            <a href="{{URL::to('/paid-invoice/'.$value->invoice_id)}}" class=" text-green manage status-change"><i class="fa fa-check"></i> </a>
                                        <!--    <a href="{{--URL::to('/cancel-invoice/'.$value->invoice_id)--}}" class=" text-warning manage  status-change"><i class="fa fa-ban"></i></a>-->
                                        @endif
                                        <a href="{{URL::to('/edit-invoice/'.$value->invoice_id)}}" class="text-info manage"><i class="fa fa-pencil"></i></a>
                                        <a href="{{URL::to('/delete-invoice/'.$value->invoice_id)}}"  class="text-danger manage status-change" id="iid1595"><i class="fa fa-times-circle"></i></a>
                                    </td>
                                </tr>
                            @endforeach


                            </tbody>


                        </table>
                        <div class="row">
                            <div class="text-left">
                                {{$all_invoice_list->links()}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="ajax-modal" class="modal container fade-scale" tabindex="-1" style="display: none;"></div>
    </div>



    <script type="text/javascript">

        $(document).ready(function(){
            $('.status-change').on('click', function(event){
                event.preventDefault();
                var ref_link= $(this).attr("href");
                bootbox.confirm(
                    {
                        title: "Alert",
                        message: "Are you sure? ",
                        buttons: {
                            cancel: {
                                label: '<i class="fa fa-times"></i> Cancel',
                                className: 'btn-danger'
                            },
                            confirm: {
                                label: '<i class="fa fa-check"></i> Confirm',
                                className: 'btn-success'
                            }
                        }, callback: function(result) {
                        if (result) {
                            //include the href duplication link here?;
                            window.location=ref_link;


                        }
                    }
                    });
            });
        });

    </script>


@endsection


