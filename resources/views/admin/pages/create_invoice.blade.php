@extends('admin.master')
@section('content')



    <div class="row wrapper white-bg page-heading">
        <div class="col-lg-12">
            <h2 style="color: #2F4050; font-size: 16px; font-weight: 400; margin-top: 18px"> Create Invoice </h2>

        </div>

    </div>
    <div class="wrapper wrapper-content animated fadeIn">

        {!! Form::open(['url' => '/save-invoice','id'=>'invform','method'=>'post','name'=>'save-invoice']) !!}


        <div class="col-md-8">
            @include('admin.partials.message')
            <div class="panel panel-default">
                <div class="col-lg-12">
                    <h2 style="color: #2F4050; font-size: 14px; font-weight: 700; margin-top: 30px; margin-left: 10px;"> Add Product Detials </h2>

                </div>
                <div class="panel-body">

                    <div class="table-responsive m-t">
                        <table class="table invoice-table" id="invoice_items">
                            <thead>
                            <tr>
                                <th width="12%">Item Code</th>
                                <th width="42%">Products</th>
                                <th width="12%">Qty</th>
                                <th width="12%">Price</th>
                                <th width="12%">Total</th>


                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>
                                    <input type="text" class="form-control" name="item_code[]">
                                    <input type="hidden" value="{{ Auth::user()->id }}" class="form-control" name="user_id">
                                </td>
                                <td><textarea class="form-control item_name" required name="product[]" rows="3"></textarea> </td>
                                <td><input type="number" class="form-control qty" required name="qty[]"></td>
                                <td><input type="number" class="form-control item_price" required name="price[]" ></td>
                                <td class="ltotal"><input type="text" readonly class="form-control lvtotal"  ></td>
                                <input type="hidden" class="form-control" name="discount" id="discount_percent">
                                <input type="hidden" class="form-control" name="discount_type" id="type_of_discount">
                                <input type="hidden" class="form-control" name="total_amount" id="total_amount">
                            </tr>
                            <input type="hidden" class="form-control" name="total_invoice_amount" id="total_invoice_amount">
                            <input type="hidden" class="form-control"  name="adv_invoice_amount" id="adv_invoice_amount">
                            </tbody>
                        </table>

                    </div>


                    <!-- /table-responsive -->
                    <div class="col-md-12">
                        <button type="button" class="btn btn-primary" id="blank-add"><i
                                    class="fa fa-plus"></i> Add New Product</button>

                        <button type="button" class="btn btn-danger" id="item-remove"><i
                                    class="fa fa-minus-circle"></i> Delete</button>
                    </div>

                    <div class="col-md-8">
                        <div class="col-lg-12" style="    padding-left: 0px;">
                            <h2 style="color: #2F4050; font-size: 14px; font-weight: 700;  margin-left: 0px;"> Notes: </h2>

                        </div>
                        <br>
                        <br>
                        <br>
                        <textarea class="form-control" name="notes" id="notes" rows="3"
                                  placeholder="Invoice Notes..."></textarea>
                        <br>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group" style="margin-top: 55px;">
                            <label for="idate">Delivery Charge</label>
                            <input type="text" class="form-control" id="delivery_payment" name="delivery_charge" required>
                        </div>
                        <div class="form-group" >
                            <label for="cn">Payment Status</label>
                            <select name="status" id="status" class="form-control" required>
                                <option value="unpaid">Unpaid</option>
                                <option value="paid">Fully Paid</option>
                                <option value="partial paid">Advance Payment</option>
                                <option value="cancelled">Cancelled</option>
                            </select>
                        </div>

                        <div class="form-group" id="adv_payment">
                            <label for="idate">Advance Payment</label>
                            <input type="text" class="form-control" id="advance_payment" name="advance_payment">
                        </div>





                        <div class="form-group">
                            <label for="add_discount">
                                <a href="#" data-target="#set_add_discount" data-toggle="modal" class="btn btn-info btn-xs" style="margin-top: 5px;">
                                    <i class="fa fa-minus-circle"></i> Set Discount</a>
                            </label>

                        </div>
                    </div>


                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-body">

                    <table class="table invoice-total">
                        <tbody>
                        <tr>
                            <td><strong>Sub Total :</strong></td>
                            <td id="sub_total" class="amount sub_total">0.00
                            </td>
                        </tr>
                        <tr class="tr-delivery-charge display-none">
                            <td><strong>Delivery Charge <span id="is_pt"></span> :</strong></td>
                            <td id="delivery_charge" class="amount delivery_charge">0.00
                            </td>
                        </tr>

                        <tr class="tr-discount display-none">
                            <td><strong class="discount_text">Discount <span id="is_pt"></span> :</strong></td>
                            <td id="discount_amount_total" class="amount discount_amount_total">0.00
                            </td>
                        </tr>
                        <tr class="tr-advence-payment display-none">
                            <td><strong>Advance payment<span id="is_pt"></span> :</strong></td>
                            <td  class="amount advence_payment" >0.00
                            </td>
                        </tr>
                        <tr>
                            <td><strong>TOTAL :</strong></td>
                            <td id="total" class="amount total">0.00
                            </td>
                        </tr>
                        </tbody>
                    </table>

                </div>
            </div>




        </div>

        <div class="col-md-4">

        <!--
            <div class="panel panel-default">
                <div class="panel-body">

                    <div class="text-right">
                        <button class="btn btn-primary" id="submit" type="submit"><i class="fa fa-save"></i> Save Invoice</button>
                        <input type="submit" class="btn btn-primary" value="Save Invoice"/>
                        <a class="btn btn-danger" href="{{URL::to('/list-invoice')}}"><i class="fa fa-times"></i> Close</a>

                    </div>

                </div>
            </div> -->
            <div class="panel panel-default">
                <div class="panel-body">

                    <table class="table invoice-total">
                        <tbody>
                        <tr>
                            <td><strong>Sub Total :</strong></td>
                            <td style="width:27%" id="sub_total" class="amount sub_total">0.00
                            </td>
                        </tr>
                        <tr class="tr-delivery-charge display-none">
                            <td><strong>Delivery Charge <span id="is_pt"></span> :</strong></td>
                            <td id="delivery_charge" class="amount delivery_charge">0.00
                            </td>
                        </tr>

                        <tr class="tr-discount display-none">
                            <td><strong class="discount_text">Discount <span id="is_pt"></span> :</strong></td>
                            <td id="discount_amount_total" class="amount discount_amount_total">0.00
                            </td>
                        </tr>
                        <tr class="tr-advence-payment display-none">
                            <td><strong>Advance payment<span id="is_pt"></span> :</strong></td>
                            <td  class="amount advence_payment" >0.00
                            </td>
                        </tr>
                        <tr>
                            <td><strong>TOTAL :</strong></td>
                            <td id="total" class="amount total">0.00
                            </td>
                        </tr>
                        </tbody>
                    </table>

                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-body">

                    <div>



                        <div class="form-group">
                            <label for="cid">Customer</label>

                            <select id="cid" class="customer_id" name="customer_id" class="form-control" required>
                                <option value="">Select Contact...</option>
                                @foreach($customers as $value)
                                    <option value="{{$value->customer_id}}">{{$value->customer_name.' ( '.$value->phone.' ) '}}</option>
                                @endforeach

                            </select>
                            <span ><a href="#" data-toggle="modal" data-target="#add_customer">| Add Customer Info</a> <a href="" style="display: none; float: right;" id="edit_customer">| Or Edit Customer Info</a> </span>
                        </div>

                        <div class="form-group address-box">
                            <label for="address">Address</label>

                            <textarea id="invoice_address" name="invoice_address" disabled class="form-control" rows="2"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="cid">Invoicing Store</label>

                            <select id="company" name="company_id" class="form-control" required>
                                <option value="">Select Store ...</option>
                                @foreach($companies as $value)
                                    <option value="{{$value->id}}"> {{$value->company_name}}  </option>
                                @endforeach

                            </select>
                        </div>

                        <div class="form-group" id="invoice_date">
                            <label for="idate">Invoice Date</label>

                            <input type="text" class="form-control" id="idate" name="invoice_date" datepicker data-date-format="yyyy-mm-dd" data-auto-close="true" >
                        </div>
                        <div class="form-group">
                            <label for="cn">Shipping Method</label>
                            <select name="shipping_method" class="form-control" required>
                                <option value="">Select Shipping Method...</option>
                                <option value="Home Delivery">Home Delivery</option>
                                <option value="Condition Delivery">Condition Delivery</option>
                            </select>
                        </div>


                    </div>

                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-body">

                    <div class="text-right">
                        <!--   <button class="btn btn-primary" id="submit" type="submit"><i class="fa fa-save"></i> Save Invoice</button> -->
                        <input type="submit" class="btn btn-primary" value="Save Invoice"/>
                        <a class="btn btn-danger" href="{{URL::to('/list-invoice')}}"><i class="fa fa-times"></i> Close</a>

                    </div>

                </div>
            </div>

            {!! Form::close() !!}


        </div>



    </div>


    <div id="add_customer" class="modal fade add-customer-modal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">


                <!--               <form class="form-horizontal" name="customer_info" id="customer_info"> -->
                {!! Form::open(['url'=>'/save-customer-ajax','method'=>'POST','name'=>"customer_info", 'id'=>"customer_info"]) !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Customer</h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <label class="col-md-3">Full Name <small class="red">*</small></label>
                        <div class="col-md-9">
                            <input class="form-control" type="text" name="customer_name"/>

                        </div>
                    </div>
                    <div class="col-md-12">
                        <label class="col-md-3">Phone <small class="red">*</small></label>
                        <div class="col-md-9">
                            <input class="form-control" type="text" name="phone"/>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label class="col-md-3">Address <small class="red">*</small></label>
                        <div class="col-md-9">
                            <input class="form-control" type="text" name="address"/>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <label class="col-md-3">Email</label>
                        <div class="col-md-9">
                            <input class="form-control" type="email" name="email"/>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label class="col-md-3">City</label>
                        <div class="col-md-9">
                            <input class="form-control" type="text" name="city"/>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label class="col-md-3">Zip / Postal Code</label>
                        <div class="col-md-9">
                            <input class="form-control" type="text" name="zip_code"/>
                        </div>
                    </div>
                    <div class="col-md-12" hidden>
                        <label class="col-md-3">Country</label>
                        <div class="col-md-9">
                            <select name="country_id" id="country" class="form-control">
                                <option>Select Country</option>
                                @foreach($countries as $value)
                                    <option value="{{$value->id}}">{{$value->countryName}}</option>
                                @endforeach

                            </select>
                        </div>
                    </div>

                    <br/>
                    <br/>
                    <br/>
                </div>
                <div class="modal-footer">
                    <div class="col-md-12">

                        <button type="submit" id="save_customer" class="btn btn-primary"><i class="fa fa-check"></i> Save </button>
                        <button type="button" class="btn btn-danger modal-close" style="margin-right: 30px;" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                    </div>
                </div>
                <!--        </form> -->
                {!! Form::close() !!}

            </div>

        </div>
    </div>


    </div>


    </div>

    <div id="set_add_discount" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">

                <form name="discount_info" id="discount_info">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Set Discount</h4>
                    </div>

                    <div class="modal-body">
                        <div class="col-md-12">
                            <label class="col-md-3 discount_text">Discount</label>
                            <div class="col-md-5">
                                <input class="form-control discount_amount" type="text" name="set_discount_amount"/>

                            </div>
                        </div>
                        <div class="col-md-12">
                            <label class="col-md-3">Discount Type</label>
                            <div class="col-md-9 discount">
                                <input  type="radio" name="set_discount_type" value="percent"/> Percentage (%) <br/>
                                <input  type="radio" name="set_discount_type" value="fixed"/> Fixed Price
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer discount">
                        <input type="submit" class="btn btn-primary"   data-dismiss="modal" id="discount_setting" value=" Success ">
                    </div>
                </form>
            </div>

        </div>
    </div>

    <input type="hidden" id="last_price" />
    <input type="hidden" id="last_qty" />
    <input type="hidden" id="last_row" />

@endsection


@section('javascript')
    <script type="text/javascript" src="{{URL::asset('js/lib/numeric.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('js/lib/select2.min.js')}}"></script>
    <script src="{{URL::asset('js/jquery.metisMenu.js')}}"></script>
    <script src="{{URL::asset('js/lib/app.js')}}"></script>
    <script type="text/javascript">

        $(document).ready(function () {


            $('#short_description').redactor(
                {
                    minHeight: 200, // pixels
                    paragraphize: false,
                    plugins: ['fontcolor'],
                    replaceDivs: false,
                    linebreaks: true
                }
            );
            $('#description').redactor(
                {
                    minHeight: 400, // pixels
                    paragraphize: false,
                    plugins: ['fontcolor'],
                    replaceDivs: false,
                    linebreaks: true
                }
            );

            $("#country").select2({
                theme: "bootstrap"
            });
            $("#company").select2({
                theme: "bootstrap"
            });

            //  $('#idate').datepicker('setDate', new Date());
            // $('#invoice_date').datepicker({
            //    setdate: new Date()
            // });
            //$('#invoice_date').datepicker('setDate', new Date());


            var d = new Date();
            var curr_date = pad(d.getDate());
            var curr_month = pad(d.getMonth()+1);
            var curr_year = d.getFullYear();
            var date = curr_year+"-"+curr_month+"-"+curr_date;
            $("#idate").val(date);

            console.log(date);

            $(".qty, .item_price, #advance_payment, #delivery_payment").keypress(function (e) {
                //if the letter is not digit then display error and don't type anything
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    //display error message
                    return false;
                }
            });
            /*
             $.ajaxSetup({
             headers: {
             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
             }
             });


             $('#save_customer').click(function(){
             $.ajax({
             url:"",
             method:'post',
             data:{
             _token:$("input[name =_token]").val(),
             customer_name:$("input[name =customer_name]").val(),
             email:$("input[name =email]").val(),
             phone:$("input[name =phone]").val(),
             address:$("input[name =address]").val(),
             city:$("input[name =city]").val(),
             zip_code:$("input[name =zip_code]").val(),
             country_id:$("input[name =country_id]").val()
             },
             success:function(response){
             if (response.success == true) {

             }
             console.log(response);
             },
             error: function(xhr) {
             var errors = xhr.responseJSON;
             var error = errors.name[0];
             if (error) {

             }
             console.log(xhr);
             }
             });
             });


             */

            $("#status").change(function() {
                if ($("#status").val() == "partial paid") {
                    $("#adv_payment").css("display", "block");
                    $("#advance_payment").css("display", "block");
                }else {
                    $("#adv_payment").css("display", "none");
                    $("#advance_payment").css("display", "none");
                }
            }).trigger("change");

            /*        if($("#advance_payment").val()>$("#total_amount").val())
             {
             $("#advance_payment").css("border", "1px solid #ed5565");
             return false;
             }
             */

            $(".item_price").keyup(function(){

                if($(this).val()!=''){
                    var price = $(this).val();
                    // price = price.toFixed(2);
                    var qty = $('.qty').val();
                    var total =  price*qty;
                    if(price=='' || qty==''){
                        price  = 0;
                        qty  = 0;
                    }

                    $('.sub_total').text(parseFloat(total).toFixed(2));

                    $('#quantity').text(qty);

                    $('.total').text(parseFloat(total).toFixed(2));
                    $('.lvtotal').val(price*qty);
                    $("#total_amount").val(price*qty);
                    $("#total_invoice_amount").val(price*qty);

                }

            }).trigger("change");





            $('#discount_setting').click(function(){
                var dis_type = $('input[name=set_discount_type]:checked', '#discount_info').val()
                if(dis_type!=null){

                    var get_discount = $('.discount_amount').val();
                    var d_charge = $('#delivery_payment').val();
                    if(!d_charge)
                        d_charge=0;
                    var adv = parseFloat($("#adv_invoice_amount").val());
                    if(!adv)
                        adv = 0;
                    $('.tr-discount').css("display","table-row");
                    if(dis_type=='percent'){
                        var discount = (100-get_discount)*0.01;
                        var amount_discount  = get_discount*0.01

                        var price = $('#sub_total').text();
                        price = parseFloat(price);
                        var dis_amount = amount_discount*price;
                        var discount_total = discount*price+ +d_charge-adv;
                        $('.discount_text').text("Discount ( "+get_discount+"% )");
                        $('.discount_amount_total').text("- "+dis_amount);

                        $('.total').text(parseFloat(discount_total));
                        $("#type_of_discount").val('Percent');
                        $("#discount_percent").val(get_discount);

                    }else if(dis_type=='fixed'){

                        $('.discount_text').text("Fixed Discount");
                        $('.discount_amount_total').text("- "+get_discount);
                        //  $('.lvtotal').val(discount);
                        // $('.item_price').val(discount);
                        $('.sub_total').text(discount);
                        var price = $('#sub_total').text();
                        var total = price-get_discount+ +d_charge-adv;
                        $('.total').text(total);
                        $("#type_of_discount").val('Fixed');
                        $("#discount_percent").val(get_discount);
                    }
                }
            });
        });

        function pad(n) {
            return (n < 10) ? ("0" + n) : n;
        }



        function add_row_qty(qty){
            var qty = parseFloat(qty);

            document.getElementById("last_qty").value = qty;
            /*   var last_quantity = document.createElement("input");
             last_quantity.setAttribute('type', 'text');
             last_quantity.setAttribute('value', qty);
             last_quantity.setAttribute("name", "last_qty"); */
        }

        function add_row_price(price, row){
            var price = parseFloat(price);
            var row = parseFloat(row);

            document.getElementById("last_price").value = price;
            document.getElementById("last_row").value = row;

            /*  var last_price = document.createElement("input");
             last_price.setAttribute('type', 'text');
             last_price.setAttribute('value', price);
             last_price.setAttribute("name", "last_price"); */
            total();
        }



        function total(){
            var price = document.getElementById("last_price").value;
            var qty = document.getElementById("last_qty").value;
            var last_row = "#row_total_"+document.getElementById("last_row").value;

            var add_row_price = price*qty;
            var total_row = document.getElementById("last_row").value;


            //var total = $('#total').text();
            //total = parseFloat(total);
            console.log(last_row);

            /*   var sub_total;
             for(var i=0; i<=total_row; i++){

             sub_total =
             } */

            var total = parseFloat($("#total_amount").val());

            var sub_total = total+add_row_price;

            $(last_row).val(add_row_price);
            $("#total_amount").val(sub_total);
            $("#total_invoice_amount").val(sub_total);


            $('.sub_total').text(parseFloat(sub_total).toFixed(2));
            $('.total').text(parseFloat(sub_total).toFixed(2));
        }

        $("#delivery_payment").change(function(){

            if($(this).val()!=''){
                var delivery_payment = $(this).val();
                if(delivery_payment==''){
                    delivery_payment  = 0;
                }

                var total =  $("#total_amount").val();
                var full_total = +total + +delivery_payment;
                $("#total_invoice_amount").val(full_total);

                //    $('#sub_total').text(parseFloat(full_total).toFixed(2));

                $('.total').text(parseFloat(full_total).toFixed(2));
                $('.tr-delivery-charge').css("display","table-row");
                $('.delivery_charge').text(parseFloat(delivery_payment).toFixed(2));


            }

        }).trigger("change");

        $('#advance_payment').change(function(){

            if($(this).val()!='') {
                var adv_payment = $(this).val();
                if (!adv_payment) {
                    adv_payment = 0;
                }

                var total = $("#total_invoice_amount").val();

                var full_total = total-adv_payment;


                //    $('#sub_total').text(parseFloat(full_total).toFixed(2));

                $('.total').text(parseFloat(full_total).toFixed(2));
                $("#adv_invoice_amount").val(adv_payment);
                $('.tr-advence-payment').css("display", "table-row");
                $('.advence_payment').text("- "+parseFloat(adv_payment).toFixed(2));
            }
        }).trigger("change");


        document.forms['customer_info'].elements['country_id'].value=19;

        @if( !empty(Session::get('customer_id')))
            document.forms['save-invoice'].elements['customer_id'].value="{{Session::get('customer_id')}}";
            document.forms['save-invoice'].elements['invoice_address'].value="{{Session::get('invoice_address')}}";

        {{ Session::put('customer_id','') }}
        {{ Session::put('invoice_address','') }}
        //document.getElementById('invoice_address').readOnly = false;
        @endif

$(".address-box").css('display',"none");


        $(function(){
            $(".customer_id").on('change',function(){
                // console.log($(this).val());
                $.ajax({
                    scriptCharset: "utf-8" ,
                    contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                    type: 'GET',
                    url: "{{URL::to('/select-customer-info')}}"+"/"+$(this).val(),
                    success: function(text) {
                        document.getElementById("invoice_address").value = text;

                    }
                });
                $(".address-box").css('display',"block");
                $("#invoice_address").css('background-color', "#FFF");
                $("#invoice_address").prop('disabled', true);
                if(!$(this).val()){
                    $(".address-box").css('display',"none");

                }

                $("#edit_customer").css('display',"block");
                var edit  = "{{URL::to('/edit-customer')}}"+"/"+$(this).val();
                //    console.log(edit);
                $("#edit_customer").attr('href',edit);
            });
        });
    </script>
    <script type="text/javascript" src="{{URL::asset('js/lib/invoice_add_v2.js')}}"></script>
@endsection