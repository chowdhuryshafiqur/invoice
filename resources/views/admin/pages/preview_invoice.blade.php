
<!DOCTYPE html>



<html>

<head>


    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="storage/icon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <link href="{{URL::asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{URL::asset('css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{URL::asset('css/lib/all.css')}}" rel="stylesheet">
    <link href="{{URL::asset('css/animate.css')}}" rel="stylesheet">
    <link href="{{URL::asset('css/style.css')}}" rel="stylesheet">
    <link href="{{URL::asset('css/custom.css')}}" rel="stylesheet">


    <style type="text/css">
        body {

            background-color: #FAFAFA;
            overflow-x: visible;
        }
        .paper {
            margin: 50px auto;
            width: 980px;
            border: 2px solid #DDD;
            background-color: #FFF;
            position: relative;

        }
    </style>
</head>

<body class="fixed-nav">

<div class="paper">
    <section class="panel">
        <div class="panel-body">
            <div class="invoice">
                <header class="clearfix">
                    <div class="row">
                        <div class="col-sm-6 mt-md">
                            <h2 class="h2 mt-none mb-sm text-dark text-bold text-uppercase">{{$invoice_number==""?"Invoice ":$invoice_number->invoice_prefix==""?"Invoice ":$invoice_number->invoice_prefix}}</h2>
                            <h4 class="h4 m-none text-dark text-bold">
                                 No- {{$invoice_number==""?$view_invoice->invoices_id:$invoice_number->invoice_number+$view_invoice->invoices_id}} </h4>
                            @if($view_invoice->status == "cancelled")
                                <h3 class="alert alert-danger">
                                    Cancelled
                                </h3>
                            @elseif($view_invoice->status == "unpaid")
                                <h3 class="alert alert-warning">
                                    Unpaid
                                </h3>
                            @elseif($view_invoice->status == "paid") <h3 class="alert alert-success">
                                Paid
                            </h3>
                                @endif
                        </div>

                        <?php

                        $company_country=DB::table('countries')
                            ->select('id','countryName','countries.id AS country_id')
                            ->where('id',$view_invoice->company_country)
                            ->first();

                        ?>

                        <div class="col-sm-6 text-right mt-md mb-md">
                            <address class="ib mr-xlg">
                                {{$view_invoice->company_name}}
                                <br> {{$view_invoice->company_address}}
                                <br>{{$view_invoice->company_city}}
                                <br>{{$company_country->countryName}}
                            </address>
                            <div class="ib">
                               @if(!empty($view_invoice->logo))
                                    <img height="70px" src="{{URL::asset('/').$view_invoice->logo}}" alt="Logo"/>
                                @else
                                    <img width="200px" src="{{URL::asset('/company_image/default.png')}}" style="visibility: hidden;" alt="Logo"/>
                                @endif
                            </div>
                        </div>
                    </div>
                </header>
                <div class="bill-info">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="bill-to">
                                <p class="h5 mb-xs text-dark text-semibold"><strong>Invoiced To</strong></p>
                                <address>
                                    {{$view_invoice->customer_name}}
                                    <br>

                                    {{$view_invoice->customer_address}}  <br>
                                    {{$view_invoice->customer_city}}  <br>
                                    Postal Code: {{$view_invoice->customer_zip_code}} <br>
                                    <?php

                                    $customer_country=DB::table('countries')
                                        ->select('id','countryName')
                                        ->where('id',$view_invoice->customer_country)
                                        ->first();

                                    ?>

                                    {{$customer_country->countryName}}
                                    <br>
                                    <strong>Phone:</strong> {{$view_invoice->customer_phone}}
                                    <br>
                                    @if($view_invoice->customer_email)  <strong>Email:</strong>{{$view_invoice->customer_email}}
                                        @endif



                                </address>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="bill-data text-right">
                                <p class="mb-none">
                                    <span class="text-dark">Invoice Date: </span>
                                    <span class="value">{{$view_invoice->invoice_date}}</span>
                                </p>
                                <p class="mb-none">
                                    <span class="text-dark">Shipping Method: </span>
                                    <span class="value">{{$view_invoice->shipping_method}}</span>
                                </p>

                                <h2> Invoice Total: {{"Tk.".number_format((float)$view_invoice->total_price, 2, '.', '')}}</h2>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="table-responsive">
                    <table class="table invoice-items">
                        <thead>
                        <tr class="h4 text-dark">
                            <th id="cell-id" class="text-semibold">Item Code</th>
                            <th id="cell-item" class="text-semibold">Products</th>

                            <th id="cell-price" class="text-right text-semibold">Price</th>
                            <th id="cell-qty" class="text-center text-semibold">Quantity</th>
                            <th id="cell-total" class="text-right text-semibold">Total</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php

                        $products=DB::table('products')
                            ->where('invoice_id',$view_invoice->invoices_id)
                            ->get();

                        $total_price = 0;
                        foreach($products as $value){
                        ?>

                        <tr>
                            <td>{{$value->product_code}}</td>
                            <td class="text-semibold text-dark">{{$value->products}}</td>

                            <td class="text-right amount" data-a-sign=" $  ">{{"Tk. ".$value->price}}</td>
                            <td class="text-center">{{$value->quantity}}</td>
                            <td class="text-right amount" data-a-sign=" $  ">{{ "Tk. ".number_format((float)$value->price*$value->quantity, 2, '.', '')}}</td>
                        </tr>
                        <?php
                        $total_price += $value->price*$value->quantity;
                        }

                        ?>
                        </tbody>
                    </table>
                </div>

                <div class="invoice-summary">
                    <div class="row">
                        <div class="col-sm-4 col-sm-offset-8">
                            <table class="table h5 text-dark">
                                <tbody>
                                <tr class="b-top-none">
                                    <td colspan="2">Subtotal</td>
                                    <td class="text-right amount" data-a-sign=" $  ">{{"Tk. ".number_format((float)$total_price, 2, '.', '')}}</td>
                                </tr>
                                @if($view_invoice->delivery_charge>0)
                                    <tr class="b-top-none">
                                        <td colspan="2">Delivery Fee</td>
                                        <td class="text-right amount" data-a-sign=" $  ">{{"Tk. ".number_format((float)$view_invoice->delivery_charge, 2, '.', '')}}</td>
                                    </tr>
                                @endif
                                @if($view_invoice->advance_payment>0)
                                    <tr class="b-top-none">
                                        <td colspan="2">Advance Payment</td>
                                        <td class="text-right amount" data-a-sign=" $  ">{{"Tk. ".number_format((float)$view_invoice->advance_payment, 2, '.', '')}}</td>
                                    </tr>
                                @endif


                                <tr class="b-top-none">
                                    <?php $discount=0; ?>
                                    @if($view_invoice->discount_type=='Percent')
                                        <td colspan="2">Discount({{$view_invoice->discount}}%)</td>
                                        <td class="text-right amount" data-a-sign=" $  ">
                                            {{"Tk. - ".number_format((float)$total_price*$view_invoice->discount*0.01, 2, '.', '')}}
                                        </td>
                                            <?php $discount=$total_price*$view_invoice->discount*0.01; ?>
                                    @elseif($view_invoice->discount_type=='Fixed')
                                        <td colspan="2">Fixed Discount</td>
                                        <td class="text-right amount" data-a-sign=" $  ">
                                            {{"Tk. - ".number_format((float)$view_invoice->discount,2, '.', '')}}
                                        </td>
                                            <?php $discount=$view_invoice->discount;?>
                                    @endif
                                </tr>
                                <tr class="h4">
                                    <td colspan="2">Grand Total</td>
                                    <td class="text-right amount" data-a-sign=" $  ">  {{"Tk. ".number_format((float)$total_price-$discount-$view_invoice->advance_payment+$view_invoice->delivery_charge, 2, '.', '')}}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="text-right">

                <br>
                <a href="{{URL::to('/download-invoice/'.$view_invoice->invoices_id)}}" class="btn btn-primary ml-sm"><i class="fa fa-print"></i> Download PDF</a>
                <a href="{{URL::to('/view-print/'.$view_invoice->invoices_id)}})}}" target="_blank" class="btn btn-primary ml-sm"><i class="fa fa-print"></i> Printable Version</a>
            </div>
        </div>
    </section>

</div>

<!-- Mainly scripts -->
<script src="js/jquery-1.10.2.js"></script>
<script src="{{URL::asset('js/jquery-ui-1.10.4.min.js')}}"></script>
<script>
    var _L = [];
    var config_animate = 'No';

</script>
<script src="{{URL::asset('js/bootstrap.min.js')}}"></script>
<script src="{{URL::asset('js/jquery.metisMenu.js')}}"></script>
<script src="{{URL::asset('js/jquery.slimscroll.min.js')}}"></script>
<!-- Custom and plugin javascript -->
<script src="{{URL::asset('js/lib/moment.js')}}"></script>

<script src="{{URL::asset('js/app.js')}}"></script>
<script src="{{URL::asset('js/lib/pace.min.js')}}"></script>
<script src="{{URL::asset('js/lib/progress.js')}}"></script>
<script src="{{URL::asset('js/lib/bootbox.min.js')}}"></script>

<!-- iCheck -->
<script src="{{URL::asset('js/lib/icheck.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('js/lib/numeric.js')}}"></script>

<script>
    jQuery(document).ready(function() {
        // initiate layout and plugins

        $('.amount').autoNumeric('init', {

            aSign: '$ ',
            dGroup: 3,
            aPad: true,
            pSign: 'p',
            aDec: '.',
            aSep: ','

        });

    });

</script>
</body>

</html>
