@extends('admin.master')
@section('content')
<div class="row wrapper white-bg page-heading">
        <div class="col-lg-12">
            <h2 style="color: #2F4050; font-size: 16px; font-weight: 400; margin-top: 18px"> Invoices<div class="btn-group pull-right" style="padding-right: 10px;">

                </div> </h2>

        </div>

    </div>

    <div class="wrapper wrapper-content animated fadeIn">







        <div class="row">
            <div class="col-md-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">

                        <div class="ibox-tools">

                            <a href="{{URL::to('/create-invoice')}}" class="btn btn-primary btn-xs"><i class="fa fa-plus"></i> Add Invoice</a>

                        </div>
                    </div>
                    <div class="ibox-content">


                        <table class="table table-bordered table-hover sys_table footable" >
                            <thead>
                            <tr>
                                <th>Inv No#</th>
                                <th>Customer Name</th>
                                <th>Company Name</th>
                                <th>Amount</th>
                                <th>Invoice Date</th>
                                <th>Due Date</th>
                                <th>
                                    Status
                                </th>

                                <th class="text-right">Manage</th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($all_invoice_list as $value)

                            <tr>
                                <td  data-value="{{$value->invoice_code}}"><a href="">{{$value->invoice_code}}</a> </td>
                                <td data-value="{{$value->customer_name}}">{{$value->customer_name}}</td>
                                <td data-value="{{$value->company_name}}">{{$value->company_name}}</td>
                                <td class="amount" data-a-sign=" $  ">{{$value->total_price}}</td>
                                <td data-value="{{$value->invoice_date}}">{{$value->invoice_date}}</td>
                                <td data-value="{{$value->due_date}}">{{$value->due_date}}</td>
                                <td>
                                @if($value->status == "unpaid")
                                    <span class="label label-warning">Unpaid</span>
                                @elseif($value->status == "paid")
                                        <span class="label label-success">Paid</span>
                                @elseif($value->status == "cancelled")
                                        <span class="label label-danger">Cancel</span>
                                @elseif($value->status == "partial paid")
                                        <span class="label label-info">Partially Paid</span>
                                @endif


                                </td>

                                <td class="text-right">
                                    <a href="{{URL::to('/view-invoice/'.$value->invoice_id)}}" class="btn btn-primary btn-xs" ><i class="fa fa-check"></i> View</a>
                                    @if($value->status == "unpaid" || $value->status == "cancelled" || $value->status == "partial paid")
                                        <a href="{{URL::to('/paid-invoice/'.$value->invoice_id)}}" class="btn btn-green btn-xs status-change"><i class="fa fa-thumbs-o-up"></i> Paid</a>
                                        <a href="{{URL::to('/cancel-invoice/'.$value->invoice_id)}}" class="btn btn-warning btn-xs status-change"><i class="fa fa-times"></i> Cancelled</a>
                                    @endif
                                    <a href="{{URL::to('/edit-invoice/'.$value->invoice_id)}}" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i> Edit</a>
                                    <a href="{{URL::to('/delete-invoice/'.$value->invoice_id)}}"  class="btn btn-danger btn-xs cdelete status-change" id="iid1595"><i class="fa fa-trash"></i> Delete</a>
                                </td>
                            </tr>
                                @endforeach


                            </tbody>


                        </table>
                    <div class="row">
                        <div class="text-left">
                            {{$all_invoice_list->links()}}
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



<script type="text/javascript">

    $(document).ready(function(){
        $('.status-change').on('click', function(event){
            event.preventDefault();
            var ref_link= $(this).attr("href");
            bootbox.confirm(
                {
                    title: "Alert",
                    message: "Are you sure? ",
                    buttons: {
                        cancel: {
                            label: '<i class="fa fa-times"></i> Cancel',
                            className: 'btn-danger'
                        },
                        confirm: {
                            label: '<i class="fa fa-check"></i> Confirm',
                            className: 'btn-success'
                        }
                    }, callback: function(result) {
                    if (result) {
                        //include the href duplication link here?;
                        window.location=ref_link;


                    }
                }
                });
        });
    });

</script>

@endsection
