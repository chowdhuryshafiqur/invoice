
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />

    <title>Invoice No- {{$data['invoices_id']}}</title>

    <style>

        * { margin: 0; padding: 0; }
        body {
            font: 14px/1.4 Helvetica, Arial, sans-serif;
        }
        #page-wrap { width: 720px; margin: 0 auto; }

        textarea { border: 0; font: 14px Helvetica, Arial, sans-serif; overflow: hidden; resize: none; }
        table { border-collapse: collapse; }
        table td, table th { border: 0.3px solid rgba(1, 1, 1, 0.74); padding: 5px 15px; }

        #header { height: 15px; width: 100%; margin: 20px 0; background: #222; text-align: center; color: white; font: bold 15px Helvetica, Sans-Serif; text-decoration: uppercase; letter-spacing: 20px; padding: 8px 0px; }

        #address { width: 250px; height: 150px; float: left; }


        #logo { text-align: right; float: right; position: relative; margin-top: 25px; border: 1px solid #fff; max-width: 540px; overflow: hidden; }
        #customer-title { font-size: 20px; font-weight: bold; float: left; }

        #meta { margin-top: 1px; width: 100%; float: right; margin-bottom: 20px;}
        #meta td { text-align: right;  }
        #meta td.meta-head { text-align: left; background: #eee; }
        #meta td textarea { width: 100%; height: 20px; text-align: right; }

        #items { clear: both; width: 100%; margin: 30px 0 0 0; border: 0.3px solid rgba(0, 0, 0, 0.64); }
        #items th { background: #eee; padding: 10px 15px;}
        #items textarea { width: 80px; height: 50px; }
        #items tr.item-row td {  vertical-align: top; padding: 12px 15px;}
        #items td.description { width: 300px; }
        #items td.item-name { width: 175px; }
        #items td.description textarea, #items td.item-name textarea { width: 100%; }
        #items td.total-line { border-right: 0; text-align: right; }
        #items td.total-value { border-left: 0; padding: 10px 15px; }
        #items td.total-value textarea { height: 20px; background: none; }
        #items td.balance { background: #eee; }
        #items td.blank { border: 0; }

        #terms { text-align: center; margin: 20px 0 0 0; }
        #terms h5 { text-transform: uppercase; font: 13px Helvetica, Sans-Serif; letter-spacing: 10px; border-bottom: .3px solid rgba(0, 0, 0, 0.73); padding: 0 0 8px 0; margin: 0 0 8px 0; }
        #terms textarea { width: 100%; text-align: center;}

#customer{
    color: rgba(10, 10, 10, 0.91);
}

        .delete-wpr { position: relative; }
        .delete { display: block; color: #000; text-decoration: none; position: absolute; background: #EEEEEE; font-weight: bold; padding: 0px 3px; border: 1px solid; top: -6px; left: -22px; font-family: Verdana; font-size: 12px; }

        /* Extra CSS for Print Button*/
        .button {
            display: -webkit-box;
            display: -webkit-flex;
            display: -ms-flexbox;
            display: flex;
            overflow: hidden;
            margin-top: 20px;
            padding: 12px 12px;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            -webkit-transition: all 60ms ease-in-out;
            transition: all 60ms ease-in-out;
            text-align: center;
            white-space: nowrap;
            text-decoration: none !important;

            color: #fff;
            border: 0 none;
            border-radius: 4px;
            font-size: 14px;
            font-weight: 500;
            line-height: 1.3;
            -webkit-appearance: none;
            -moz-appearance: none;

            -webkit-box-pack: center;
            -webkit-justify-content: center;
            -ms-flex-pack: center;
            justify-content: center;
            -webkit-box-align: center;
            -webkit-align-items: center;
            -ms-flex-align: center;
            align-items: center;
            -webkit-box-flex: 0;
            -webkit-flex: 0 0 160px;
            -ms-flex: 0 0 160px;
            flex: 0 0 160px;
        }
        .button:hover {
            -webkit-transition: all 60ms ease;
            transition: all 60ms ease;
            opacity: .85;
        }
        .button:active {
            -webkit-transition: all 60ms ease;
            transition: all 60ms ease;
            opacity: .75;
        }
        .button:focus {
            outline: 1px dotted #959595;
            outline-offset: -4px;
        }

        .button.-regular {
            color: #202129;
            background-color: #edeeee;
        }
        .button.-regular:hover {
            color: #202129;
            background-color: #e1e2e2;
            opacity: 1;
        }
        .button.-regular:active {
            background-color: #d5d6d6;
            opacity: 1;
        }

        .button.-dark {
            color: #FFFFFF;
            background: #333030;
        }
        .button.-dark:focus {
            outline: 1px dotted white;
            outline-offset: -4px;
        }
        .footer{
            bottom: 0;
            position: fixed;
            width: 100%;
            text-align: center;
        }

        @media print
        {
            .no-print, .no-print *
            {
                display: none !important;
            }
        }

    </style>

</head>

<body>

<div id="page-wrap">

    <table width="100%" style="margin-top: 20px;">
        <tr>
            <td style="border: 0;  text-align: left; padding-left: 0;" width="38%">
                @if(!empty($data['logo']))
                <img height="70px" src="{{URL::asset('/').$data['logo']}}" alt="Logo"/>
                @else
                <img width="200px" src="{{URL::asset('/company_image/default.png')}}"alt="Logo"/>
                @endif
                <br>
            </td>


            <td style="border: 0;  text-align: right; padding-right: 0;" width="62%;">
                <div id="logo">
                 <!--   <strong>  {{--$data['company_name']--}}</strong>  -->
                    <br> {{$data['company_address']}}
                    <br>{{$data['company_city'].", ".$data['company_country']}}
                </div>
            </td>
        </tr>



    </table>

  <!--  <hr> -->
    <br>

    <div id="customer">

        <table id="meta">
            <tr>
                <td rowspan="5" style=" border: 0; padding-right: 100px; text-align: left; width:60%;  padding-left:0; font-size: 18px;">
                    <strong>{{$data['customer_name']}}</strong>
                    <br><br>


                    <strong>Address: </strong>
                    {{$data['customer_address']}}  <br>
                    {{$data['customer_city'].", ".$data['customer_country']}} <br>

                    @if(!empty($data['customer_zip_code']))
                    Postal Code: {{$data['customer_zip_code']}} <br>
                    <br/>
                    @endif

                    @if(!empty($data['customer_email']))
                    <strong>Email: </strong>{{$data['customer_email']}}
                    @endif
                    <br/>
                    <strong>Phone: </strong> {{$data['customer_phone']}}
                <td colspan="2" style="text-transform: uppercase; border: 0; padding-right: 0; width: 40%;"><h1>INVOICE {{--$data['invoice_prefix']==""?"INVOICE ":$data['invoice_prefix']--}}</h1></td>

            </tr>
            <tr style="font-weight: bold;">
                <td class="meta-head" style="border-left: 0.3px solid rgba(0,0,0,0.72); padding: 8px 15px;">NO# </td>
                <td><h2>{{$data['invoice_number']}}</h2></td>
            </tr>
            <tr>

                <td class="meta-head" style="border-left: 0.3px solid rgba(0,0,0,0.72); ">Invoice Date</td>
                <td>
              <?php
                $date = date("d-M-Y", strtotime($data['invoice_date']));

                ?>
                  {{$date}}
                </td>
            </tr>
            <tr>

                <td class="meta-head" style="border-left: 0.3px solid rgba(0,0,0,0.72); ">Shipping Method</td>
                <td>{{$data['shipping_method']}}</td>
            </tr>

        </table>

    </div>

    <table id="items" >

        <tr>
            <th align="left">Item Code</th>
            <th width="45%">Products</th>
            <th align="right" width="100px">Price</th>
            <th align="center">Qty</th>
            <th align="right" width="100px">Total</th>

        </tr>


        <?php

        $products=DB::table('products')
            ->where('invoice_id',$data['invoices_id'])
            ->get();

        $total_price = 0;
        foreach($products as $value){
            ?>

        <tr class="item-row">


            <td align="left">{{$value->product_code}}</td>
            <td class="description">{{$value->products}}</td>
            <td align="right">Tk. {{$value->price}}</td>
            <td align="center">{{$value->quantity}}</td>
            <td align="right"><span class="price">Tk. {{$value->price*$value->quantity}}</span></td>
        </tr>
        <?php
        $total_price += $value->price*$value->quantity;
        }
        ?>

        <tr>
            <td class="blank"> </td>
            <td class="blank"> </td>
            <td  class="total-line">Sub Total</td>
            <td colspan="2" class="total-value" align="right"> <div id="subtotal">{{"Tk. ".$total_price}}</div></td>
        </tr>
        @if($data['delivery_charge']>0)
            <tr>
                <td class="blank"> </td>
                <td class="blank"> </td>
                <td   class="total-line">Delivery Fee</td>
                <td  colspan="2" class="total-value" align="right"><div id="subtotal">{{"Tk. ".$data['delivery_charge']}}</div></td>
            </tr>
        @endif
        <?php $discount=0; ?>
        @if($data['discount_type']=='Percent')
            <?php $discount=$data['total_price']*$data['discount']*0.01; ?>
        <tr>
            <td class="blank"> </td>
            <td class="blank"> </td>
            <td  class="total-line">Discount ({{$data['discount']}}%)</td>
            <td colspan="2" class="total-value" align="right"><div id="subtotal">{{"Tk. - ".$total_price*$data['discount']*0.01}}</div></td>
        </tr>
        @elseif($data['discount_type']=='Fixed')
            <?php $discount=$data['discount'];?>
            <tr>
                <td class="blank"> </td>
                <td class="blank"> </td>
                <td  class="total-line">Fixed Discount</td>
                <td colspan="2" class="total-value" align="right"><div id="subtotal">{{"Tk. - ".$data['discount']}}</div></td>
            </tr>
        @endif

        @if($data['advance_payment']>0)
            <tr>
                <td class="blank"> </td>
                <td class="blank"> </td>
                <td class="total-line" >Adv. payment</td>
                <td   colspan="2"  class="total-value" align="right"><div id="subtotal">{{"Tk. - ".$data['advance_payment']}}</div></td>
            </tr>
        @endif
        <tr style="font-weight: bold; text-transform: uppercase; font-size: 18px;">
            <td class="blank"> </td>
            <td class="blank"> </td>
            <td  class="total-line balance">Total</td>
            <td colspan="2" class="total-value balance" align="right"><div class="due">{{"Tk. ".($total_price-$discount-$data['advance_payment']+$data['delivery_charge'])}}</div></td>
        </tr>
    </table>
    <table width="100%" style="border: 0; margin-top: 30px;">
        <tr>
            <td colspan="2"  style="border: 0;" >
                <p style="margin-bottom: 5px; "> <b >Special Notes:</b></p>
                <?php echo $data['notes'];?>
            </td>
        </tr>
    </table>
    <footer width="100%" style="border: 0;  position: fixed; bottom: 10px; right: 0px; height: 50px; ">
         <p style="margin-right: 50px; float: right; text-align: center; border-top: 0.3px solid rgba(0,0,0,0.72); width: 110px;">Signature</p>

    </footer>
        <!--
     <table class="footer">
         <tr>
             <td width="100%"> <div>Signature</div></td>
         </tr>
         <tr>
             <td></td>
             <td></td>
             <td></td>
             <td></td>
         </tr>
     </table>

     -->

    <!--    related transactions -->


    <!--    end related transactions -->



    <button class='button -dark center no-print'  onClick="window.print();">Click Here to Print</button>
</div>

</body>

</html>