@extends('admin.master')
@section('content')
    <link rel="stylesheet" type="text/css" href="{{URL::asset('css/dashboard_style.css')}}" />


    <div class="row wrapper white-bg page-heading">
        <div class="col-lg-12">
            <h2 style="color: #2F4050; font-size: 16px; font-weight: 400; margin-top: 18px">Dashboard </h2>

        </div>

    </div>

    <div class="wrapper wrapper-content animated fadeIn">
        <div class="row">
            <div class="col-lg-2 col-sm-6">
                <div class="circle-tile">
                    <a href="#">
                        <div class="circle-tile-heading dark-blue">
                            <i class="fa fa-tags fa-fw fa-3x"></i>
                        </div>
                    </a>
                    <div class="circle-tile-content dark-blue">
                        <div class="circle-tile-description text-faded">
                            Total Invoice
                        </div>
                        <div class="circle-tile-number text-faded">
                            {{$total_invoice->total_invoice}}

                            <span id="sparklineA"></span>
                        </div>
                        <a href="{{URL::to('/list-invoice')}}" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a>
                    </div>
                </div>
            </div>

            <div class="col-lg-2 col-sm-6">
                <div class="circle-tile">
                    <a href="#">
                        <div class="circle-tile-heading blue">
                            <i class="fa fa-money fa-fw fa-3x"></i>
                        </div>
                    </a>
                    <div class="circle-tile-content blue">
                        <div class="circle-tile-description text-faded">
                            Paid Invocie
                        </div>
                        <div class="circle-tile-number text-faded">
                            {{$total_paid->total_paid}}

                            <span id="sparklineB"></span>
                        </div>
                        <a href="{{URL::to('/paid-list-invoice')}}" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-sm-6">
                <div class="circle-tile">
                    <a href="#">
                        <div class="circle-tile-heading green">
                            <i class="fa fa-circle-o-notch fa-fw fa-3x"></i>
                        </div>
                    </a>
                    <div class="circle-tile-content green">
                        <div class="circle-tile-description text-faded">
                            Partial Invoice
                        </div>
                        <div class="circle-tile-number text-faded">
                            {{$total_partial_paid->total_partial_paid}}
                        </div>
                        <a href="partially-paid-list-invoice" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-sm-6">
                <div class="circle-tile">
                    <a href="#">
                        <div class="circle-tile-heading orange">
                            <i class="fa fa-thumbs-o-down fa-fw fa-3x"></i>
                        </div>
                    </a>
                    <div class="circle-tile-content orange">
                        <div class="circle-tile-description text-faded">
                            Unpaid Invoice
                        </div>
                        <div class="circle-tile-number text-faded">
                            {{$total_unpaid->total_unpaid}}
                        </div>
                        <a href="{{URL::to('/unpaid-list-invoice')}}" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a>
                    </div>
                </div>
            </div>

            <div class="col-lg-2 col-sm-6">
                <div class="circle-tile">
                    <a href="#">
                        <div class="circle-tile-heading red">
                            <i class="fa fa-ban fa-fw fa-3x"></i>
                        </div>
                    </a>
                    <div class="circle-tile-content red">
                        <div class="circle-tile-description text-faded">
                            Cancel Invoice
                        </div>
                        <div class="circle-tile-number text-faded">
                            {{ $total_cancel->total_cancel>0?$total_cancel->total_cancel:"0"}}
                            <span id="sparklineC"></span>
                        </div>
                        <a href="{{URL::to('/cancel-list-invoice')}}" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a>
                    </div>
                </div>
            </div>

            <div class="col-lg-2 col-sm-6">
                <div class="circle-tile">
                    <a href="#">
                        <div class="circle-tile-heading blue">
                            <i class="fa fa-comments fa-fw fa-3x"></i>
                        </div>
                    </a>
                    <div class="circle-tile-content blue">
                        <div class="circle-tile-description text-faded">
                            Last Month
                        </div>
                        <div class="circle-tile-number text-faded">

                          {{$last_month_amount->last_month_amount>0?$last_month_amount->last_month_amount:"0"}}
                            <span style="font-size: 13px;">TK</span>

                            <span id="sparklineD"></span>
                        </div>
                        <a href="#" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-sm-6">
                <div class="circle-tile">
                    <a href="#">
                        <div class="circle-tile-heading dark-blue">
                            <i class="fa fa-comments fa-fw fa-3x"></i>
                        </div>
                    </a>
                    <div class="circle-tile-content dark-blue">
                        <div class="circle-tile-description text-faded">
                            Total Amount
                        </div>
                        <div class="circle-tile-number text-faded">

                            {{$total_invoice->total_amount>0?$total_invoice->total_amount:'0'}}
                            <span style="font-size: 13px;">TK</span>
                            <span id="sparklineD"></span>
                        </div>
                        <a href="#" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-sm-6">
                <div class="circle-tile">
                    <a href="#">
                        <div class="circle-tile-heading blue">
                            <i class="fa fa-money fa-fw fa-3x"></i>
                        </div>
                    </a>
                    <div class="circle-tile-content blue">
                        <div class="circle-tile-description text-faded">
                             Paid Amount
                        </div>
                        <div class="circle-tile-number text-faded">
                            {{$total_paid->paid_amount>0?$total_paid->paid_amount:"0"}}
                            <span style="font-size: 13px;">TK</span>
                            <span id="sparklineD"></span>
                        </div>
                        <a href="#" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-sm-6">
                <div class="circle-tile">
                    <a href="#">
                        <div class="circle-tile-heading green">
                            <i class="fa fa-circle-o-notch fa-fw fa-3x"></i>
                        </div>
                    </a>
                    <div class="circle-tile-content green">
                        <div class="circle-tile-description text-faded">
                            Partial Amount
                        </div>
                        <div class="circle-tile-number text-faded">
                            {{$total_partial_paid->partial_paid_amount>0?$total_partial_paid->partial_paid_amount:"0"}}

                            <span style="font-size: 13px;">TK</span>
                            <span id="sparklineD"></span>
                        </div>
                        <a href="#" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-sm-6">
                <div class="circle-tile">
                    <a href="#">
                        <div class="circle-tile-heading orange">
                            <i class="fa fa-thumbs-o-down fa-fw fa-3x"></i>
                        </div>
                    </a>
                    <div class="circle-tile-content orange">
                        <div class="circle-tile-description text-faded">
                            Unpaid Amount
                        </div>
                        <div class="circle-tile-number text-faded">
                            {{$total_unpaid->unpaid_amount>0?$total_unpaid->unpaid_amount:"0"}}
                            <span style="font-size: 13px;">TK</span>
                            <span id="sparklineD"></span>
                        </div>
                        <a href="#" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a>
                    </div>
                </div>
            </div>



            <div class="col-lg-2 col-sm-6">
                <div class="circle-tile">
                    <a href="#">
                        <div class="circle-tile-heading purple">
                            <i class="fa fa-comments fa-fw fa-3x"></i>
                        </div>
                    </a>
                    <div class="circle-tile-content purple">
                        <div class="circle-tile-description text-faded">
                            Current Month
                        </div>
                        <div class="circle-tile-number text-faded">
                            {{$current_month_amount->current_month_amount>0?$current_month_amount->current_month_amount:'0'}}
                            <span style="font-size: 13px;">TK</span>
                            <span id="sparklineD"></span>
                        </div>
                        <a href="#" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-sm-6">
                <div class="circle-tile">
                    <a href="#">
                        <div class="circle-tile-heading blue">
                            <i class="fa fa-comments fa-fw fa-3x"></i>
                        </div>
                    </a>
                    <div class="circle-tile-content blue">
                        <div class="circle-tile-description text-faded">
                            Today Amount
                        </div>
                        <div class="circle-tile-number text-faded">
                            {{$today_amount>0?$today_amount:0}}

                            <span style="font-size: 13px;">TK</span>

                            <span id="sparklineD"></span>
                        </div>
                        <a href="#" class="circle-tile-footer">More Info <i class="fa fa-chevron-circle-right"></i></a>
                    </div>
                </div>
            </div>



        </div>

    </div>
@endsection