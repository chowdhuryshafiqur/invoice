@extends('admin.master')
@section('content')


    <div class="row wrapper white-bg page-heading">
        <div class="col-lg-12">
            <h2 style="color: #2F4050; font-size: 16px; font-weight: 400; margin-top: 18px">Dashboard </h2>

        </div>

    </div>

    <div class="wrapper wrapper-content animated fadeIn">

        <div class="row">
            <div class="col-md-3">
                <div class="ibox float-e-margins">
                    <div class="col-md-6 dashboard-tag" style="background-color:rgb(243,165,48);">

                        <i class="fa fa-tags" style="font-size:50px; color:#fff;margin:23px 20px;"   aria-hidden="true"></i>




                    </div>
                    <div class="col-md-6 dashboard-tag" style="background-color:rgb(244,181,88)">
                        <h1 style="text-align:right; font-size:30px; color:#fff; margin: 20px 0px;">{{$total_invoice->total_invoice}}</h1>
                        <h6 style="text-align:right; color:#fff; font-size:12px;">Total Invoice</h6></div>





                </div>
            </div>

            <div class="col-md-3">
                <div class="ibox float-e-margins">
                    <div class="col-md-6 dashboard-tag" style="background-color:#2196f3;">

                        <i class="fa fa-money" style="font-size:50px;color:#fff;  margin:23px 23px;"   aria-hidden="true"></i>



                        </div>
                    <div class="col-md-6 dashboard-tag" style="background-color:rgb(93,147,201);">
                        <h1 style="text-align:right; color:#fff; font-size:30px; margin: 20px 0px;">{{$total_paid->total_paid}}</h1>
                        <h6 style="text-align:right; color:#fff; font-size:12px;">Paid Invoice</h6></div>





                </div>
            </div>

                <div class="col-md-3">
                    <div class="ibox float-e-margins">
                        <div class="col-md-6 dashboard-tag" style="background-color:rgb(136,197,66);">

                            <i class="fa fa-circle-o-notch" style="font-size:50px; margin:23px 23px; color:#fff;"   aria-hidden="true"></i>



                        </div>
                        <div class="col-md-6 dashboard-tag" style="background-color:rgb(158,207,102);">
                            <h1 style="text-align:right; font-size:30px; margin: 20px 0px; color:#fff;">{{$total_partial_paid->total_partial_paid}}</h1>
                            <h6 style="text-align:right; color:#fff; font-size:12px;">Partial Paid</h6></div>





                    </div>
                </div>




                    <div class="col-md-3">
                        <div class="ibox float-e-margins">
                            <div class="col-md-6 dashboard-tag" style="background-color:rgb(217,83,79);">

                                <i class="fa fa-thumbs-o-down" style="font-size:50px; margin:23px 20px; color: #fff;"   aria-hidden="true"></i>



                            </div>
                            <div class="col-md-6 dashboard-tag" style="background-color:rgba(217,83,79,0.83);">
                                <h1 style="text-align:right; font-size:30px; color:#fff; margin: 20px 0px;">{{$total_unpaid->total_unpaid}}</h1>
                                <h6 style="text-align:right; color:#fff; font-size:12px;">Unpaid Invoice</h6></div>





                        </div>
                    </div>






                </div>



        <div class="row" style="margin-top: 33px;">

            <div class="col-md-3" >
                <div class="ibox float-e-margins">
                    <div class="col-md-5 dashboard-tag" style="background-color:#B22222;">

                        <i class="fa fa-ban" style="font-size:39px; margin:23px 20px; color: #fff;"   aria-hidden="true"></i>



                    </div>
                    <div class="col-md-7 dashboard-tag" style="background-color:#DC143C;">
                        <h1 style="text-align:right; font-size:20px; color:#fff; margin: 20px 0px 19px;">{{ $total_cancel->total_cancel>0?$total_cancel->total_cancel:"0"}}</h1>
                        <h6 style="text-align:right; color:#fff; font-size:13px;">Cancelled Invoice</h6></div>





                </div>
            </div>

            <div class="col-md-3">
                <div class="ibox float-e-margins">
                    <div class="col-md-5 dashboard-tag" style="background-color:  #DA542F  ;">

                        <i class="" style="font-size:60px; color:#fff;margin:23px 23px; text-align:left;"   aria-hidden="true">&#2547</i>





                    </div>
                    <div class="col-md-7 dashboard-tag" style="background-color:#DA542F   ">
                        <h6 style="text-align:right; color:#fff;  font-size:20px; margin: 20px 0px 19px;">{{$total_invoice->total_amount>0?$total_invoice->total_amount:'0'}}</h6>
                        <h6 style="text-align:right; color:#fff; font-size:13px;">Total Amount</h6></div>





                </div>
            </div>

            <div class="col-md-3">
                <div class="ibox float-e-margins">
                    <div class="col-md-5 dashboard-tag" style="background-color: #19BC9C ;">

                        <i class="" style="font-size:60px;color:#fff;  margin:23px 23px;"   aria-hidden="true">&#2547</i>



                    </div>
                    <div class="col-md-7 dashboard-tag" style="background-color:#19BC9C;">
                        <h6 style="text-align:right; color:#fff; font-size:21px; margin: 20px 0px 19px;">{{$total_paid->paid_amount>0?$total_paid->paid_amount:"0"}}</h6>
                        <h6 style="text-align:right; color:#fff; font-size:12px;">Paid Amount</h6></div>





                </div>
            </div>

            <div class="col-md-3">
                <div class="ibox float-e-margins">
                    <div class="col-md-5 dashboard-tag" style="background-color:#F74D4D;">

                        <i class="" style="font-size:60px; margin:23px 23px; color:#fff;"   aria-hidden="true">&#2547</i>



                    </div>
                    <div class="col-md-7 dashboard-tag" style="background-color:#F74D4D;">
                        <h1 style="text-align:right; color:#fff; font-size:20px; margin: 20px 0px 19px;">{{$total_partial_paid->partial_paid_amount>0?$total_partial_paid->partial_paid_amount:"0"}}</h1>
                        <h6 style="text-align:right; color:#fff; font-size:13px;">Partial Amount</h6></div>





                </div>
            </div>






        </div>

        <div class="row" style="margin-top: 33px;">
            <div class="col-md-3">
                <div class="ibox float-e-margins">
                    <div class="col-md-5 dashboard-tag" style="background-color:#4CB1CF;">

                        <i class="" style="font-size:60px; margin:23px 20px; color: #fff;"   aria-hidden="true">&#2547</i>



                    </div>
                    <div class="col-md-7 dashboard-tag" style="background-color:#4CB1CF;">
                        <h1 style="text-align:right; color:#fff; font-size:20px; margin: 20px 0px 19px;">{{$total_unpaid->unpaid_amount>0?$total_unpaid->unpaid_amount:"0"}}</h1>
                        <h6 style="text-align:right; color:#fff; font-size:13px;">Unpaid Amount</h6></div>





                </div>
            </div>

            <div class="col-md-3">
                <div class="ibox float-e-margins">
                    <div class="col-md-5 dashboard-tag" style="background-color:#5D6D7E;">

                        <i class="" style="font-size:60px; margin:23px 20px; color: #fff;"   aria-hidden="true">&#2547</i>



                    </div>
                    <div class="col-md-7 dashboard-tag" style="background-color:#34495E;">
                        <h1 style="text-align:right; font-size:20px; color:#fff; margin: 20px 0px 19px;">{{$last_month_amount->last_month_amount>0?$last_month_amount->last_month_amount:"0"}}</h1>
                        <h6 style="text-align:right; color:#fff; font-size:12.4px;">Last Month Invoice</h6></div>

                </div>
            </div>


            <div class="col-md-3">
                <div class="ibox float-e-margins">
                    <div class="col-md-5 dashboard-tag" style="background-color:#4CB1CF;">

                        <i class="" style="font-size:60px; margin:23px 20px; color: #fff;"   aria-hidden="true">&#2547</i>



                    </div>
                    <div class="col-md-7 dashboard-tag" style="background-color:#4CB1CF;">
                        <h1 style="text-align:right; font-size:25px; color:#fff; margin: 20px 0px 19px;">{{$current_month_amount->current_month_amount>0?$current_month_amount->current_month_amount:"0"}}</h1>
                        <h6 style="text-align:right; color:#fff; font-size:8.5px;">Current Month Invoice</h6></div>

                </div>
            </div>
        </div>









        @endsection

@section('javascript')

    <script type="text/javascript" src="{{URL::asset('js/lib/graph.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('js/lib/echarts.min.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('js/lib/dashboard.js')}}"></script>
    @endsection