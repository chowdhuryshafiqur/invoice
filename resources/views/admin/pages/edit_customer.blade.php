@extends('admin.master')
@section('content')


	<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.0/jquery.validate.min.js"></script>



	<div class="row wrapper white-bg page-heading">
		<div class="col-lg-12">
			<h2 style="color: #2F4050; font-size: 16px; font-weight: 400; margin-top: 18px"> Customer </h2>

		</div>

	</div>

	<div class="wrapper wrapper-content animated fadeIn">


		<div class="wrapper wrapper-content">
			<div class="row">

				<div class="col-md-12">



					<div class="ibox float-e-margins">
						<div class="ibox-title">
							<h5>Edit Customer</h5>

						</div>
						<div class="ibox-content" id="ibox_form">
							@include('admin.partials.message')




							{!! Form::open(['url' => '/update-customer','class'=>'form-horizontal','id'=>'rform','name'=>'edit_customer']) !!}

							<div class="row">
								<div class="col-md-6 col-sm-12">


									<div class="form-group">
										<label class="col-md-4 control-label" for="account">Full Name <small class="red">*</small> </label>
										<div class="col-lg-8">
											<input type="text" id="full_name" value="{{$edit_customer->customer_name}}" name="customer_name" class="form-control"  required autofocus>
											<input type="hidden" id="full_name" value="{{$edit_customer->customer_id}}" name="customer_id" class="form-control" autofocus>

										</div>
									</div>

									

									<div class="form-group">
										<label class="col-md-4 control-label" for="phone">Mobile <small class="red">*</small></label>
										<div class="col-lg-8">
											<input type="text" id="phone" required name="phone" value="{{$edit_customer->phone}}" class="form-control">
										</div>
									</div>

									<div class="form-group">
										<label class="col-md-4 control-label" for="address">Address <small class="red">*</small></label>
										<div class="col-lg-8">
											<input type="text" value="{{$edit_customer->address}}" id="address" name="address" required class="form-control">
										</div>
									</div>

<div class="form-group">
										<label class="col-md-4 control-label" for="email">Email</label>

										<div class="col-lg-8">
											<input type="email" id="email" name="email" value="{{$edit_customer->email}}" class="form-control">
										</div>
									</div>
									
									<div class="form-group">
										<label class="col-md-4 control-label" for="city">City</label>

										<div class="col-lg-8">
											<input type="text" value="{{$edit_customer->city}}"id="city" name="city" class="form-control">
										</div>
									</div>

									<div class="form-group">
										<label class="col-md-4 control-label" for="zip">ZIP/Postal Code </label>
										<div class="col-lg-8">
											<input type="text" id="zip" value="{{$edit_customer->zip_code}}" name="zip_code" class="form-control">
										</div>
									</div>

									<div class="form-group" hidden>
										<label class="col-md-4 control-label" for="country">Country <small class="red">*</small></label>

										<div class="col-lg-8">
											<select name="country_id" id="country" value="{{$edit_customer->country_id}}" class="form-control">
												<option value="">Select Country</option>
												@foreach($countries as $value)
													<option value="{{$value->id}}">{{$value->countryName}}</option>
												@endforeach
											</select>

										</div>
									</div>





								</div>

							</div>


							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
										<div class="col-md-offset-2 col-lg-10">

											<button class="md-btn md-btn-primary waves-effect waves-light" type="submit" name="submit" id="btn"><i class="fa fa-check"></i> Save</button> | <a href="">Or Cancel</a>


										</div>
									</div>
								</div>
							</div>


							{!! Form::close() !!}
						</div>
					</div>
				</div>
			</div>


		</div>

	</div>



	</div>


	</div>
@endsection


@section('javascript')
	<script type="text/javascript">
        $(document).ready(function() {
            $("#rform").validate({
                rules: {
                    customer_name: "required",
                    phone: "required",
                    address: "required"
                },
                messages: {
                    customer_name: "Please specify Customer Name",
                    phone: "Please specify your Valid Phone",
                    address: "Please specify your Address"

                }
            })



            $('#btn').click(function() {
                //alert();
                $("#rform").valid();
            });


            $("#companies").select2({
                theme: "bootstrap"
            });

            $("#country").select2({
                theme: "bootstrap"
            });



        });
	</script>

	<script type="text/javascript">
        document.forms['edit_customer'].elements['country_id'].value={{$edit_customer->country_id}};
	</script>
@endsection