
@extends('admin.master')
@section('content')



    <div class="row wrapper white-bg page-heading">
        <div class="col-lg-12">
            <h2 style="color: #2F4050; font-size: 16px; font-weight: 400; margin-top: 18px"> Create Invoice </h2>

        </div>

    </div>
    <div class="wrapper wrapper-content animated fadeIn">

        {!! Form::open(['url' => '/update-invoice','id'=>'invform','method'=>'post','name'=>'update-invoice']) !!}


        <div class="col-md-8">
            @include('admin.partials.message')
            <div class="panel panel-default">
                <div class="col-lg-12">
                    <h2 style="color: #2F4050; font-size: 14px; font-weight: 700; margin-top: 30px; margin-left: 10px;"> Add Product Detials </h2>

                </div>
                <div class="panel-body">

                    <div class="table-responsive m-t">
                        <table class="table invoice-table" id="invoice_items">
                            <thead>
                            <tr>
                                <th width="10%">Item Code</th>
                                <th width="50%">Products</th>
                                <th width="10%">Qty</th>
                                <th width="10%">Price</th>
                                <th width="10%">Total</th>


                            </tr>
                            </thead>
                            <tbody>\
                            <input type="hidden" class="form-control" name="invoice_id" id="invoice_id" value="{{$edit_invoice->invoices_id}}">
                            <input type="hidden" class="form-control" name="user_id" value="{{$edit_invoice->created_by}}">
                            <input type="hidden" class="form-control" name="total_amount" id="total_amount" value="0">
                            @foreach($products as $key => $value)
                                <tr>
                                    <td><input type="text" class="form-control" name="item_code[]" value="{{$value->product_code}}">
                                        <input type="hidden" class="form-control product_id" name="product_id[]" value="{{$value->id}}" >
                                    </td>
                                    <td><textarea class="form-control item_name" name="product[]" rows="3">{{$value->products}}</textarea> </td>
                                    <td><input type="number" class="form-control qty" value="{{$value->quantity}}" name="qty[]"  id="total_pro_qty_{{$key}}" onkeyup="add_row_qty(this.value, {{$key}})"></td>

                                    <td><input type="number" class="form-control item_price" name="price[]" value="{{$value->price}}" id="per_price_{{$key}}" onkeyup="add_row_price(this.value,{{$key}})"  data-qty="#total_pro_qty_{{$key}}" data-price="#per_price_{{$key}}" data-target="#total_pro_price_{{$key}}" ></td>
                                    <td class="ltotal"><input type="text" id="total_pro_price_{{$key}}" readonly class="form-control per-total-price" data-total="{{$key}}" ></td>
                                </tr>
                            @endforeach

                            </tbody>
                        </table>

                    </div>


                    <!--   <div class="col-md-12">
                         <button type="button" class="btn btn-primary" id="blank-add"><i
                                     class="fa fa-plus"></i> Add New Product</button>

                         <button type="button" class="btn btn-danger" id="item-remove"><i
                                     class="fa fa-minus-circle"></i> Delete</button>
                     </div>-->

                    <div class="col-md-8">
                        <div class="col-lg-12" style="    padding-left: 0px;">
                            <h2 style="color: #2F4050; font-size: 14px; font-weight: 700;  margin-left: 0px;"> Notes: </h2>

                        </div>
                        <br>
                        <br>
                        <br>
                        <textarea class="form-control" name="notes" id="notes" rows="3"
                                  placeholder="Invoice Notes...">{{$edit_invoice->notes}}</textarea>
                        <br>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group" style="margin-top: 55px;">
                            <label for="idate">Delivery Charge</label>
                            <input type="text" class="form-control" id="delivery_payment" name="delivery_charge" value="{{$edit_invoice->delivery_charge}}">
                        </div>
                        <div class="form-group" >
                            <label for="cn">Payment Status</label>
                            <select name="status" id="status" class="form-control">
                                <option value="unpaid">Unpaid</option>
                                <option value="paid">Fully Paid</option>
                                <option value="partial paid">Advance Payment</option>
                                <option value="cancelled">Cancelled</option>
                            </select>
                        </div>

                        <div class="form-group" id="adv_payment">
                            <label for="idate">Advance Payment</label>
                            <input type="text" class="form-control" id="advance_payment" name="advance_payment" value="{{$edit_invoice->advance_payment}}">
                        </div>





                        <div class="form-group">
                            <label for="add_discount">
                                <a href="#" data-target="#set_add_discount" data-toggle="modal" class="btn btn-info btn-xs" style="margin-top: 5px;">
                                    <i class="fa fa-minus-circle"></i> Set Discount</a>
                            </label>

                        </div>
                    </div>

                </div>
            </div>

            <input type="hidden" name="discount" id="discount_percent" value="{{$edit_invoice->discount}}">
            <input type="hidden" name="discount_type" id="type_of_discount" value="{{$edit_invoice->discount_type}}">
            <input type="hidden" class="form-control" name="total_amount" id="total_amount">
            <input type="hidden" class="form-control" name="total_invoice_amount" id="total_invoice_amount">
            <input type="hidden" class="form-control" name="adv_invoice_amount" id="adv_invoice_amount">
        </div>

        <div class="col-md-4">

            <!--
                   <div class="panel panel-default">
                       <div class="panel-body">

                           <div class="text-right">
                               <!--     <button class="btn btn-primary" id="submit" type="submit"><i class="fa fa-save"></i> Save Invoice</button>  -->
        <!--         <input type="submit" class="btn btn-primary" value="Save Invoice"/>
                    <a class="btn btn-danger" href="{{URL::to('/')}}"><i class="fa fa-times"></i> Close</a>

                </div>

            </div>
        </div>
-->
        <!--       <div class="panel panel-default">
            <div class="panel-body">

                <table class="table invoice-total">
                    <tbody>
                    <tr>
                        <td><strong>Sub Total :</strong></td>
                        <td id="sub_total" class="amount">
                        </td>
                    </tr>
                    <tr>
                                   <td><strong>Qty <span id="is_pt"></span> :</strong></td>
                                   <td id="quantity" >0
                                   </td>
                               </tr>

                    <tr>
                        <td><strong>Discount <span id="is_pt"></span> :</strong></td>
                        <td id="discount_amount_total" class="amount">{{$edit_invoice->discount}}%
                        </td>
                    </tr>
                    <tr>
                        <td><strong>TOTAL :</strong></td>
                        <td id="total" class="amount">
                        </td>
                    </tr>
                    </tbody>
                </table>

            </div>
        </div>-->

            <div class="panel panel-default">
                <div class="panel-body">

                    <div>

                        <div class="form-group">
                            <label for="cid">Customer</label>

                            <select id="cid" class="customer_id" name="customer_id" class="form-control" required>
                                <option value="">Select Contact...</option>
                                @foreach($customers as $value)
                                    <option value="{{$value->customer_id}}">{{$value->customer_name.' ( '.$value->city.' ) '}}</option>
                                @endforeach

                            </select>
                            <span><a href="#" data-toggle="modal" data-target="#add_customer">| Add Customer Info</a> <a href="{{URL::to('/edit-customer/'.$edit_invoice->customer_id)}}" style=" float: right;" id="edit_customer">| Or Edit Customer Info</a> </span>
                        </div>

                        <div class="form-group">
                            <label for="address">Address</label>

                            <textarea id="invoice_address" name="invoice_address" readonly class="form-control" rows="2"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="cid">Invoice Store</label>

                            <select id="company" name="company_id" class="form-control" required>
                                <option value="">Select Invoiceing Store ...</option>
                                @foreach($companies as $value)
                                    <option value="{{$value->id}}"> {{$value->company_name}}  </option>
                                @endforeach

                            </select>
                        </div>

                        <div class="form-group" id="invoice_date">
                            <label for="idate">Invoice Date</label>

                            <input type="text" class="form-control" id="idate" name="invoice_date" datepicker data-date-format="yyyy-mm-dd" data-auto-close="true" value="{{$edit_invoice->invoice_date}}">
                        </div>



                        <div class="form-group">
                            <label for="cn">Shipping Method</label>
                            <select name="shipping_method" class="form-control">
                                <option >Select Shipping Method...</option>
                                <option value="Home Delivery">Home Delivery</option>
                                <option value="Condition Delivery">Condition Delivery</option>
                            </select>
                        </div>



                    </div>

                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-body">

                    <div class="text-right">
                        <!--   <button class="btn btn-primary" id="submit" type="submit"><i class="fa fa-save"></i> Save Invoice</button> -->
                        <input type="submit" class="btn btn-primary" value="View Update"/>
                        <a class="btn btn-danger" href="{{URL::to('/list-invoice')}}"><i class="fa fa-times"></i> Close</a>

                    </div>

                </div>
            </div>


        </div>

        {!! Form::close() !!}


    </div>


    <div id="add_customer" class="modal fade add-customer-modal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">


                <!--               <form class="form-horizontal" name="customer_info" id="customer_info"> -->
                {!! Form::open(['url'=>'/save-customer-ajax','method'=>'POST','name'=>"customer_info", 'id'=>"customer_info"]) !!}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Customer</h4>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
                        <label class="col-md-3">Full Name </label>
                        <div class="col-md-9">
                            <input class="form-control" type="text" name="customer_name"/>

                        </div>
                    </div>
                    <div class="col-md-12">
                        <label class="col-md-3">Mobile</label>
                        <div class="col-md-9">
                            <input class="form-control" type="text" name="phone"/>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label class="col-md-3">Address</label>
                        <div class="col-md-9">
                            <input class="form-control" type="text" name="address"/>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label class="col-md-3">Email</label>
                        <div class="col-md-9">
                            <input class="form-control" type="email" name="email"/>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <label class="col-md-3">City</label>
                        <div class="col-md-9">
                            <input class="form-control" type="text" name="city"/>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label class="col-md-3">Zip / Postal Code</label>
                        <div class="col-md-9">
                            <input class="form-control" type="text" name="zip_code"/>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <label class="col-md-3">Country</label>
                        <div class="col-md-9">
                            <select name="country_id" id="country" class="form-control">
                                <option>Select Country</option>
                                @foreach($countries as $value)
                                    <option value="{{$value->id}}">{{$value->countryName}}</option>
                                @endforeach

                            </select>
                        </div>
                    </div>

                    <br/>
                    <br/>
                    <br/>
                </div>
                <div class="modal-footer">
                    <div class="col-md-12">

                        <button type="submit" id="save_customer" class="btn btn-primary"><i class="fa fa-check"></i> Save </button>
                        <button type="button" class="btn btn-danger modal-close" style="margin-right: 30px;" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                    </div>
                </div>
                <!--        </form> -->
                {!! Form::close() !!}

            </div>

        </div>
    </div>


    </div>


    </div>

    <div id="set_add_discount" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">

                <form name="discount_info" id="discount_info">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Set Discount</h4>
                    </div>

                    <div class="modal-body">
                        <div class="col-md-12">
                            <label class="col-md-3 discount_text">Discount</label>
                            <div class="col-md-5">
                                <input class="form-control discount_amount" type="text" name="set_discount_amount"/>

                            </div>
                        </div>
                        <div class="col-md-12">
                            <label class="col-md-3">Discount Type</label>
                            <div class="col-md-9 discount">
                                <input  type="radio" name="set_discount_type" value="percent"/> Percentage (%) <br/>
                                <input  type="radio" name="set_discount_type" value="fixed"/> Fixed Price
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer discount">
                        <input type="submit" class="btn btn-primary"   data-dismiss="modal" id="discount_setting" value=" Success ">
                    </div>
                </form>
            </div>

        </div>
    </div>

    <input type="hidden" id="last_price" />
    <input type="hidden" id="last_qty" />
    <input type="hidden" id="last_row" />

@endsection


@section('javascript')
    <script type="text/javascript" src="{{URL::asset('js/lib/numeric.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('js/lib/select2.min.js')}}"></script>
    <script src="{{URL::asset('js/jquery.metisMenu.js')}}"></script>
    <script src="{{URL::asset('js/lib/app.js')}}"></script>
    <script type="text/javascript">

        $(document).ready(function () {


            $('#short_description').redactor(
                {
                    minHeight: 200, // pixels
                    paragraphize: false,
                    plugins: ['fontcolor'],
                    replaceDivs: false,
                    linebreaks: true
                }
            );
            $('#description').redactor(
                {
                    minHeight: 400, // pixels
                    paragraphize: false,
                    plugins: ['fontcolor'],
                    replaceDivs: false,
                    linebreaks: true
                }
            );

            $("#country").select2({
                theme: "bootstrap"
            });
            $("#company").select2({
                theme: "bootstrap"
            });



            $(".qty, .item_price, #advance_payment, #delivery_payment").keypress(function (e) {
                //if the letter is not digit then display error and don't type anything
                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    //display error message
                    return false;
                }
            });



            $(".item_price").change(function(){

                if($(this).val()!=''){

                    var target_total_id = $(this).attr("data-target");
                    var target_qty = $(this).attr("data-qty");
                    var per_price = $(this).attr("data-price");

                    //alert(target_total_id+target_qty+per_price);

                    // price = price.toFixed(2);
                    var qty = $(target_qty).val();
                    var price = $(per_price).val();

                    /*          if($("#dis_percent").val()!="" && $("#dis_type").val()=="Percent")
                     {
                     var discount = $("#dis_percent").val();
                     }else if($("#dis_type").val()=="Fixed"){
                     var discount = $("#dis_percent").val();
                     }else{

                     } */

                    var per_product_price = price*qty;

                    /*var total;
                     if(!$("#total_amount").val())
                     {
                     total = per_product_price+$("#total_amount").val();
                     }else{
                     total = per_product_price;
                     } */

                    if(price==''){
                        price  = "0";
                    }
                    //


                    // $('#sub_total').text(parseFloat(total).toFixed(2));

                    //  $('#quantity').text(qty);

                    //  $('#total').text(parseFloat(total).toFixed(2));
                    $(target_total_id).val(per_product_price);
                    // $("#total_amount").val(total);

                }

            }).trigger("change");

            $(".per-total-price").change(function(){

                var total_product = $(this).attr("data-total");

                for(var i=0; i<=total_product; i++){
                    var total = $("#total_pro_price_"+total_product).val();
                    var to_amount = $("#total_amount").val();
                    total=+total + +to_amount;
                }
                $("#total_amount").val(total);
                $('#sub_total').text(parseFloat(total).toFixed(2));
                $('#total').text(parseFloat(total).toFixed(2));

                // alert(total);

            }).trigger("change");



        });

        $('#discount_setting').click(function(){
            var dis_type = $('input[name=set_discount_type]:checked', '#discount_info').val()
            if(dis_type!=null){

                var get_discount = $('.discount_amount').val();
                var d_charge = $('#delivery_payment').val();
                if(!d_charge)
                    d_charge=0;
                if(dis_type=='percent'){
                    var discount = (100-get_discount)*0.01;
                    var amount_discount  = get_discount*0.01
                    $('#discount_amount_total').text(get_discount+" %");
                    var price = $('#sub_total').text();
                    price = parseFloat(price);
                    var discount_total = discount*price+ +d_charge;
                    $('#total').text(parseFloat(discount_total));
                    $("#type_of_discount").val('Percent');
                    $("#discount_percent").val(get_discount);

                }else if(dis_type=='fixed'){
                    $('#discount_amount_total').text("Fixed("+get_discount+")");
                    //  $('.lvtotal').val(discount);
                    // $('.item_price').val(discount);
                    $('#sub_total').text(discount);
                    var price = $('#sub_total').text();
                    var total = price-get_discount+ +d_charge;
                    $('#total').text(total);
                    $("#type_of_discount").val('Fixed');
                    $("#discount_percent").val(get_discount);
                }
            }
        });


        function add_row_qty(qty,row){

            var qty = parseFloat(qty);
            if(!qty)
                qty=0;

            document.getElementById("last_qty").value = qty;
            document.getElementById("last_row").value = row;

            total();
        }

        function add_row_price(price, row){

            var price = parseFloat(price);
            if(!price)
                price=0;


            var row = parseFloat(row);

            document.getElementById("last_price").value = price;
            document.getElementById("last_row").value = row;

            total();
        }



        function total(){
            var price = document.getElementById("last_price").value;
            var qty = document.getElementById("last_qty").value;
            var last_row = "#row_total_"+document.getElementById("last_row").value;

            var add_row_price = price*qty;
            var total_row = document.getElementById("last_row").value;

            var total = parseFloat($("#total_amount").val());

            var sub_total = total+add_row_price;

            $(last_row).val(add_row_price);
            $("#total_amount").val(sub_total);
            $("#total_invoice_amount").val(sub_total);


            $('.sub_total').text(parseFloat(sub_total).toFixed(2));
            $('.total').text(parseFloat(sub_total).toFixed(2));
        }


        document.forms['customer_info'].elements['country_id'].value=19;
        document.forms['update-invoice'].elements['customer_id'].value="{{$edit_invoice->customer_id}}";
        document.forms['update-invoice'].elements['company_id'].value="{{$edit_invoice->company_id}}";
        document.forms['update-invoice'].elements['shipping_method'].value="{{$edit_invoice->shipping_method}}";
        document.forms['update-invoice'].elements['status'].value="{{$edit_invoice->status}}";

        @foreach($countries as $value)
                @if($value->id==$edit_invoice->country_id)
        <?php $country = $value->countryName; ?>
                @endif
                @endforeach

            document.forms['update-invoice'].elements['invoice_address'].value="{{$edit_invoice->address.', '.$edit_invoice->city.', '.$country}}";





        @if( !empty(Session::get('customer_id')))
            document.forms['update-invoice'].elements['customer_id'].value="{{Session::get('customer_id')}}";
        document.forms['update-invoice'].elements['invoice_address'].value="{{Session::get('invoice_address')}}";

        {{ Session::put('customer_id','') }}
        {{ Session::put('invoice_address','') }}
        //document.getElementById('invoice_address').readOnly = false;
        @endif




$(function(){
            $(".customer_id").on('change',function(){
                console.log($(this).val());
                $.ajax({
                    scriptCharset: "utf-8" ,
                    contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                    type: 'GET',
                    url: "{{URL::to('/select-customer-info')}}"+"/"+$(this).val(),
                    success: function(text) {
                        document.getElementById("invoice_address").value = text;
                    }
                });
            });
        });


        $("#status").change(function() {
            if ($("#status").val() == "partial paid") {
                $("#adv_payment").css("display", "block");
                $("#advance_payment").css("display", "block");
            }else {
                $("#adv_payment").css("display", "none");
                $("#advance_payment").css("display", "none");
            }
        }).trigger("change");

    </script>
    <script type="text/javascript" src="{{URL::asset('js/lib/invoice_add_v2.js')}}"></script>
@endsection


