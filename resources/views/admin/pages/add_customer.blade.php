@extends('admin.master')
@section('content')


    <script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.0/jquery.validate.min.js"></script>


    <div class="row wrapper white-bg page-heading">
        <div class="col-lg-12">
            <h2 style="color: #2F4050; font-size: 16px; font-weight: 400; margin-top: 18px">Customer</h2>

        </div>

    </div>

    <div class="wrapper wrapper-content animated fadeIn">


        <div class="wrapper wrapper-content">
            <div class="row">

                <div class="col-md-12">



                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Add Customer</h5>

                            <!--   <a href="" class="btn btn-xs btn-primary btn-rounded pull-right"><i class="fa fa-bars"></i> Import Contacts</a>-->

                        </div>
                        <div class="ibox-content" id="ibox_form">

                            @include('admin.partials.message')

                            {!! Form::open(['url' => '/save-customer','class'=>'form-horizontal','id'=>'rform','name'=>'customer_info']) !!}

                            <div class="row">
                                <div class="col-md-6 col-sm-12">


                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="account">Full Name <small class="red">*</small> </label>
                                        <div class="col-lg-8">
                                            <input type="text" id="full_name" required name="customer_name" class="form-control" autofocus>

                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="phone">Mobile <small class="red">*</small> </label>

                                        <div class="col-lg-8">
                                            <input type="text" id="phone" required  name="phone" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="address"> Address <small class="red">*</small></label>
                                        <div class="col-lg-8">
                                            <input type="text" id="address" required name="address" class="form-control">
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="city">City</label>

                                        <div class="col-lg-8">
                                            <input type="text" id="city" name="city" class="form-control">

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="email">Email</label>
                                        <div class="col-lg-8">
                                            <input type="email" id="email" name="email" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="zip">ZIP/Postal Code </label>

                                        <div class="col-lg-8">
                                            <input type="text" id="zip" name="zip_code" class="form-control">

                                        </div>
                                    </div>
                                    <div class="form-group" hidden>
                                        <label class="col-md-4 control-label" for="country">Country <small class="red">*</small></label>

                                        <div class="col-lg-8">

                                            <select name="country_id" id="country" class="form-control" >
                                                <option>Select Country</option>
                                                @foreach($countries as $value)
                                                    <option value="{{$value->id}}">{{$value->countryName}}</option>
                                                @endforeach

                                            </select>

                                        </div>
                                    </div>





                                </div>

                            </div>


                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="col-md-offset-2 col-lg-10">

                                            <button class="md-btn md-btn-primary waves-effect waves-light" type="submit" name="submit" id="btn"><i class="fa fa-check"></i> Save</button> | <button class="md-btn md-btn-danger waves-effect waves-light" type="reset" >Reset</button> | <a href="{{URL::to('/list-customer')}}" class="md-btn md-btn-danger waves-effect waves-light" type="reset" ><i class="fa fa-times"></i> Close</a>


                                        </div>
                                    </div>
                                </div>
                            </div>


                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>



    </div>


    </div>



    </div>
@endsection
@section('javascript')


    <script type="text/javascript">
        $(document).ready(function() {
            $("#rform").validate({
                rules: {
                    customer_name: "required",
                    phone: "required",
                    address: "required",
                    country_id: "required"
                },
                messages: {
                    customer_name: "Please specify Customer Name",
                    phone: "Please specify your Valid Phone",
                    address: "Please specify your Address",
                    country_id: "Please specify your Country name"

                }
            })



            $('#btn').click(function() {
                //alert();
                $("#rform").valid();
            });


            $("#companies").select2({
                theme: "bootstrap"
            });

            $("#country").select2({
                theme: "bootstrap"
            });



        });

        document.forms['customer_info'].elements['country_id'].value=19;
    </script>
@endsection