@extends('admin.master')
@section('content')
<div class="wrapper wrapper-content animated fadeIn">
                




<div class="row">
    <div class="col-md-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Add Shipping Method</h5>

            </div>
            <div class="ibox-content">
                @include('admin.partials.message')

                @if($edit_shipping==null)
                {!! Form::open(['url' => '/save-shipping-method','class'=>'form-horizontal','id'=>'rform','name'=>'']) !!}
                @else
                    {!! Form::open(['url' => '/update-shipping-method','class'=>'form-horizontal','id'=>'rform','name'=>'']) !!}
                @endif

                <div class="col-md-5"><label for="rname" class="control-label"><small class="req text-danger">* </small>Add Shipping Method</label>
                    <input type="text" id="rname" name="name" value="{{$edit_shipping==null?'':$update_shipping->name}}" required class="form-control" autofocus><br>
                    <input type="hidden" id="rname" name="id" value="{{$edit_shipping==null?'':$update_shipping->id}}" class="form-control" autofocus>
                    <button class="md-btn md-btn-primary" type="submit" id="submit"><i class="fa fa-check"></i> Save</button><br><br>
                </div>


                            <table class="table table-bordered roles no-margin">
                                <thead>
                                <tr>
                                    <th class="bold">Name</th>
                                    <th class="text-center bold">Manage</th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach($list_shipping_method as $value)
                                <tr data-id="1">

                                    <td>{{$value->name}}</td>

                                    <td class="text-right">

                                        <a href="{{URL::to('/edit-shipping/'.$value->id)}}" class="btn btn-inverse btn-xs"><i class="fa fa-pencil"></i> Edit</a>
                                        <a href="{{URL::to('/delete-shipping/'.$value->id)}}" class="btn btn-danger btn-xs cdelete" id="uid118"><i class="fa fa-trash"></i> Delete</a>
                                    </td>



                                </tr>
                                    @endforeach



                                </tbody>
                            </table>




                             </div>

                          </div>>
                        {!! Form::close() !!}


            </div>
        </div>




</div>


@endsection
@section('javascript')
    <script type="text/javascript">
        $(document).ready(function() {
            $("#rform").validate({
                rules: {
                    customer_name: "required",

                },
                messages: {
                    name: "Please specify Name",


                }
            })
        });
    </script>
@endsection