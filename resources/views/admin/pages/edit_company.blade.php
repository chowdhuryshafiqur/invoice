@extends('admin.master')
@section('content')


    <script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.0/jquery.validate.min.js"></script>


    <div class="row wrapper white-bg page-heading">
        <div class="col-lg-12">
        </div>

    </div>

    <div class="wrapper wrapper-content animated fadeIn">


        <div class="wrapper wrapper-content">
            <div class="row">

                <div class="col-md-12">



                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5>Edit  Company</h5>

                            <!--   <a href="" class="btn btn-xs btn-primary btn-rounded pull-right"><i class="fa fa-bars"></i> Import Contacts</a>-->

                        </div>
                        <div class="ibox-content" id="ibox_form">

                            @include('admin.partials.message')

                            {!! Form::open(['url' => '/update-company','class'=>'form-horizontal','id'=>'rform','name'=>'company_info','enctype'=>'multipart/form-data']) !!}

                            <div class="row">
                                <div class="col-md-6 col-sm-12">


                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="account">Store Name <small class="red">*</small> </label>
                                        <div class="col-lg-8">
                                            <input type="text" id="full_name" value="{{$edit_company->company_name}}"name="company_name" class="form-control" required autofocus>
                                            <input type="hidden" id="full_name" value="{{$edit_company->id}}" name="id" class="form-control" autofocus>

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="phone">Mobile</label>

                                        <div class="col-lg-8">
                                            <input type="text" id="phone" value="{{$edit_company->phone}}"name="phone" required class="form-control">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="address">Address</label>
                                        <div class="col-lg-8">
                                            <input type="text" id="address"value="{{$edit_company->address}}" name="address" required class="form-control">
                                        </div>
                                    </div>


                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="city">City</label>

                                        <div class="col-lg-8">
                                            <input type="text" id="city" value="{{$edit_company->city}}" name="city" class="form-control">

                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="zip">ZIP/Postal Code </label>

                                        <div class="col-lg-8">
                                            <input type="text" id="zip" value="{{$edit_company->zip_code}}"name="zip_code" class="form-control">

                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="email">Email</label>
                                        <div class="col-lg-8">
                                            <input type="email" id="email" value="{{$edit_company->email}}" name="email" class="form-control">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="country">Country</label>

                                        <div class="col-lg-8">

                                            <select name="country_id" id="country" class="form-control">
                                                <option>Select Country</option>
                                                @foreach($countries as $value)
                                                    <option value="{{$value->id}}">{{$value->countryName}}</option>
                                                @endforeach

                                            </select>

                                        </div>
                                    </div>





                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="website">Website</label>
                                        <div class="col-lg-8">
                                            <input type="url" id="website" value="{{$edit_company->website}}" name="website" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-4 control-label" for="fb">Facebook Link</label>

                                        <div class="col-lg-8">
                                            <input type="text" id="fb_link" value="{{$edit_company->fb_link}}" name="fb_link" class="form-control">
                                        </div>
                                    </div>
                                     <div class="form-group">
                                         <label class="col-md-4 control-label" for="fb">Comapny logo</label>

                                         <div class="col-lg-8" style="margin-top: 5px">
                                             <input type="file" id="file" name="logo">
                                             <img src="{{asset($edit_company->logo)}}" width="50%" height="50%">
                                             <input  name="last_image_path" value="{{$edit_company->logo}}" type="hidden">
                                         </div>
                                     </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <div class="col-md-offset-2 col-lg-10">

                                            <button class="md-btn md-btn-primary waves-effect waves-light" type="submit" name="submit" id="btn"><i class="fa fa-check"></i> Save</button> | <button class="md-btn md-btn-danger waves-effect waves-light" type="reset" >Cancel</button>


                                        </div>
                                    </div>
                                </div>
                            </div>


                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>

            </div>


        </div>
    </div>



    </div>


    </div>



    </div>
@endsection


@section('javascript')
    <script type="text/javascript">
        $(document).ready(function() {
            $("#rform").validate({
                rules: {
                    company_name: "required",
                    phone: "required",
                    address: "required"
                },
                messages: {
                    company_name: "Please specify Customer Name",
                    phone: "Please specify your Valid Phone",
                    address: "Please specify your Address"
                }
            })



            $('#btn').click(function() {
                //alert();
                $("#rform").valid();
            });


            $("#companies").select2({
                theme: "bootstrap"
            });

            $("#country").select2({
                theme: "bootstrap"
            });



        });

        document.forms['company_info'].elements['country_id'].value={{$edit_company->country_id}};
    </script>
@endsection