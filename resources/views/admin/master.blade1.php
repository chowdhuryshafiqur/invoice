
<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="robots" content="noindex,nofollow">
    <meta name="googlebot" content="noindex">

    <title>Dashboard</title>
    <link rel="shortcut icon" href="icon/favicon.ico" type="image/x-icon" />

    <link href="{{URL::asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{URL::asset('css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{URL::asset('css/lib/_all.css')}}" rel="stylesheet">
    <link href="{{URL::asset('css/animate.css')}}" rel="stylesheet">
    <link href="{{URL::asset('css/bootstrap-toggle.min.css')}}" rel="stylesheet">
    <link href="{{URL::asset('css/open-sans.css')}}" rel="stylesheet">
    <link href="{{URL::asset('css/style.css')}}" rel="stylesheet">
    <link href="{{URL::asset('css/component.css')}}" rel="stylesheet">
    <link href="{{URL::asset('css/custom.css')}}" rel="stylesheet">

    <link href="{{URL::asset('css/ibilling_icons.css')}}" rel="stylesheet">
    <link href="{{URL::asset('css/material.css')}}" rel="stylesheet">

    <link href="{{URL::asset('css/dark.css')}}" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="{{URL::asset('css/select2.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{URL::asset('css/lib/dashboard.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{URL::asset('css/lib/datepicker.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{URL::asset('css/lib/redactor.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{URL::asset('css/liststyle.css')}}" />

    <link rel="stylesheet" type="text/css" href="{{URL::asset('css/demo.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{URL::asset('css/plugins.css')}}" />





    <script src="{{URL::asset('js/jquery-1.10.2.js')}}"></script>
    <script src="{{URL::asset('js/jquery-ui-1.10.4.min.js')}}"></script>
    <script src="{{URL::asset('js/bootstrap.min.js')}}"></script>


</head>

<body class="fixed-nav ">
<section>
    <div id="wrapper">
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="side-menu">

                    <li class="nav-header">
                        <div class="dropdown profile-element">
                                    <span>
                                        <img src="{{URL::asset('icon/default-user-avatar.png')}}" class="img-circle" style="max-width: 64px;" alt="">
                                    </span>
                            <a data-toggle="dropdown" class="dropdown-toggle" href="#" aria-expanded="false">
                                                    <span class="clear"> <span class="block m-t-xs">
                                                            <strong class="font-bold font-capitalize" style="text-transform: uppercase;">{{ Auth::user()->name }}</strong>
                                                     </span>
                                                    <!-- <span class="text-muted text-xs block">My Account <b class="caret"></b></span> --></span>
                            </a>
                          <!--  <ul class="dropdown-menu animated fadeIn m-t-xs">
                                <li><a href="">Edit Profile</a></li>
                                <li><a href="">Change Password</a></li>
                                <li class="divider"></li>
                                <li>
                                    <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul> -->
                        </div>
                    </li>



                    <li ><a href="{{URL::to('/home')}}"><i class="fa fa-tachometer"></i> <span class="nav-label">Dashboard</span></a></li>

                    <li class="">
                        <a href="#"><i class="fa fa-tag"></i> <span class="nav-label">Invoice</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="{{URL::to('/create-invoice')}}">New Invoice</a></li>
                            <li><a href="{{URL::to('/list-invoice')}}">List Invoice</a></li>
                            <li><a href="{{URL::to('/unpaid-list-invoice')}}">Unpaid Invoice</a></li>
                            <li><a href="{{URL::to('/paid-list-invoice')}}">Paid Invoice</a></li>
                            <li><a href="{{URL::to('/cancel-list-invoice')}}">Cancelled Invoice</a></li>
                            <li><a href="{{URL::to('/partially-paid-list-invoice')}}">Partially Paid Invoice</a></li>



                        </ul>
                    </li>


                    <li class="">
                        <a href="#"><i class="fa fa-users"></i> <span class="nav-label">Customers</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="{{URL::to('/add-customer')}}">Add Customer</a></li>

                            <li><a href="{{URL::to('/list-customer')}}">List Customers</a></li>


                        </ul>
                    </li>


                    <li class="">
                        <a href="#"><i class="fa fa-cogs"></i> <span class="nav-label">Settings</span><span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                             <li><a href="{{URL::to('/add-company')}}"> <i class="fa fa-university"> </i> Add Store</a></li>

                             <li><a href="{{URL::to('/list-company')}}"><i class="fa fa-university"> </i>  List Store</a></li>
                            <li><a href="{{URL::to('/add-shipping-method')}}"> <i class="fa fa-truck"></i> Shipping Method</a></li>
                            <li><a href="{{URL::to('/system-setting')}}"> <i class="fa fa-cog"></i> System Settings</a></li>



                        </ul>
                    </li>

                </ul>

            </div>
        </nav>
        <div id="page-wrapper" class="gray-bg">
            <div class="row border-bottom">
                <nav class="navbar navbar-fixed-top white-bg" role="navigation" style="margin-bottom: 0">

                    <img class="logo" style="max-height: 40px; width: auto;" src="{{URL::asset('icon/logo.png')}}" alt="Logo">

                    <div class="navbar-header">
                        <a class="navbar-minimalize minimalize-styl-2 btn btn-primary btn-flat" href="#"><i class="fa fa-dedent"></i> </a>

                    </div>
                    <ul class="nav navbar-top-links navbar-right pull-right">
<!--
                        <li class="dropdown">
                            <a class="dropdown-toggle count-info" data-toggle="dropdown" id="get_activity" href="#" aria-expanded="true">
                                <i class="fa fa-bell"></i>
                            </a><div class="dropdown-backdrop"></div>
                            <ul class="dropdown-menu dropdown-alerts" id="activity_loaded">



                                <li id="activity_wait">
                                    <div class="text-center link-block">
                                        <a href="javascript:void(0)">
                                            <strong>Please Wait...</strong> <br> <br>
                                            <img class="text-center" src="" alt="Loading....">

                                        </a>
                                    </div>
                                </li>
                            </ul>
                        </li>
-->
                        <li class="dropdown navbar-user">

                            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">

                                <img src="{{URL::asset('icon/default-user-avatar.png')}}" alt="">
                                <span class="hidden-xs">Welcome   {{ Auth::user()->name }} </span> <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu animated fadeIn">
                                <li class="arrow"></li>
                                <li><a href="">Edit Profile</a></li>
                                <li><a href="">Change Password</a></li>
                                <li class="divider"></li>
                                <li>
                                    <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>

                    </ul>

                </nav>
            </div>
            @yield('content')
        </div>
    </div>
</section>
<!-- BEGIN PRELOADER -->
<!-- END PRELOADER -->
<!-- Mainly scripts -->
<script>
    var _L = [];


    var base_url = '{{URL::to('/')}}';


</script>




<script src="{{URL::asset('js/lib/moment-with-locales.min.js')}}"></script>

<script>
    moment.locale('en');
</script>



<!--<script src="{{URL::asset('js/lib/blockui.js')}}"></script>-->

<script src="{{URL::asset('js/jquery.metisMenu.js')}}"></script>
<script src="{{URL::asset('js/jquery.slimscroll.min.js')}}"></script>
<!--<script src="{{URL::asset('js/lib/bootstrap-toggle.min.js')}}"></script>-->


<!--<script src="{{URL::asset('js/lib/progress.js')}}"></script>-->
<script src="{{URL::asset('js/lib/bootbox.min.js')}}"></script>


<!-- iCheck -->
<!--<script src="{{URL::asset('js/lib/icheck.min.js')}}"></script>-->
<script src="{{URL::asset('js/theme.js')}}"></script>




<script type="text/javascript" src="{{URL::asset('js/lib/redactor.min.js')}}"></script>


<!--<script type="text/javascript" src="{{URL::asset('js/lib/en.js')}}"></script>-->
<script type="text/javascript" src="{{URL::asset('js/lib/datepicker.min.js')}}"></script>



<script type="text/javascript">
    $(document).ready(function(){
        $(".alert-warning, .alert-success").show().delay(3000).fadeOut(500);
    });
</script>


@yield('javascript')


