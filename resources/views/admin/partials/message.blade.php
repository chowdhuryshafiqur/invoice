@if (Session::get('message'))
    <div class="alert alert-success">
        <span >{{ Session::get('message')}} </span>
        {{ Session::put('message', '') }}
    </div>
@elseif (Session::get('error'))
    <div class="alert alert-danger">
        <span >{{ Session::get('error')}} </span>
        {{ Session::put('error', '') }}
    </div>
@endif